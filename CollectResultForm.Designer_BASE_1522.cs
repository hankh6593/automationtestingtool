﻿namespace ReadingResult
{
    partial class CollectResultForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.ResultFolderDialog = new System.Windows.Forms.FolderBrowserDialog();
            this.progressExport = new System.Windows.Forms.ProgressBar();
            this.label6 = new System.Windows.Forms.Label();
            this.cbbProductName = new System.Windows.Forms.ComboBox();
            this.label4 = new System.Windows.Forms.Label();
            this.btnClear = new System.Windows.Forms.Button();
            this.btnBrowseResult = new System.Windows.Forms.Button();
            this.regressionTestToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.lbMessage = new System.Windows.Forms.Label();
            this.evaluationHistoryToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.otherZonesResultToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.txtFolderPath = new System.Windows.Forms.TextBox();
            this.evaluationToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.configurationToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.lbPath = new System.Windows.Forms.Label();
            this.collectResultToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.automationTestToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.jSONGeneratorToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.txtFileName = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.btnSaveAs = new System.Windows.Forms.Button();
            this.txtSaveAs = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.btnExport = new System.Windows.Forms.Button();
            this.label3 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.txtCriteriaOrig = new System.Windows.Forms.TextBox();
            this.cbbTypeExport = new System.Windows.Forms.ComboBox();
            this.label5 = new System.Windows.Forms.Label();
            this.SaveAsDialog = new System.Windows.Forms.FolderBrowserDialog();
            this.label8 = new System.Windows.Forms.Label();
            this.txtCriteriaCopy = new System.Windows.Forms.TextBox();
            this.menuStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // progressExport
            // 
            this.progressExport.Location = new System.Drawing.Point(269, 913);
            this.progressExport.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.progressExport.Name = "progressExport";
            this.progressExport.Size = new System.Drawing.Size(1077, 53);
            this.progressExport.TabIndex = 92;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(22, 275);
            this.label6.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(173, 29);
            this.label6.TabIndex = 88;
            this.label6.Text = "Product Name:";
            // 
            // cbbProductName
            // 
            this.cbbProductName.BackColor = System.Drawing.Color.White;
            this.cbbProductName.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbbProductName.FormattingEnabled = true;
            this.cbbProductName.Location = new System.Drawing.Point(267, 265);
            this.cbbProductName.Margin = new System.Windows.Forms.Padding(4);
            this.cbbProductName.Name = "cbbProductName";
            this.cbbProductName.Size = new System.Drawing.Size(1077, 37);
            this.cbbProductName.TabIndex = 87;
            this.cbbProductName.SelectedIndexChanged += new System.EventHandler(this.cbbProductName_SelectedIndexChanged);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.label4.Location = new System.Drawing.Point(26, 135);
            this.label4.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(929, 29);
            this.label4.TabIndex = 84;
            this.label4.Text = "* Please select folder which includes files format [DeviceID]_[Location][Orig/Cop" +
    "y].csv";
            // 
            // btnClear
            // 
            this.btnClear.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(129)))), ((int)(((byte)(213)))));
            this.btnClear.Font = new System.Drawing.Font("Microsoft Sans Serif", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnClear.ForeColor = System.Drawing.Color.White;
            this.btnClear.Location = new System.Drawing.Point(513, 814);
            this.btnClear.Margin = new System.Windows.Forms.Padding(4);
            this.btnClear.Name = "btnClear";
            this.btnClear.Size = new System.Drawing.Size(195, 57);
            this.btnClear.TabIndex = 83;
            this.btnClear.Text = "Clear All Data";
            this.btnClear.UseVisualStyleBackColor = false;
            this.btnClear.Click += new System.EventHandler(this.btnClear_Click);
            // 
            // btnBrowseResult
            // 
            this.btnBrowseResult.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(129)))), ((int)(((byte)(213)))));
            this.btnBrowseResult.FlatAppearance.BorderSize = 0;
            this.btnBrowseResult.Font = new System.Drawing.Font("Microsoft Sans Serif", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnBrowseResult.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.btnBrowseResult.Location = new System.Drawing.Point(1371, 189);
            this.btnBrowseResult.Margin = new System.Windows.Forms.Padding(4);
            this.btnBrowseResult.Name = "btnBrowseResult";
            this.btnBrowseResult.Size = new System.Drawing.Size(195, 44);
            this.btnBrowseResult.TabIndex = 82;
            this.btnBrowseResult.Text = "Browse";
            this.btnBrowseResult.UseVisualStyleBackColor = false;
            this.btnBrowseResult.Click += new System.EventHandler(this.btnBrowseResult_Click);
            // 
            // regressionTestToolStripMenuItem
            // 
            this.regressionTestToolStripMenuItem.ForeColor = System.Drawing.Color.White;
            this.regressionTestToolStripMenuItem.Name = "regressionTestToolStripMenuItem";
            this.regressionTestToolStripMenuItem.Size = new System.Drawing.Size(138, 31);
            this.regressionTestToolStripMenuItem.Text = "Evaluation";
            this.regressionTestToolStripMenuItem.Click += new System.EventHandler(this.regressionTestToolStripMenuItem_Click);
            // 
            // lbMessage
            // 
            this.lbMessage.AutoSize = true;
            this.lbMessage.Font = new System.Drawing.Font("Microsoft Sans Serif", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbMessage.ForeColor = System.Drawing.Color.Red;
            this.lbMessage.Location = new System.Drawing.Point(1044, 135);
            this.lbMessage.Name = "lbMessage";
            this.lbMessage.Size = new System.Drawing.Size(0, 29);
            this.lbMessage.TabIndex = 89;
            // 
            // evaluationHistoryToolStripMenuItem
            // 
            this.evaluationHistoryToolStripMenuItem.ForeColor = System.Drawing.Color.White;
            this.evaluationHistoryToolStripMenuItem.Name = "evaluationHistoryToolStripMenuItem";
            this.evaluationHistoryToolStripMenuItem.Size = new System.Drawing.Size(219, 31);
            this.evaluationHistoryToolStripMenuItem.Text = "Evaluation History";
            this.evaluationHistoryToolStripMenuItem.Click += new System.EventHandler(this.evaluationHistoryToolStripMenuItem_Click);
            // 
            // otherZonesResultToolStripMenuItem
            // 
            this.otherZonesResultToolStripMenuItem.ForeColor = System.Drawing.Color.White;
            this.otherZonesResultToolStripMenuItem.Name = "otherZonesResultToolStripMenuItem";
            this.otherZonesResultToolStripMenuItem.Size = new System.Drawing.Size(244, 31);
            this.otherZonesResultToolStripMenuItem.Text = "Non-STC Evaluation";
            this.otherZonesResultToolStripMenuItem.Click += new System.EventHandler(this.otherZonesResultToolStripMenuItem_Click);
            // 
            // txtFolderPath
            // 
            this.txtFolderPath.BackColor = System.Drawing.Color.White;
            this.txtFolderPath.Font = new System.Drawing.Font("Microsoft Sans Serif", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtFolderPath.Location = new System.Drawing.Point(267, 194);
            this.txtFolderPath.Margin = new System.Windows.Forms.Padding(4);
            this.txtFolderPath.Multiline = true;
            this.txtFolderPath.Name = "txtFolderPath";
            this.txtFolderPath.Size = new System.Drawing.Size(1077, 44);
            this.txtFolderPath.TabIndex = 80;
            // 
            // evaluationToolStripMenuItem
            // 
            this.evaluationToolStripMenuItem.Font = new System.Drawing.Font("Arial", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.evaluationToolStripMenuItem.ForeColor = System.Drawing.Color.White;
            this.evaluationToolStripMenuItem.Name = "evaluationToolStripMenuItem";
            this.evaluationToolStripMenuItem.Size = new System.Drawing.Size(155, 31);
            this.evaluationToolStripMenuItem.Text = "Comparison";
            this.evaluationToolStripMenuItem.Click += new System.EventHandler(this.evaluationToolStripMenuItem_Click);
            // 
            // configurationToolStripMenuItem
            // 
            this.configurationToolStripMenuItem.Font = new System.Drawing.Font("Arial", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.configurationToolStripMenuItem.ForeColor = System.Drawing.Color.White;
            this.configurationToolStripMenuItem.Name = "configurationToolStripMenuItem";
            this.configurationToolStripMenuItem.Size = new System.Drawing.Size(170, 31);
            this.configurationToolStripMenuItem.Text = "Configuration";
            this.configurationToolStripMenuItem.Click += new System.EventHandler(this.configurationToolStripMenuItem_Click);
            // 
            // lbPath
            // 
            this.lbPath.AutoSize = true;
            this.lbPath.Font = new System.Drawing.Font("Microsoft Sans Serif", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbPath.Location = new System.Drawing.Point(22, 209);
            this.lbPath.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lbPath.Name = "lbPath";
            this.lbPath.Size = new System.Drawing.Size(164, 29);
            this.lbPath.TabIndex = 79;
            this.lbPath.Text = "Result Folder:";
            // 
            // collectResultToolStripMenuItem
            // 
            this.collectResultToolStripMenuItem.ForeColor = System.Drawing.Color.White;
            this.collectResultToolStripMenuItem.Name = "collectResultToolStripMenuItem";
            this.collectResultToolStripMenuItem.Size = new System.Drawing.Size(176, 31);
            this.collectResultToolStripMenuItem.Text = "Collect Result";
            this.collectResultToolStripMenuItem.Click += new System.EventHandler(this.collectResultToolStripMenuItem_Click);
            // 
            // menuStrip1
            // 
            this.menuStrip1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(12)))), ((int)(((byte)(123)))), ((int)(((byte)(177)))));
            this.menuStrip1.Font = new System.Drawing.Font("Arial", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.menuStrip1.ImageScalingSize = new System.Drawing.Size(20, 20);
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.regressionTestToolStripMenuItem,
            this.evaluationHistoryToolStripMenuItem,
            this.otherZonesResultToolStripMenuItem,
            this.collectResultToolStripMenuItem,
            this.evaluationToolStripMenuItem,
            this.automationTestToolStripMenuItem,
            this.configurationToolStripMenuItem,
            this.jSONGeneratorToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Padding = new System.Windows.Forms.Padding(5, 6, 0, 6);
            this.menuStrip1.Size = new System.Drawing.Size(1592, 43);
            this.menuStrip1.TabIndex = 78;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // automationTestToolStripMenuItem
            // 
            this.automationTestToolStripMenuItem.ForeColor = System.Drawing.Color.White;
            this.automationTestToolStripMenuItem.Name = "automationTestToolStripMenuItem";
            this.automationTestToolStripMenuItem.Size = new System.Drawing.Size(197, 31);
            this.automationTestToolStripMenuItem.Text = "Automation Test";
            this.automationTestToolStripMenuItem.Click += new System.EventHandler(this.automationTestToolStripMenuItem_Click);
            // 
            // jSONGeneratorToolStripMenuItem
            // 
            this.jSONGeneratorToolStripMenuItem.ForeColor = System.Drawing.Color.White;
            this.jSONGeneratorToolStripMenuItem.Name = "jSONGeneratorToolStripMenuItem";
            this.jSONGeneratorToolStripMenuItem.Size = new System.Drawing.Size(205, 31);
            this.jSONGeneratorToolStripMenuItem.Text = "JSON Generator";
            this.jSONGeneratorToolStripMenuItem.Click += new System.EventHandler(this.jSONGeneratorToolStripMenuItem_Click);
            // 
            // txtFileName
            // 
            this.txtFileName.BackColor = System.Drawing.Color.White;
            this.txtFileName.Font = new System.Drawing.Font("Microsoft Sans Serif", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtFileName.Location = new System.Drawing.Point(267, 398);
            this.txtFileName.Margin = new System.Windows.Forms.Padding(4);
            this.txtFileName.Multiline = true;
            this.txtFileName.Name = "txtFileName";
            this.txtFileName.Size = new System.Drawing.Size(1077, 38);
            this.txtFileName.TabIndex = 98;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(22, 407);
            this.label1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(126, 29);
            this.label1.TabIndex = 97;
            this.label1.Text = "File name:";
            // 
            // btnSaveAs
            // 
            this.btnSaveAs.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(129)))), ((int)(((byte)(213)))));
            this.btnSaveAs.FlatAppearance.BorderSize = 0;
            this.btnSaveAs.Font = new System.Drawing.Font("Microsoft Sans Serif", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnSaveAs.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.btnSaveAs.Location = new System.Drawing.Point(1371, 329);
            this.btnSaveAs.Margin = new System.Windows.Forms.Padding(4);
            this.btnSaveAs.Name = "btnSaveAs";
            this.btnSaveAs.Size = new System.Drawing.Size(195, 40);
            this.btnSaveAs.TabIndex = 96;
            this.btnSaveAs.Text = "Browse";
            this.btnSaveAs.UseVisualStyleBackColor = false;
            this.btnSaveAs.Click += new System.EventHandler(this.btnSaveAs_Click);
            // 
            // txtSaveAs
            // 
            this.txtSaveAs.BackColor = System.Drawing.Color.White;
            this.txtSaveAs.Font = new System.Drawing.Font("Microsoft Sans Serif", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtSaveAs.Location = new System.Drawing.Point(267, 331);
            this.txtSaveAs.Margin = new System.Windows.Forms.Padding(4);
            this.txtSaveAs.Multiline = true;
            this.txtSaveAs.Name = "txtSaveAs";
            this.txtSaveAs.Size = new System.Drawing.Size(1077, 38);
            this.txtSaveAs.TabIndex = 95;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(22, 340);
            this.label2.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(106, 29);
            this.label2.TabIndex = 94;
            this.label2.Text = "Save As:";
            // 
            // btnExport
            // 
            this.btnExport.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(129)))), ((int)(((byte)(213)))));
            this.btnExport.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnExport.ForeColor = System.Drawing.Color.White;
            this.btnExport.Location = new System.Drawing.Point(269, 814);
            this.btnExport.Margin = new System.Windows.Forms.Padding(4);
            this.btnExport.Name = "btnExport";
            this.btnExport.Size = new System.Drawing.Size(204, 57);
            this.btnExport.TabIndex = 93;
            this.btnExport.Text = "Export Excel";
            this.btnExport.UseVisualStyleBackColor = false;
            this.btnExport.Click += new System.EventHandler(this.btnExport_Click);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(25, 70);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(199, 36);
            this.label3.TabIndex = 99;
            this.label3.Text = "Collect Result";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(22, 481);
            this.label7.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(150, 29);
            this.label7.TabIndex = 97;
            this.label7.Text = "Criteria Orig:";
            // 
            // txtCriteriaOrig
            // 
            this.txtCriteriaOrig.BackColor = System.Drawing.Color.White;
            this.txtCriteriaOrig.Font = new System.Drawing.Font("Microsoft Sans Serif", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtCriteriaOrig.Location = new System.Drawing.Point(267, 472);
            this.txtCriteriaOrig.Margin = new System.Windows.Forms.Padding(4);
            this.txtCriteriaOrig.Multiline = true;
            this.txtCriteriaOrig.Name = "txtCriteriaOrig";
            this.txtCriteriaOrig.Size = new System.Drawing.Size(1077, 103);
            this.txtCriteriaOrig.TabIndex = 98;
            this.txtCriteriaOrig.Text = "True Positive-Valid-Valid(1);False Negative-Not Valid-Not Valid(3);Please Try Aga" +
    "in-Please Try Again-Please Try Again(4)";
            // 
            // cbbTypeExport
            // 
            this.cbbTypeExport.BackColor = System.Drawing.Color.White;
            this.cbbTypeExport.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbbTypeExport.FormattingEnabled = true;
            this.cbbTypeExport.Items.AddRange(new object[] {
            "Honeywell Regression Test"});
            this.cbbTypeExport.Location = new System.Drawing.Point(267, 745);
            this.cbbTypeExport.Margin = new System.Windows.Forms.Padding(4);
            this.cbbTypeExport.Name = "cbbTypeExport";
            this.cbbTypeExport.Size = new System.Drawing.Size(1077, 37);
            this.cbbTypeExport.TabIndex = 87;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(22, 755);
            this.label5.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(149, 29);
            this.label5.TabIndex = 88;
            this.label5.Text = "Type Export:";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.Location = new System.Drawing.Point(22, 616);
            this.label8.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(159, 29);
            this.label8.TabIndex = 97;
            this.label8.Text = "Criteria Copy:";
            // 
            // txtCriteriaCopy
            // 
            this.txtCriteriaCopy.BackColor = System.Drawing.Color.White;
            this.txtCriteriaCopy.Font = new System.Drawing.Font("Microsoft Sans Serif", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtCriteriaCopy.Location = new System.Drawing.Point(267, 607);
            this.txtCriteriaCopy.Margin = new System.Windows.Forms.Padding(4);
            this.txtCriteriaCopy.Multiline = true;
            this.txtCriteriaCopy.Name = "txtCriteriaCopy";
            this.txtCriteriaCopy.Size = new System.Drawing.Size(1077, 103);
            this.txtCriteriaCopy.TabIndex = 98;
            this.txtCriteriaCopy.Text = "False Positive-Valid-Valid(1);True Negative-Not Valid-Not Valid(3);Please Try Aga" +
    "in-Please Try Again-Please Try Again(4)";
            // 
            // CollectResultForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(1592, 1030);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.txtCriteriaCopy);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.txtCriteriaOrig);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.txtFileName);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.btnSaveAs);
            this.Controls.Add(this.txtSaveAs);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.btnExport);
            this.Controls.Add(this.progressExport);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.cbbTypeExport);
            this.Controls.Add(this.cbbProductName);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.btnClear);
            this.Controls.Add(this.btnBrowseResult);
            this.Controls.Add(this.lbMessage);
            this.Controls.Add(this.txtFolderPath);
            this.Controls.Add(this.lbPath);
            this.Controls.Add(this.menuStrip1);
            this.Name = "CollectResultForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "U-nica Mini Tool v2.13";
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.FolderBrowserDialog ResultFolderDialog;
        private System.Windows.Forms.ProgressBar progressExport;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.ComboBox cbbProductName;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Button btnClear;
        private System.Windows.Forms.Button btnBrowseResult;
        private System.Windows.Forms.ToolStripMenuItem regressionTestToolStripMenuItem;
        private System.Windows.Forms.Label lbMessage;
        private System.Windows.Forms.ToolStripMenuItem evaluationHistoryToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem otherZonesResultToolStripMenuItem;
        private System.Windows.Forms.TextBox txtFolderPath;
        private System.Windows.Forms.ToolStripMenuItem evaluationToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem configurationToolStripMenuItem;
        private System.Windows.Forms.Label lbPath;
        private System.Windows.Forms.ToolStripMenuItem collectResultToolStripMenuItem;
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.TextBox txtFileName;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button btnSaveAs;
        private System.Windows.Forms.TextBox txtSaveAs;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button btnExport;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox txtCriteriaOrig;
        private System.Windows.Forms.ComboBox cbbTypeExport;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.FolderBrowserDialog SaveAsDialog;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.TextBox txtCriteriaCopy;
        private System.Windows.Forms.ToolStripMenuItem automationTestToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem jSONGeneratorToolStripMenuItem;
    }
}