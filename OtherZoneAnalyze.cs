﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Windows.Forms.DataVisualization.Charting;
using MathNet.Numerics.Statistics;
using Microsoft.Office.Interop.Excel;
using DataTable = System.Data.DataTable;
using Excel = Microsoft.Office.Interop.Excel;

namespace ReadingResult
{
    public partial class OtherZoneAnalyze : Form
    {
        List<OtherZonesData> listResult = null;
        List<Device> listDevice = null;
        List<Product> listProduct = null;
        List<Condition> listCondition = null;
        List<Excel.Range> listRange = null;
        Database db;
        public OtherZoneAnalyze()
        {
            InitializeComponent();
            this.FolderDialog = new System.Windows.Forms.FolderBrowserDialog();
            db = new Database();
            progressExport.Visible = false;
            btnReset.Enabled = false;
            btnDraw.Enabled = false;
            LoadProduct();
        }
        private void LoadDevice(List<String> listCode)
        {
            listDevice = new List<Device>();
            for (int i = 0; i < listCode.Count; i++)
            {
                listDevice.Add(db.GetDeviceByCodeAndLocation(listCode[i].Split('_')[0], listCode[i].Split('_')[1]));
            }

            if (listDevice.Count > 0)
            {
                Dictionary<int, String> listDislplay = new Dictionary<int, string>();
                for (int i = 0; i < listDevice.Count; i++)
                {
                    listDislplay.Add(listDevice[i].id, listDevice[i].name + " (" + listDevice[i].location + ")");
                }

                var bindingDevice = new BindingSource();
                bindingDevice.DataSource = listDislplay;

                cbbDeviceName.DataSource = new BindingSource(listDislplay, null);
                cbbDeviceName.ValueMember = "Key";
                cbbDeviceName.DisplayMember = "Value";
                cbbDeviceName.SelectedIndex = -1;
            }

        }
        private void LoadProduct()
        {

            listProduct = db.GetAllProducts();
            if (listProduct.Count > 0)
            {
                var bindingProduct = new BindingSource();
                bindingProduct.DataSource = listProduct;

                cbbProductName.DataSource = bindingProduct.DataSource;

                cbbProductName.DisplayMember = "Name";
                cbbProductName.ValueMember = "id";

            }
        }
        private void otherZonesResultToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.Hide();
            OtherZoneAnalyze form = new OtherZoneAnalyze();
            form.Show();
        }
        private void btnBrowseResult_Click(object sender, EventArgs e)
        {
            FolderDialog.ShowDialog();
            txtFolderPath.Text = FolderDialog.SelectedPath;
            lbMessage.Text = "";
            btnGetResult.Enabled = true;
        }
        private void btnGetResult_Click(object sender, EventArgs e)
        {
            lbMessage.Text = "";
            listResult = new List<OtherZonesData>();
            if (txtFolderPath.Text.Length == 0)
            {
                MessageBox.Show("Please browse the result folder.");
            }
            else if (listCondition.Count == 0)
            {
                MessageBox.Show("This product has not setup yet. Please perform configuration for this product.");
            }
            else if (cbbProductName.SelectedIndex == -1)
            {
                MessageBox.Show("Please select Product.");
            }
            else
            {
                Product product = ((Product)cbbProductName.SelectedItem);
                product.listCondition = db.GetConditionByProductID(product.id);
                for (int i = 0; i < product.listCondition.Count; i++)
                {
                    if (product.listCondition[i].isActivate && product.listCondition[i].method.Equals("CDS"))
                    {
                        txtThreshold.Text = product.listCondition[i].threshold.ToString();
                    }
                }

                List<String> listCode = new List<String>();
                DirectoryInfo d = new DirectoryInfo(txtFolderPath.Text);
                FileInfo[] files = d.GetFiles("*.csv");
                for (int i = 0; i < files.Count(); i++)
                {
                    if (files[i].Name.Contains("Orig"))
                    {
                        listCode.Add(files[i].Name.Replace("Orig.csv", ""));
                    }
                }
                LoadDevice(listCode);
                progressExport.Value = 0;
                progressExport.Minimum = 0;
                progressExport.Visible = true;
                progressExport.Maximum = listCode.Count() * 2;
                int index = 1;
                listRange = new List<Excel.Range>();
                for (int i = 0; i < listCode.Count(); i++)
                {

                    OtherZonesData data = new OtherZonesData();
                    data.deviceID = listCode[i].Split('_')[0];
                    data.productName = cbbProductName.Text;
                    String fileOrig = txtFolderPath.Text + @"\" + listCode[i] + "Orig.csv";
                    String fileCopy = txtFolderPath.Text + @"\" + listCode[i] + "Copy.csv";

                    List<List<String>> listDataOrig = Ultility.GetCleanData(fileOrig, listCondition);
                    data.listResultOrig = Ultility.GetResultListFromDataRaw(listDataOrig,listCondition,1, listDataOrig.Count,false);
                    progressExport.Value = index;

                    List<List<String>> listDataCopy = Ultility.GetCleanData(fileCopy, listCondition);
                    data.listResultCopy = Ultility.GetResultListFromDataRaw(listDataCopy, listCondition,1,listDataCopy.Count, false);
                    progressExport.Value = index+1;

                    index += 2;
                    listResult.Add(data);
                }
                if (listCode.Count > 0)
                {
                    progressExport.Value = listCode.Count() * 2 - 1;
                }

            }

            lbMessage.Text = "Get result has been done.";
            progressExport.Visible = false;
        }
        private void ShowResult()
        {

            dgvResult.Columns.Clear();
            dgvResult.DataSource = null;
            OtherZonesData result = new OtherZonesData();
            for (int i = 0; i < listResult.Count; i++)
            {
                int id;
                if (Int32.TryParse(cbbDeviceName.SelectedValue.ToString(), out id))
                {
                    if (listResult[i].deviceID.Equals(db.GetDeviceByID(id).code))
                    {
                        result = listResult[i];
                        i = listResult.Count;
                    }
                }
                else
                {
                   
                    if (listResult[i].deviceID.Equals(db.GetDeviceByID(Int32.Parse(((KeyValuePair<int, string>)cbbDeviceName.SelectedValue).Key.ToString())).code))
                    {
                        result = listResult[i];
                        i = listResult.Count;
                    }
                }

              
            }

            DataTable dt = new DataTable();
            dt.Columns.Add("Index ");
            for (int i = 0; i < listCondition.Count; i++)
            {
                if (listCondition[i].isActivate && !listCondition[i].method.Equals("STC"))
                {
                    
                    dt.Columns.Add("Zone Result " + (i + 1) + " Original");
                    dt.Columns.Add("Zone Result " + (i + 1) + " Copy");
                    dt.Columns.Add("Threshold " + (i + 1));
                }
            }
            dt.Columns.Add("Device ID");

            int count = result.listResultCopy.Count > result.listResultOrig.Count ? result.listResultCopy.Count : result.listResultOrig.Count;
            int distance = Math.Abs(result.listResultCopy.Count - result.listResultOrig.Count);
            bool isCopyMore = result.listResultCopy.Count > result.listResultOrig.Count ? true : false;
            for (int i = 0; i < count; i++)
            {
                int column = 1;
                dt.Rows.Add();
                dt.Rows[dt.Rows.Count - 1][0] = (i + 1);
                if (listCondition[0].isActivate && !listCondition[0].method.Equals("STC"))
                {
                    if (i >= count - distance)
                    {
                        if (isCopyMore)
                        {
                            dt.Rows[dt.Rows.Count - 1][column] = "";
                            dt.Rows[dt.Rows.Count - 1][column + 1] = Double.Parse(result.listResultCopy[i].ResultZone1);
                        }
                        else
                        {
                            dt.Rows[dt.Rows.Count - 1][column] = Double.Parse(result.listResultOrig[i].ResultZone1);
                            dt.Rows[dt.Rows.Count - 1][column + 1] = "";
                        }
                    }
                    else
                    {
                        dt.Rows[dt.Rows.Count - 1][column] = Double.Parse(result.listResultOrig[i].ResultZone1);
                        dt.Rows[dt.Rows.Count - 1][column + 1] = Double.Parse(result.listResultCopy[i].ResultZone1);

                    }
                    if (listCondition[0].method.Equals("CDS"))
                    {
                        dt.Rows[dt.Rows.Count - 1][column + 2] = listCondition[0].threshold;
                    }
                    else
                    {
                        dt.Rows[dt.Rows.Count - 1][column + 2] = "";
                    }

                    column += 3;
                }
                if (listCondition[1].isActivate && !listCondition[1].method.Equals("STC"))
                {
                    if (i >= count - distance)
                    {
                        if (isCopyMore)
                        {
                            dt.Rows[dt.Rows.Count - 1][column] = "";
                            dt.Rows[dt.Rows.Count - 1][column + 1] = Double.Parse(result.listResultCopy[i].ResultZone2);
                        }
                        else
                        {
                            dt.Rows[dt.Rows.Count - 1][column] = Double.Parse(result.listResultOrig[i].ResultZone2);
                            dt.Rows[dt.Rows.Count - 1][column + 1] = "";
                        }
                    }
                    else
                    {
                        dt.Rows[dt.Rows.Count - 1][column] = Double.Parse(result.listResultOrig[i].ResultZone2);
                        dt.Rows[dt.Rows.Count - 1][column + 1] = Double.Parse(result.listResultCopy[i].ResultZone2);

                    }
                    if (listCondition[1].method.Equals("CDS"))
                    {
                        dt.Rows[dt.Rows.Count - 1][column + 2] = listCondition[1].threshold;
                    }
                    else
                    {
                        dt.Rows[dt.Rows.Count - 1][column + 2] = "";
                    }

                    column += 3;
                }
                if (listCondition[2].isActivate && !listCondition[2].method.Equals("STC"))
                {
                    if (i >= count - distance)
                    {
                        if (isCopyMore)
                        {
                            dt.Rows[dt.Rows.Count - 1][column] = "";
                            dt.Rows[dt.Rows.Count - 1][column + 1] = Double.Parse(result.listResultCopy[i].ResultZone3);
                        }
                        else
                        {
                            dt.Rows[dt.Rows.Count - 1][column] = Double.Parse(result.listResultOrig[i].ResultZone3);
                            dt.Rows[dt.Rows.Count - 1][column + 1] = "";
                        }
                    }
                    else
                    {
                        dt.Rows[dt.Rows.Count - 1][column] = Double.Parse(result.listResultOrig[i].ResultZone3);
                        dt.Rows[dt.Rows.Count - 1][column + 1] = Double.Parse(result.listResultCopy[i].ResultZone3);

                    }
                    if (listCondition[2].method.Equals("CDS"))
                    {
                        dt.Rows[dt.Rows.Count - 1][column + 2] = listCondition[2].threshold;
                    }
                    else
                    {
                        dt.Rows[dt.Rows.Count - 1][column + 2] = "";
                    }

                    column += 3;
                }
                if (listCondition[3].isActivate && !listCondition[3].method.Equals("STC"))
                {
                    if (i >= count - distance)
                    {
                        if (isCopyMore)
                        {
                            dt.Rows[dt.Rows.Count - 1][column] = "";
                            dt.Rows[dt.Rows.Count - 1][column + 1] = Double.Parse(result.listResultCopy[i].ResultZone4);
                        }
                        else
                        {
                            dt.Rows[dt.Rows.Count - 1][column] = Double.Parse(result.listResultOrig[i].ResultZone4);
                            dt.Rows[dt.Rows.Count - 1][column + 1] = "";
                        }
                    }
                    else
                    {
                        dt.Rows[dt.Rows.Count - 1][column] = Double.Parse(result.listResultOrig[i].ResultZone4);
                        dt.Rows[dt.Rows.Count - 1][column + 1] = Double.Parse(result.listResultCopy[i].ResultZone4);

                    }
                    if (listCondition[3].method.Equals("CDS"))
                    {
                        dt.Rows[dt.Rows.Count - 1][column + 2] = listCondition[3].threshold;
                    }
                    else
                    {
                        dt.Rows[dt.Rows.Count - 1][column + 2] = "";
                    }

                    column += 3;
                }
                if (listCondition[4].isActivate && !listCondition[4].method.Equals("STC"))
                {
                    if (i >= count - distance)
                    {
                        if (isCopyMore)
                        {
                            dt.Rows[dt.Rows.Count - 1][column] = "";
                            dt.Rows[dt.Rows.Count - 1][column + 1] = Double.Parse(result.listResultCopy[i].ResultZone5);
                        }
                        else
                        {
                            dt.Rows[dt.Rows.Count - 1][column] = Double.Parse(result.listResultOrig[i].ResultZone5);
                            dt.Rows[dt.Rows.Count - 1][column + 1] = "";
                        }
                    }
                    else
                    {
                        dt.Rows[dt.Rows.Count - 1][column] = Double.Parse(result.listResultOrig[i].ResultZone5);
                        dt.Rows[dt.Rows.Count - 1][column + 1] = Double.Parse(result.listResultCopy[i].ResultZone5);

                    }
                    if (listCondition[4].method.Equals("CDS"))
                    {
                        dt.Rows[dt.Rows.Count - 1][column + 2] = listCondition[4].threshold;
                    }
                    else
                    {
                        dt.Rows[dt.Rows.Count - 1][column + 2] = "";
                    }

                    column += 3;
                }
                int id = 0;
                if (Int32.TryParse(cbbDeviceName.SelectedValue.ToString(), out id))
                {
                    dt.Rows[dt.Rows.Count - 1][column] = db.GetDeviceByID(Int32.Parse(cbbDeviceName.SelectedValue.ToString())).code;
                }
                else
                {
                    dt.Rows[dt.Rows.Count - 1][column] = db.GetDeviceByID(Int32.Parse(((KeyValuePair<int, string>)cbbDeviceName.SelectedValue).Key.ToString())).code;
                }
              
            }

            dgvResult.DataSource = dt;
        }
        private void btnClear_Click(object sender, EventArgs e)
        {
            txtFolderPath.Text = "";
            btnGetResult.Enabled = false;
            cbbDeviceName.DataSource = null;
            cbbProductName.SelectedIndex = -1;
            dgvResult.DataSource = null;
            dgvResult.Rows.Clear();
            txtFalseNegative.Text = "";
            txtFalsePositive.Text = "";
            txtPTA.Text = "";
            dgvCalculateThreshold.DataSource = null;
            dgvCalculateThreshold.Rows.Clear();
            txtThreshold.Text = "";
            ChartCDS.Series.Clear();

        }
        private void cbbProductName_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (cbbProductName.SelectedIndex != -1)
            {
                int result = -1;
                if (Int32.TryParse(cbbProductName.SelectedValue.ToString(), out result))
                {
                    listCondition = db.GetConditionByProductID(Int32.Parse(cbbProductName.SelectedValue.ToString()));
                }
                else
                {
                    listCondition = db.GetConditionByProductID(((Product)cbbProductName.SelectedValue).id);
                }

            }


        }
        private void OtherZoneAnalyze_FormClosed(object sender, FormClosedEventArgs e)
        {
            System.Windows.Forms.Application.Exit();
        }
        private void regressionTestToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.Hide();
            RegressionTestForm form = new RegressionTestForm();
            form.Show();
        }
        private void evaluationToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.Hide();
            EvaluationStatisticForm form = new EvaluationStatisticForm();
            form.Show();
        }
        private void configurationToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.Hide();
            ConfigurationForm form = new ConfigurationForm();
            form.Show();
        }
        private void cbbDeviceName_SelectedIndexChanged(object sender, EventArgs e)
        {
            double threshold;
            if (listResult.Count > 0 && cbbDeviceName.SelectedIndex != -1)
            {
                btnDraw.Enabled = true;
                btnReset.Enabled = true;
                dgvCalculateThreshold.DataSource = null;
                ShowResult();
                ChartCDS.ResetAutoValues();
                ChartCDS.Series.Clear();
                ChartCDS.Legends[0].Docking = Docking.Bottom;
                ChartCDS.MinimumSize = new Size(1397, 700);

                OtherZonesData result = new OtherZonesData();
                for (int i = 0; i < listResult.Count; i++)
                {
                    int id;
                    if(Int32.TryParse(cbbDeviceName.SelectedValue.ToString(),out id))
                    {
                        if (listResult[i].deviceID.Equals(db.GetDeviceByID(Int32.Parse(cbbDeviceName.SelectedValue.ToString())).code))
                        {
                            result = listResult[i];
                            i = listResult.Count;
                        }
                    }
                    else
                    {
                        if (listResult[i].deviceID.Equals(db.GetDeviceByID(Int32.Parse(((KeyValuePair<int, string>)cbbDeviceName.SelectedValue).Key.ToString())).code))
                        {
                            result = listResult[i];
                            i = listResult.Count;
                        }
                    }
                }

                int totalData = 0;
                for (int i = 1; i < listCondition.Count; i++)
                {
                    if (listCondition[i].isActivate && !listCondition[i].method.Equals("STC"))
                    {
                        totalData += result.listResultOrig.Count * 2;
                    }
                }

                List<DataDrawChart> listChart = new List<DataDrawChart>();
                List<DataDrawChart> listOrg = new List<DataDrawChart>();
                List<DataDrawChart> listCopy = new List<DataDrawChart>();

                for (int i = 0; i < result.listResultOrig.Count; i++)
                {
                    DataDrawChart data = new DataDrawChart();
                    if (listCondition[0].isActivate && !listCondition[0].method.Equals("STC"))
                    {
                        data.pointValue = Double.Parse(result.listResultOrig[i].ResultZone1);
                    }
                    if (listCondition[1].isActivate && !listCondition[1].method.Equals("STC"))
                    {
                        data.pointValue = Double.Parse(result.listResultOrig[i].ResultZone2);
                    }
                    if (listCondition[2].isActivate && !listCondition[2].method.Equals("STC"))
                    {
                        data.pointValue = Double.Parse(result.listResultOrig[i].ResultZone3);
                    }
                    if (listCondition[3].isActivate && !listCondition[3].method.Equals("STC"))
                    {
                        data.pointValue = Double.Parse(result.listResultOrig[i].ResultZone4);
                    }
                    if (listCondition[4].isActivate && !listCondition[4].method.Equals("STC"))
                    {
                        data.pointValue = Double.Parse(result.listResultOrig[i].ResultZone5);
                    }
                    listChart.Add(data);
                    listOrg.Add(data);
                }
                for (int i = 0; i < result.listResultCopy.Count; i++)
                {
                    DataDrawChart data = new DataDrawChart();
                    if (listCondition[0].isActivate && !listCondition[0].method.Equals("STC"))
                    {
                        data.pointValue = Double.Parse(result.listResultCopy[i].ResultZone1);
                    }
                    if (listCondition[1].isActivate && !listCondition[1].method.Equals("STC"))
                    {
                        data.pointValue = Double.Parse(result.listResultCopy[i].ResultZone2);
                    }
                    if (listCondition[2].isActivate && !listCondition[2].method.Equals("STC"))
                    {
                        data.pointValue = Double.Parse(result.listResultCopy[i].ResultZone3);
                    }
                    if (listCondition[3].isActivate && !listCondition[3].method.Equals("STC"))
                    {
                        data.pointValue = Double.Parse(result.listResultCopy[i].ResultZone4);
                    }
                    if (listCondition[4].isActivate && !listCondition[4].method.Equals("STC"))
                    {
                        data.pointValue = Double.Parse(result.listResultCopy[i].ResultZone5);
                    }
                    listChart.Add(data);
                    listCopy.Add(data);
                }

                ChartCDS.ChartAreas[0].Position.X = 0;
                ChartCDS.ChartAreas[0].Position.Y = 0;
                ChartCDS.ChartAreas[0].Position.Width = 100;
                ChartCDS.Width = (listOrg.Count + listCopy.Count) * 10;
                if (Double.TryParse(txtThreshold.Text, out threshold))
                {
                    txtFalseNegative.Text = Ultility.CalculateFalseNegative(listOrg, threshold).ToString();
                    txtFalsePositive.Text = Ultility.CalculateFalsePositive(listCopy, threshold).ToString();
                    txtPTA.Text = Ultility.CalculatePTA(listOrg, listCopy).ToString();
                    CalculateThreshold(listOrg, listCopy);
                }

                Random random = new Random();

                var seriesOrig = new System.Windows.Forms.DataVisualization.Charting.Series();
                seriesOrig.Name = "Orig";
                seriesOrig.ChartType = SeriesChartType.Point;
                seriesOrig.IsValueShownAsLabel = true;
                seriesOrig.MarkerSize = 8;
                seriesOrig.MarkerStyle = MarkerStyle.Circle;
                seriesOrig.Color = Color.Green;
                ChartCDS.Series.Add(seriesOrig);
                for (int i = 0; i < listOrg.Count; i++)
                {
                    int index = i + 1;
                    ChartCDS.Series[0].Points.AddXY(index, listOrg[i].pointValue);
                }

                var seriesCopy = new System.Windows.Forms.DataVisualization.Charting.Series();
                seriesCopy.Name = "Copy";
                seriesCopy.ChartType = SeriesChartType.Point;
                seriesCopy.IsValueShownAsLabel = true;
                seriesCopy.MarkerSize = 8;
                seriesCopy.MarkerStyle = MarkerStyle.Circle;
                seriesCopy.Color = Color.Red;
                ChartCDS.Series.Add(seriesCopy);
                for (int i = 0; i < listCopy.Count; i++)
                {

                    int index = i + 1;
                    ChartCDS.Series[1].Points.AddXY(index, listCopy[i].pointValue);

                }

                var SeriesThreshold = new System.Windows.Forms.DataVisualization.Charting.Series();
                SeriesThreshold.Name = "Threshold";
                SeriesThreshold.ChartType = SeriesChartType.Point;
                SeriesThreshold.MarkerSize = 8;
                SeriesThreshold.MarkerStyle = MarkerStyle.Circle;
                SeriesThreshold.Color = Color.LightGray;
                ChartCDS.Series.Add(SeriesThreshold);
                if (Double.TryParse(txtThreshold.Text, out threshold))
                {
                    int count = listOrg.Count > listCopy.Count ? listOrg.Count : listCopy.Count;
                    for (int i = 0; i < count; i++)
                    {

                        int index = i + 1;
                        ChartCDS.Series[2].Points.AddXY(index, threshold);
                    }
                }
                else
                {
                    MessageBox.Show("Threshold value is not valid. Threshold value should be decimal.");
                }

            }

        }

        private double CalculateMean(List<double> listData, int qty)
        {
            double sum = 0;
            int count = 0;
            for (int i = 0; i < qty; i++)
            {
                if (listData[i] != -1)
                {
                    sum += listData[i];
                    count++;
                }
            }
            return Math.Round(sum / count, 2);
        }

        private void CalculateThreshold(List<DataDrawChart> listOrig, List<DataDrawChart> listCopy)
        {
            DataTable dt = new DataTable();
            dt.Columns.Add("Threshold");
            dt.Columns.Add("False Negative");
            dt.Columns.Add("False Positive");
           

            double threshold = 0;
            for (int i = 0; i < listCondition.Count; i++)
            {
                if (listCondition[i].isActivate && listCondition[i].method.Equals("CDS"))
                {
                    threshold = listCondition[i].threshold;
                }
            }


            List<Double> listRange = Ultility.GetRangeThreshold(listOrig, listCopy, threshold);
            for (int i = 0; i < listRange.Count; i++)
            {
                dt.Rows.Add();
                dt.Rows[i][0] = listRange[i];
                dt.Rows[i][1] = Ultility.CalculateFalseNegative(listOrig, listRange[i]);
                dt.Rows[i][2] = Ultility.CalculateFalsePositive(listCopy, listRange[i]);
               
            }
            dgvCalculateThreshold.DataSource = dt;
        }
        private void btnExport_Click(object sender, EventArgs e)
        {
            Excel.Application xlApp;
            Excel.Workbook xlWorkBook;
            Excel.Worksheet xlWorkSheet = new Excel.Worksheet();
            object misValue = System.Reflection.Missing.Value;

            xlApp = new Excel.Application();

            progressExport.Minimum = 0;
            progressExport.Value = 0;
            progressExport.Visible = true;



            xlWorkBook = xlApp.Workbooks.Add(misValue);

            int index = 0;
            DirectoryInfo d = new DirectoryInfo(txtFolderPath.Text);
            FileInfo[] files = d.GetFiles("*.csv");
            progressExport.Maximum = (files.Length + (files.Length / 2)) * 10;
            for (int i = 0; i < cbbDeviceName.Items.Count; i++)
            {
                progressExport.Value = index * 10;
                Device device = db.GetDeviceByID(Int32.Parse(((KeyValuePair<int, string>)cbbDeviceName.Items[i]).Key.ToString()));
                xlWorkSheet = (Excel.Worksheet)xlWorkBook.Worksheets.get_Item(xlWorkBook.Sheets.Count);
                int qtyCopy = 0;
                DataTable dtRaw = Ultility.ParseDataRawToTable(files[index], listCondition);
                xlWorkSheet.Range[xlWorkSheet.Cells[1, 1], xlWorkSheet.Cells[dtRaw.Rows.Count, dtRaw.Columns.Count]].Value = Ultility.ParseDtToArray(dtRaw);
                xlWorkSheet.Name = "D_C_" + device.code + "(" + device.location + ")";
                Excel.Range rangeRaw = xlWorkSheet.Rows[1];
                rangeRaw.Select();
                rangeRaw.Font.Bold = true;
                rangeRaw.Cells.Style.HorizontalAlignment = Excel.XlHAlign.xlHAlignCenter;

                xlWorkBook.Worksheets.Add(After: xlWorkBook.Sheets[xlWorkBook.Sheets.Count]);
                index++;
                progressExport.Value = index * 10;
                xlWorkSheet = (Excel.Worksheet)xlWorkBook.Worksheets.get_Item(xlWorkBook.Sheets.Count);
                int qtyOrignal = 0;
                dtRaw = Ultility.ParseDataRawToTable(files[index], listCondition);
                xlWorkSheet.Range[xlWorkSheet.Cells[1, 1], xlWorkSheet.Cells[dtRaw.Rows.Count, dtRaw.Columns.Count]].Value = Ultility.ParseDtToArray(dtRaw);
                xlWorkSheet.Name = "D_O_" + device.code + "(" + device.location + ")";
                rangeRaw = xlWorkSheet.Rows[1];
                rangeRaw.Select();
                rangeRaw.Font.Bold = true;
                rangeRaw.Cells.Style.HorizontalAlignment = Excel.XlHAlign.xlHAlignCenter;
                xlWorkBook.Worksheets.Add(After: xlWorkBook.Sheets[xlWorkBook.Sheets.Count]);
                index++;
                progressExport.Value = index * 10;
                xlWorkSheet = (Excel.Worksheet)xlWorkBook.Worksheets.get_Item(xlWorkBook.Sheets.Count);
                xlWorkSheet.Name = "Graph_" + device.code + "(" + device.location + ")";
                List<Result> listSave = new List<Result>();
                for (int k = 0; k < listResult.Count; k++)
                {
                    if (device.code.Equals(listResult[k].deviceID))
                    {
                        qtyCopy = listResult[k].listResultCopy.Count;
                        qtyOrignal = listResult[k].listResultOrig.Count;
                        for (int m = 0; m < listResult[k].listResultOrig.Count; m++)
                        {
                            listSave.Add(listResult[k].listResultOrig[m]);

                        }
                        for (int m = 0; m < listResult[k].listResultCopy.Count; m++)
                        {
                            listSave.Add(listResult[k].listResultCopy[m]);

                        }
                        k = listResult.Count;



                    }
                }
                DataTable dt = GetDataTable(listSave, device, qtyOrignal, qtyCopy);
                xlWorkSheet.Range[xlWorkSheet.Cells[1, 1], xlWorkSheet.Cells[dt.Rows.Count, dt.Columns.Count]].Value = Ultility.ParseDtToArray(dt);
                Excel.Range range = xlWorkSheet.Rows[1];
                range.Select();
                range.Font.Bold = true;
                range.Cells.Style.HorizontalAlignment = Excel.XlHAlign.xlHAlignCenter;

                int valid = 0;
                for (int m = 0; m < listCondition.Count; m++)
                {
                    if (listCondition[m].isActivate && !listCondition[m].method.Equals("STC"))
                    {
                        valid++;
                    }
                }
                Excel.ChartObjects xlChart = (Excel.ChartObjects)xlWorkSheet.ChartObjects(Type.Missing);
                Excel.ChartObject myChart = (Excel.ChartObject)xlChart.Add(400 * valid, 120, 700, 425);
                Excel.Chart chartPage = myChart.Chart;

                chartPage.Legend.Position = Excel.XlLegendPosition.xlLegendPositionBottom;
                myChart.Select();

                chartPage.ChartType = Excel.XlChartType.xlXYScatter;
                Excel.SeriesCollection seriesCollection = chartPage.SeriesCollection();
                int row = dt.Rows.Count;
                String[] rangeColumn = new String[] { "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M", "N", "O", "P" };
                int position = 0;
                xlWorkSheet.Cells[2, 8] = "False Negative";
                xlWorkSheet.Cells[3, 8] = "False Positive";
                xlWorkSheet.Cells[4, 8] = "PTA";
                xlWorkSheet.Cells[5, 8] = "Mean Orig";
                xlWorkSheet.Cells[6, 8] = "Std Dev Sample";
                xlWorkSheet.Cells[7, 8] =  "300%";
                xlWorkSheet.Cells[8, 8] = "Threshold (Mean+ 300% of Std Dev)";

                xlWorkSheet.Cells[2, 9].Formula = "=COUNTIF(B2:B" + (qtyOrignal + 1) + ",\">\"&D2)-COUNTIF(B2:B" + (qtyOrignal + 1) + ",-1)";
                xlWorkSheet.Cells[3, 9].Formula = "=COUNTIF(B" + (qtyOrignal + 2) + ":B" + row + ",\"<\"&D2)-COUNTIF(B" + (qtyOrignal + 2) + ":B" + row + ",-1)-COUNTIF(B" + (qtyOrignal + 2) + ":B" + row + ",0)";
                xlWorkSheet.Cells[4, 9].Formula = "=COUNTIF(B2:B" + row + ",-1)";
                xlWorkSheet.Cells[5, 9].Formula = "=AVERAGEIF(B2:B" + (qtyOrignal + 1) + ",\"<>-1\")";
                xlWorkSheet.Cells[6, 9].Formula = "=STDEV.S(B2:B" + (qtyOrignal + 1) + ")";
                xlWorkSheet.Cells[7, 9].Formula = "=I6*SUBSTITUTE(H7,\"%\",)";
                xlWorkSheet.Cells[8, 9].Formula = "=I5+I7";

                for (int k = 2; k <= row; k++)
                {
                    xlWorkSheet.Cells[k, 4].Formula = "=ROUND($I$8,1)";
                }

                FormatCondition formatOrig = (FormatCondition)(xlWorkSheet.get_Range("B2:B" + row,
                Type.Missing).FormatConditions.Add(XlFormatConditionType.xlCellValue, XlFormatConditionOperator.xlGreater, "=$D$2", Type.Missing, Type.Missing, Type.Missing, Type.Missing, Type.Missing));
                formatOrig.Interior.Color = 0x000000FF;

                FormatCondition formatCopy = (FormatCondition)(xlWorkSheet.get_Range("C2:C" + row,
                Type.Missing).FormatConditions.Add(XlFormatConditionType.xlCellValue, XlFormatConditionOperator.xlLess, "=$D$2", Type.Missing, Type.Missing, Type.Missing, Type.Missing, Type.Missing));
                formatCopy.Interior.Color = 0x000000FF;
                xlWorkSheet.Columns[8].ColumnWidth = 20;
                xlWorkSheet.Columns[9].ColumnWidth = 20;

                if (listCondition[0].isActivate && !listCondition[0].method.Equals("STC"))
                {
                    Excel.Series seriesOrig = seriesCollection.NewSeries();
                    seriesOrig.MarkerStyle = Excel.XlMarkerStyle.xlMarkerStyleCircle;
                    seriesOrig.Name = "Original";
                    seriesOrig.MarkerSize = 5;
                    seriesOrig.MarkerBackgroundColor = (int)Excel.XlRgbColor.rgbGreenYellow;
                    seriesOrig.MarkerForegroundColor = (int)Excel.XlRgbColor.rgbGreenYellow;
                    seriesOrig.XValues = xlWorkSheet.get_Range("A2", "A" + (qtyOrignal + 1));
                    seriesOrig.Values = xlWorkSheet.get_Range(rangeColumn[position] + "2", rangeColumn[position] + (qtyOrignal + 1));


                    Excel.Series seriesCopy = seriesCollection.NewSeries();
                    seriesCopy.MarkerStyle = Excel.XlMarkerStyle.xlMarkerStyleCircle;
                    seriesCopy.Name = "Copy";
                    seriesCopy.MarkerSize = 5;
                    seriesCopy.MarkerBackgroundColor = (int)Excel.XlRgbColor.rgbRed;
                    seriesCopy.MarkerForegroundColor = (int)Excel.XlRgbColor.rgbRed;
                    seriesCopy.XValues = xlWorkSheet.get_Range("A2", "A" + (qtyOrignal + 1));
                    seriesCopy.Values = xlWorkSheet.get_Range(rangeColumn[position] + (qtyOrignal + 2), rangeColumn[position] + row);

                    Excel.Series seriesThreshold = seriesCollection.NewSeries();
                    seriesThreshold.MarkerStyle = Excel.XlMarkerStyle.xlMarkerStyleCircle;
                    seriesThreshold.Name = "Threshold";
                    seriesThreshold.MarkerSize = 5;
                    seriesThreshold.MarkerBackgroundColor = (int)Excel.XlRgbColor.rgbLightGray;
                    seriesThreshold.MarkerForegroundColor = (int)Excel.XlRgbColor.rgbLightGray;
                    seriesThreshold.XValues = xlWorkSheet.get_Range("A2", "A" + row);
                    seriesThreshold.Values = xlWorkSheet.get_Range("D2", "D" + row);
                    position++;

                }
                if (listCondition[1].isActivate && !listCondition[1].method.Equals("STC"))
                {
                    Excel.Series seriesOrig = seriesCollection.NewSeries();
                    seriesOrig.MarkerStyle = Excel.XlMarkerStyle.xlMarkerStyleCircle;
                    seriesOrig.Name = "Original";
                    seriesOrig.MarkerSize = 5;
                    seriesOrig.MarkerBackgroundColor = (int)Excel.XlRgbColor.rgbGreenYellow;
                    seriesOrig.MarkerForegroundColor = (int)Excel.XlRgbColor.rgbGreenYellow;
                    seriesOrig.XValues = xlWorkSheet.get_Range("A2", "A" + (qtyOrignal + 1));
                    seriesOrig.Values = xlWorkSheet.get_Range(rangeColumn[position] + "2", rangeColumn[position] + (qtyOrignal + 1));


                    Excel.Series seriesCopy = seriesCollection.NewSeries();
                    seriesCopy.MarkerStyle = Excel.XlMarkerStyle.xlMarkerStyleCircle;
                    seriesCopy.Name = "Copy";
                    seriesCopy.MarkerSize = 5;
                    seriesCopy.MarkerBackgroundColor = (int)Excel.XlRgbColor.rgbRed;
                    seriesCopy.MarkerForegroundColor = (int)Excel.XlRgbColor.rgbRed;
                    seriesCopy.XValues = xlWorkSheet.get_Range("A2", "A" + (qtyOrignal + 1));
                    seriesCopy.Values = xlWorkSheet.get_Range(rangeColumn[position] + (qtyOrignal + 2), rangeColumn[position] + row);


                    Excel.Series seriesThreshold = seriesCollection.NewSeries();
                    seriesThreshold.MarkerStyle = Excel.XlMarkerStyle.xlMarkerStyleCircle;
                    seriesThreshold.Name = "Threshold";
                    seriesThreshold.MarkerSize = 5;
                    seriesThreshold.MarkerBackgroundColor = (int)Excel.XlRgbColor.rgbLightGray;
                    seriesThreshold.MarkerForegroundColor = (int)Excel.XlRgbColor.rgbLightGray;
                    seriesThreshold.XValues = xlWorkSheet.get_Range("A2", "A" + row);
                    seriesThreshold.Values = xlWorkSheet.get_Range("D2", "D" + row);
                    position++;
                }
                if (listCondition[2].isActivate && !listCondition[2].method.Equals("STC"))
                {
                    Excel.Series seriesOrig = seriesCollection.NewSeries();
                    seriesOrig.MarkerStyle = Excel.XlMarkerStyle.xlMarkerStyleCircle;
                    seriesOrig.Name = "Original";
                    seriesOrig.MarkerSize = 5;
                    seriesOrig.MarkerBackgroundColor = (int)Excel.XlRgbColor.rgbGreenYellow;
                    seriesOrig.MarkerForegroundColor = (int)Excel.XlRgbColor.rgbGreenYellow;
                    seriesOrig.XValues = xlWorkSheet.get_Range("A2", "A" + (qtyOrignal + 1));
                    seriesOrig.Values = xlWorkSheet.get_Range(rangeColumn[position] + "2", rangeColumn[position] + (qtyOrignal + 1));


                    Excel.Series seriesCopy = seriesCollection.NewSeries();
                    seriesCopy.MarkerStyle = Excel.XlMarkerStyle.xlMarkerStyleCircle;
                    seriesCopy.Name = "Copy";
                    seriesCopy.MarkerSize = 5;
                    seriesCopy.MarkerBackgroundColor = (int)Excel.XlRgbColor.rgbRed;
                    seriesCopy.MarkerForegroundColor = (int)Excel.XlRgbColor.rgbRed;
                    seriesCopy.XValues = xlWorkSheet.get_Range("A2", "A" + (qtyOrignal + 1));
                    seriesCopy.Values = xlWorkSheet.get_Range(rangeColumn[position] + (qtyOrignal + 2), rangeColumn[position] + row);


                    Excel.Series seriesThreshold = seriesCollection.NewSeries();
                    seriesThreshold.MarkerStyle = Excel.XlMarkerStyle.xlMarkerStyleCircle;
                    seriesThreshold.Name = "Threshold";
                    seriesThreshold.MarkerSize = 5;
                    seriesThreshold.MarkerBackgroundColor = (int)Excel.XlRgbColor.rgbLightGray;
                    seriesThreshold.MarkerForegroundColor = (int)Excel.XlRgbColor.rgbLightGray;
                    seriesThreshold.XValues = xlWorkSheet.get_Range("A2", "A" + row);
                    seriesThreshold.Values = xlWorkSheet.get_Range("D2", "D" + row);
                    position++;
                }
                if (listCondition[3].isActivate && !listCondition[3].method.Equals("STC"))
                {
                    Excel.Series seriesOrig = seriesCollection.NewSeries();
                    seriesOrig.MarkerStyle = Excel.XlMarkerStyle.xlMarkerStyleCircle;
                    seriesOrig.Name = "Original";
                    seriesOrig.MarkerSize = 5;
                    seriesOrig.MarkerBackgroundColor = (int)Excel.XlRgbColor.rgbGreenYellow;
                    seriesOrig.MarkerForegroundColor = (int)Excel.XlRgbColor.rgbGreenYellow;
                    seriesOrig.XValues = xlWorkSheet.get_Range("A2", "A" + (qtyOrignal + 1));
                    seriesOrig.Values = xlWorkSheet.get_Range(rangeColumn[position] + "2", rangeColumn[position] + (qtyOrignal + 1));


                    Excel.Series seriesCopy = seriesCollection.NewSeries();
                    seriesCopy.MarkerStyle = Excel.XlMarkerStyle.xlMarkerStyleCircle;
                    seriesCopy.Name = "Copy";
                    seriesCopy.MarkerSize = 5;
                    seriesCopy.MarkerBackgroundColor = (int)Excel.XlRgbColor.rgbRed;
                    seriesCopy.MarkerForegroundColor = (int)Excel.XlRgbColor.rgbRed;
                    seriesCopy.XValues = xlWorkSheet.get_Range("A2", "A" + (qtyOrignal + 1));
                    seriesCopy.Values = xlWorkSheet.get_Range(rangeColumn[position] + (qtyOrignal + 2), rangeColumn[position] + row);


                    Excel.Series seriesThreshold = seriesCollection.NewSeries();
                    seriesThreshold.MarkerStyle = Excel.XlMarkerStyle.xlMarkerStyleCircle;
                    seriesThreshold.Name = "Threshold";
                    seriesThreshold.MarkerSize = 5;
                    seriesThreshold.MarkerBackgroundColor = (int)Excel.XlRgbColor.rgbLightGray;
                    seriesThreshold.MarkerForegroundColor = (int)Excel.XlRgbColor.rgbLightGray;
                    seriesThreshold.XValues = xlWorkSheet.get_Range("A2", "A" + row);
                    seriesThreshold.Values = xlWorkSheet.get_Range("D2", "D" + row);
                    position++;
                }
                if (listCondition[4].isActivate && !listCondition[4].method.Equals("STC"))
                {
                    Excel.Series seriesOrig = seriesCollection.NewSeries();
                    seriesOrig.MarkerStyle = Excel.XlMarkerStyle.xlMarkerStyleCircle;
                    seriesOrig.Name = "Original";
                    seriesOrig.MarkerSize = 5;
                    seriesOrig.MarkerBackgroundColor = (int)Excel.XlRgbColor.rgbGreenYellow;
                    seriesOrig.MarkerForegroundColor = (int)Excel.XlRgbColor.rgbGreenYellow;
                    seriesOrig.XValues = xlWorkSheet.get_Range("A2", "A" + (qtyOrignal + 1));
                    seriesOrig.Values = xlWorkSheet.get_Range(rangeColumn[position] + "2", rangeColumn[position] + (qtyOrignal + 1));


                    Excel.Series seriesCopy = seriesCollection.NewSeries();
                    seriesCopy.MarkerStyle = Excel.XlMarkerStyle.xlMarkerStyleCircle;
                    seriesCopy.Name = "Copy";
                    seriesCopy.MarkerSize = 5;
                    seriesCopy.MarkerBackgroundColor = (int)Excel.XlRgbColor.rgbRed;
                    seriesCopy.MarkerForegroundColor = (int)Excel.XlRgbColor.rgbRed;
                    seriesCopy.XValues = xlWorkSheet.get_Range("A2", "A" + (qtyOrignal + 1));
                    seriesCopy.Values = xlWorkSheet.get_Range(rangeColumn[position] + (qtyOrignal + 2), rangeColumn[position] + row);

                    Excel.Series seriesThreshold = seriesCollection.NewSeries();
                    seriesThreshold.MarkerStyle = Excel.XlMarkerStyle.xlMarkerStyleCircle;
                    seriesThreshold.Name = "Threshold";
                    seriesThreshold.MarkerSize = 5;
                    seriesThreshold.MarkerBackgroundColor = (int)Excel.XlRgbColor.rgbLightGray;
                    seriesThreshold.MarkerForegroundColor = (int)Excel.XlRgbColor.rgbLightGray;
                    seriesThreshold.XValues = xlWorkSheet.get_Range("A2", "A" + row);
                    seriesThreshold.Values = xlWorkSheet.get_Range("D2", "D" + row);
                    position++;
                }
                if (i != cbbDeviceName.Items.Count - 1)
                {
                    xlWorkBook.Worksheets.Add(After: xlWorkBook.Sheets[xlWorkBook.Sheets.Count]);
                }


            }

            String fileName = txtFolderPath.Text + @"\" + "Data_Set";

            try
            {
                xlWorkBook.SaveAs(fileName, Excel.XlFileFormat.xlWorkbookDefault, Missing.Value, Missing.Value, false, false, Excel.XlSaveAsAccessMode.xlNoChange, Excel.XlSaveConflictResolution.xlUserResolution, true, Missing.Value, Missing.Value, Missing.Value);

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            Marshal.ReleaseComObject(xlWorkSheet);
            xlWorkBook.Close(true, misValue, misValue);
            xlApp.Quit();
            MessageBox.Show("Excel file " + fileName + " has been created.");



            Marshal.ReleaseComObject(xlWorkBook);
            Marshal.ReleaseComObject(xlApp);

            progressExport.Visible = false;
        }

        private double GetThreshold(List<Result> listSave, String type, int qtyOrig, int qtyCopy)
        {
            List<double> listData = new List<double>();
            for (int i = 0; i < qtyOrig; i++)
            {
                if (listCondition[0].isActivate && !listCondition[0].method.Equals("STC"))
                {
                    if(Double.Parse(listSave[i].ResultZone1) != -1)
                    {
                        listData.Add(Double.Parse(listSave[i].ResultZone1));
                    }
                   
                }
                if (listCondition[1].isActivate && !listCondition[1].method.Equals("STC"))
                {
                    if (Double.Parse(listSave[i].ResultZone2) != -1)
                    {
                        listData.Add(Double.Parse(listSave[i].ResultZone2));
                    }
                }
                if (listCondition[2].isActivate && !listCondition[2].method.Equals("STC"))
                {
                    if (Double.Parse(listSave[i].ResultZone3) != -1)
                    {
                        listData.Add(Double.Parse(listSave[i].ResultZone3));
                    }
                }
                if (listCondition[3].isActivate && !listCondition[3].method.Equals("STC"))
                {
                    if (Double.Parse(listSave[i].ResultZone4) != -1)
                    {
                        listData.Add(Double.Parse(listSave[i].ResultZone4));
                    }
                }
                if (listCondition[4].isActivate && !listCondition[4].method.Equals("STC"))
                {
                    if (Double.Parse(listSave[i].ResultZone5) != -1)
                    {
                        listData.Add(Double.Parse(listSave[i].ResultZone5));
                    }
                }
            }

            double threshold = Math.Round(listData.StandardDeviation()*3 + CalculateMean(listData, listData.Count),1);

            return threshold;

        }
        private DataTable GetDataTable(List<Result> listSave, Device device, int qtyOrig, int qtyCopy)
        {
            DataTable dt = new DataTable();

            for (int i = 0; i < listCondition.Count; i++)
            {
                if (listCondition[i].isActivate && !listCondition[i].Equals("STC"))
                {
                    dt.Columns.Add();
                    dt.Columns.Add();
                    dt.Columns.Add();
                }

            }
            dt.Columns.Add();
            dt.Columns.Add();
            int column = 1;
            //Adding the Rows.
            dt.Rows.Add();
            dt.Rows[0][0] = "Statistic ID";
            for (int i = 0; i < listCondition.Count; i++)
            {
                if (listCondition[i].isActivate && !listCondition[i].method.Equals("STC"))
                {
                    dt.Rows[0][column] = "Zone Result Zone " + (i + 1);
                    dt.Rows[0][column + 1] = "Type";
                    dt.Rows[0][column + 2] = "Threshold Zone " + (i + 1);
                    column += 3;
                }

            }

            dt.Rows[0][column] = "Device Model";



            int index = 1;
     

            for (int i = 0; i < qtyOrig; i++)
            {
                column = 0;
                dt.Rows.Add();
                dt.Rows[dt.Rows.Count - 1][column] = index;
                index++;
                column++;
                if (listCondition[0].isActivate && !listCondition[0].method.Equals("STC"))
                {
                    if (Double.Parse(listSave[i].ResultZone1) != -1)
                    {
                        dt.Rows[dt.Rows.Count - 1][column] = Double.Parse(listSave[i].ResultZone1);
                    }
                    else
                    {
                        dt.Rows[dt.Rows.Count - 1][column] = "";
                    }

                    dt.Rows[dt.Rows.Count - 1][column + 1] = "Original";
                    dt.Rows[dt.Rows.Count - 1][column + 2] = GetThreshold(listSave, "Thershold", qtyOrig, qtyCopy);
                    column += 3;
                }
                if (listCondition[1].isActivate && !listCondition[1].method.Equals("STC"))
                {
                   // dt.Rows[dt.Rows.Count - 1][column] = Double.Parse(listSave[i].ResultZone2);
                    if (Double.Parse(listSave[i].ResultZone2) != -1)
                    {
                        dt.Rows[dt.Rows.Count - 1][column] = Double.Parse(listSave[i].ResultZone2);
                    }
                    else
                    {
                        dt.Rows[dt.Rows.Count - 1][column] = "";
                    }
                    dt.Rows[dt.Rows.Count - 1][column + 1] = "Original";
                    dt.Rows[dt.Rows.Count - 1][column + 2] = GetThreshold(listSave, "Thershold", qtyOrig, qtyCopy);
                    column += 3;
                }
                if (listCondition[2].isActivate && !listCondition[2].method.Equals("STC"))
                {
                   // dt.Rows[dt.Rows.Count - 1][column] = Double.Parse(listSave[i].ResultZone3);
                    if (Double.Parse(listSave[i].ResultZone3) != -1)
                    {
                        dt.Rows[dt.Rows.Count - 1][column] = Double.Parse(listSave[i].ResultZone3);
                    }
                    else
                    {
                        dt.Rows[dt.Rows.Count - 1][column] = "";
                    }
                    dt.Rows[dt.Rows.Count - 1][column + 1] = "Original";
                    dt.Rows[dt.Rows.Count - 1][column + 2] = GetThreshold(listSave, "Thershold", qtyOrig, qtyCopy);
                    column += 3;
                }
                if (listCondition[3].isActivate && !listCondition[3].method.Equals("STC"))
                {
                    dt.Rows[dt.Rows.Count - 1][column] = Double.Parse(listSave[i].ResultZone4);
                    if (Double.Parse(listSave[i].ResultZone4) != -1)
                    {
                        dt.Rows[dt.Rows.Count - 1][column] = Double.Parse(listSave[i].ResultZone4);
                    }
                    else
                    {
                        dt.Rows[dt.Rows.Count - 1][column] = "";
                    }

                    dt.Rows[dt.Rows.Count - 1][column + 1] = "Original";
                    dt.Rows[dt.Rows.Count - 1][column + 2] = GetThreshold(listSave, "Thershold", qtyOrig, qtyCopy);
                    column += 3;
                }
                if (listCondition[4].isActivate && !listCondition[4].method.Equals("STC"))
                {
                    //  dt.Rows[dt.Rows.Count - 1][column] = Double.Parse(listSave[i].ResultZone5);
                    if (Double.Parse(listSave[i].ResultZone5) != -1)
                    {
                        dt.Rows[dt.Rows.Count - 1][column] = Double.Parse(listSave[i].ResultZone5);
                    }
                    else
                    {
                        dt.Rows[dt.Rows.Count - 1][column] = "";
                    }
                    dt.Rows[dt.Rows.Count - 1][column + 1] = "Original";
                    dt.Rows[dt.Rows.Count - 1][column + 2] = GetThreshold(listSave, "Thershold", qtyOrig, qtyCopy);
                    column += 3;
                }
                dt.Rows[dt.Rows.Count - 1][column] = device.code + " - " + device.name;
            }

            for (int i = qtyOrig; i < qtyOrig + qtyCopy; i++)
            {
                column = 0;
                dt.Rows.Add();
                dt.Rows[dt.Rows.Count - 1][column] = index;
                index++;
                column++;
                if (listCondition[0].isActivate && !listCondition[0].method.Equals("STC"))
                {
                    //        dt.Rows[dt.Rows.Count - 1][column] = Double.Parse(listSave[i].ResultZone1);
                    if (Double.Parse(listSave[i].ResultZone1) != -1)
                    {
                        dt.Rows[dt.Rows.Count - 1][column] = Double.Parse(listSave[i].ResultZone1);
                    }
                    else
                    {
                        dt.Rows[dt.Rows.Count - 1][column] = "";
                    }
                    dt.Rows[dt.Rows.Count - 1][column + 1] = "Copy";
                    dt.Rows[dt.Rows.Count - 1][column + 2] = GetThreshold(listSave,"Thershold",qtyOrig,qtyCopy);
                    column += 3;
                }
                if (listCondition[1].isActivate && !listCondition[1].method.Equals("STC"))
                {
                 //   dt.Rows[dt.Rows.Count - 1][column] = Double.Parse(listSave[i].ResultZone2);
                    if (Double.Parse(listSave[i].ResultZone2) != -1)
                    {
                        dt.Rows[dt.Rows.Count - 1][column] = Double.Parse(listSave[i].ResultZone2);
                    }
                    else
                    {
                        dt.Rows[dt.Rows.Count - 1][column] = "";
                    }
                    dt.Rows[dt.Rows.Count - 1][column + 1] = "Copy";
                    dt.Rows[dt.Rows.Count - 1][column + 2] = GetThreshold(listSave, "Thershold", qtyOrig, qtyCopy);
                    column += 3;
                }
                if (listCondition[2].isActivate && !listCondition[2].method.Equals("STC"))
                {
                    // dt.Rows[dt.Rows.Count - 1][column] = Double.Parse(listSave[i].ResultZone3);
                    if (Double.Parse(listSave[i].ResultZone3) != -1)
                    {
                        dt.Rows[dt.Rows.Count - 1][column] = Double.Parse(listSave[i].ResultZone3);
                    }
                    else
                    {
                        dt.Rows[dt.Rows.Count - 1][column] = "";
                    }
                    dt.Rows[dt.Rows.Count - 1][column + 1] = "Copy";
                    dt.Rows[dt.Rows.Count - 1][column + 2] = GetThreshold(listSave, "Thershold", qtyOrig, qtyCopy);
                    column += 3;
                }
                if (listCondition[3].isActivate && !listCondition[3].method.Equals("STC"))
                {
                    // dt.Rows[dt.Rows.Count - 1][column] = Double.Parse(listSave[i].ResultZone4);
                    if (Double.Parse(listSave[i].ResultZone4) != -1)
                    {
                        dt.Rows[dt.Rows.Count - 1][column] = Double.Parse(listSave[i].ResultZone4);
                    }
                    else
                    {
                        dt.Rows[dt.Rows.Count - 1][column] = "";
                    }
                    dt.Rows[dt.Rows.Count - 1][column + 1] = "Copy";
                    dt.Rows[dt.Rows.Count - 1][column + 2] = GetThreshold(listSave, "Thershold", qtyOrig, qtyCopy);
                    column += 3;
                }
                if (listCondition[4].isActivate && !listCondition[4].method.Equals("STC"))
                {
                    //dt.Rows[dt.Rows.Count - 1][column] = Double.Parse(listSave[i].ResultZone5);
                    if (Double.Parse(listSave[i].ResultZone5) != -1)
                    {
                        dt.Rows[dt.Rows.Count - 1][column] = Double.Parse(listSave[i].ResultZone5);
                    }
                    else
                    {
                        dt.Rows[dt.Rows.Count - 1][column] = "";
                    }
                    dt.Rows[dt.Rows.Count - 1][column + 1] = "Copy";
                    dt.Rows[dt.Rows.Count - 1][column + 2] = GetThreshold(listSave, "Thershold", qtyOrig, qtyCopy);
                    column += 3;
                }
                dt.Rows[dt.Rows.Count - 1][column] = device.code + " - " + device.name;
            }
            return dt;

        }
       
      
       private void evaluationHistoryToolStripMenuItem_Click(object sender, EventArgs e) 
        {
            this.Hide();
            EvaluationHistory form = new EvaluationHistory();
            form.Show();
        }
        private void btnDraw_Click(object sender, EventArgs e)
        {
            cbbDeviceName_SelectedIndexChanged(sender, e);
        }
        private void btnReset_Click(object sender, EventArgs e)
        {
            Product product = ((Product)cbbProductName.SelectedItem);
            product.listCondition = db.GetConditionByProductID(product.id);
            for (int i = 0; i < product.listCondition.Count; i++)
            {
                if (product.listCondition[i].isActivate && product.listCondition[i].method.Equals("CDS"))
                {
                    txtThreshold.Text = product.listCondition[i].threshold.ToString();
                }
            }
            btnDraw_Click(sender, e);
        }
        private void btnUpdate_Click(object sender, EventArgs e)
        {
            List<Condition> listConditionUpdate = db.GetConditionByProductID(Int32.Parse(cbbProductName.SelectedValue.ToString()));
            for (int i = 0; i < listConditionUpdate.Count; i++)
            {
                if (listConditionUpdate[i].isActivate && listConditionUpdate[i].method.Equals("CDS"))
                {
                    db.UpdateConditionByID(listCondition[i].id, listCondition[i].isActivate, listCondition[i].method, listCondition[i].code, Double.Parse(txtThreshold.Text), listCondition[i].zoneResult);
                }
            }
            btnGetResult_Click(sender, e);
            listCondition = db.GetConditionByProductID(Int32.Parse(cbbProductName.SelectedValue.ToString()));
            cbbDeviceName_SelectedIndexChanged(sender, e);
            dgvResult.DataSource = null;
            dgvCalculateThreshold.DataSource = null;
            ChartCDS.ResetAutoValues();
            ChartCDS.Series.Clear();
            ChartCDS.Legends[0].Docking = Docking.Bottom;
        }
        private void collectResultToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.Hide();
            CollectResultForm form = new CollectResultForm();
            form.Show();
        }
        private void automationTestToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.Hide();
            AutomationTestForm form = new AutomationTestForm();
            form.Show();
        }

        private void jSONGeneratorToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.Hide();
            ParameterJSONForm form = new ParameterJSONForm();
            form.Show();
        }

        private void btnExportGraph_Click(object sender, EventArgs e)
        {
            Excel.Application xlApp;
            Excel.Workbook xlWorkBook;
            Excel.Worksheet xlWorkSheet = new Excel.Worksheet();
            object misValue = System.Reflection.Missing.Value;

            xlApp = new Excel.Application();

            progressExport.Minimum = 0;
            progressExport.Value = 0;
            progressExport.Visible = true;
            xlWorkBook = xlApp.Workbooks.Add(misValue);

            DirectoryInfo d = new DirectoryInfo(txtFolderPath.Text);
            FileInfo[] files = d.GetFiles("*.csv");
            progressExport.Maximum = files.Length/2 * 10;
            for (int i = 0; i < cbbDeviceName.Items.Count; i++)
            {
                progressExport.Value = i * 10;
                Device device = db.GetDeviceByID(Int32.Parse(((KeyValuePair<int, string>)cbbDeviceName.Items[i]).Key.ToString()));
                xlWorkSheet = (Excel.Worksheet)xlWorkBook.Worksheets.get_Item(xlWorkBook.Sheets.Count);
                int qtyOrignal = 0;
                int qtyCopy = 0;
                xlWorkSheet.Name = "Graph_" + device.code + "(" + device.location + ")";
                List<Result> listSave = new List<Result>();
                for (int k = 0; k < listResult.Count; k++)
                {
                    if (device.code.Equals(listResult[k].deviceID))
                    {
                        qtyCopy = listResult[k].listResultCopy.Count;
                        qtyOrignal = listResult[k].listResultOrig.Count;
                        for (int m = 0; m < listResult[k].listResultOrig.Count; m++)
                        {
                            listSave.Add(listResult[k].listResultOrig[m]);

                        }
                        for (int m = 0; m < listResult[k].listResultCopy.Count; m++)
                        {
                            listSave.Add(listResult[k].listResultCopy[m]);

                        }
                        k = listResult.Count;



                    }
                }
                DataTable dt = GetDataTable(listSave, device, qtyOrignal, qtyCopy);
                xlWorkSheet.Range[xlWorkSheet.Cells[1, 1], xlWorkSheet.Cells[dt.Rows.Count, dt.Columns.Count]].Value = Ultility.ParseDtToArray(dt);
                Excel.Range range = xlWorkSheet.Rows[1];
                range.Select();
                range.Font.Bold = true;
                range.Cells.Style.HorizontalAlignment = Excel.XlHAlign.xlHAlignCenter;

                int valid = 0;
                for (int m = 0; m < listCondition.Count; m++)
                {
                    if (listCondition[m].isActivate && !listCondition[m].method.Equals("STC"))
                    {
                        valid++;
                    }
                }
                Excel.ChartObjects xlChart = (Excel.ChartObjects)xlWorkSheet.ChartObjects(Type.Missing);
                Excel.ChartObject myChart = (Excel.ChartObject)xlChart.Add(400 * valid, 120, 700, 425);
                Excel.Chart chartPage = myChart.Chart;

                chartPage.Legend.Position = Excel.XlLegendPosition.xlLegendPositionBottom;
                myChart.Select();

                chartPage.ChartType = Excel.XlChartType.xlXYScatter;
                Excel.SeriesCollection seriesCollection = chartPage.SeriesCollection();
                int row = dt.Rows.Count;
                String[] rangeColumn = new String[] { "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M", "N", "O", "P" };
                int position = 0;
                xlWorkSheet.Cells[2, 8] = "False Negative";
                xlWorkSheet.Cells[3, 8] = "False Positive";
                xlWorkSheet.Cells[4, 8] = "PTA";
                xlWorkSheet.Cells[5, 8] = "Mean Orig";
                xlWorkSheet.Cells[6, 8] = "Std Dev Sample";
                xlWorkSheet.Cells[7, 8] = "300%";
                xlWorkSheet.Cells[8, 8] = "Threshold (Mean+ 300% of Std Dev)";

                xlWorkSheet.Cells[2, 9].Formula = "=COUNTIF(B2:B" + (qtyOrignal + 1) + ",\">\"&D2)-COUNTIF(B2:B" + (qtyOrignal + 1) + ",-1)";
                xlWorkSheet.Cells[3, 9].Formula = "=COUNTIF(B" + (qtyOrignal + 2) + ":B" + row + ",\"<\"&D2)-COUNTIF(B" + (qtyOrignal + 2) + ":B" + row + ",-1)-COUNTIF(B" + (qtyOrignal + 2) + ":B" + row + ",0)";
                xlWorkSheet.Cells[4, 9].Formula = "=COUNTIF(B2:B" + row + ",-1)";
                xlWorkSheet.Cells[5, 9].Formula = "=AVERAGEIF(B2:B" + (qtyOrignal + 1) + ",\"<>-1\")";
                xlWorkSheet.Cells[6, 9].Formula = "=STDEV.S(B2:B" + (qtyOrignal + 1) + ")";
                xlWorkSheet.Cells[7, 9].Formula = "=I6*SUBSTITUTE(H7,\"%\",)";
                xlWorkSheet.Cells[8, 9].Formula = "=I5+I7";

                for(int k=2; k <= row; k++)
                {
                    xlWorkSheet.Cells[k, 4].Formula = "=ROUND($I$8,1)";
                }

                FormatCondition formatOrig = (FormatCondition)(xlWorkSheet.get_Range("B2:B" + row,
                Type.Missing).FormatConditions.Add(XlFormatConditionType.xlCellValue, XlFormatConditionOperator.xlGreater, "=$D$2", Type.Missing, Type.Missing, Type.Missing, Type.Missing, Type.Missing));
                formatOrig.Interior.Color = 0x000000FF;

                FormatCondition formatCopy = (FormatCondition)(xlWorkSheet.get_Range("C2:C" + row,
                Type.Missing).FormatConditions.Add(XlFormatConditionType.xlCellValue, XlFormatConditionOperator.xlLess, "=$D$2", Type.Missing, Type.Missing, Type.Missing, Type.Missing, Type.Missing));
                formatCopy.Interior.Color = 0x000000FF;
                xlWorkSheet.Columns[8].ColumnWidth = 20;
                xlWorkSheet.Columns[9].ColumnWidth = 20;

                if (listCondition[0].isActivate && !listCondition[0].method.Equals("STC"))
                {
                    Excel.Series seriesOrig = seriesCollection.NewSeries();
                    seriesOrig.MarkerStyle = Excel.XlMarkerStyle.xlMarkerStyleCircle;
                    seriesOrig.Name = "Original";
                    seriesOrig.MarkerSize = 5;
                    seriesOrig.MarkerBackgroundColor = (int)Excel.XlRgbColor.rgbGreenYellow;
                    seriesOrig.MarkerForegroundColor = (int)Excel.XlRgbColor.rgbGreenYellow;
                    seriesOrig.XValues = xlWorkSheet.get_Range("A2", "A" + (qtyOrignal + 1));
                    seriesOrig.Values = xlWorkSheet.get_Range(rangeColumn[position] + "2", rangeColumn[position] + (qtyOrignal + 1));


                    Excel.Series seriesCopy = seriesCollection.NewSeries();
                    seriesCopy.MarkerStyle = Excel.XlMarkerStyle.xlMarkerStyleCircle;
                    seriesCopy.Name = "Copy";
                    seriesCopy.MarkerSize = 5;
                    seriesCopy.MarkerBackgroundColor = (int)Excel.XlRgbColor.rgbRed;
                    seriesCopy.MarkerForegroundColor = (int)Excel.XlRgbColor.rgbRed;
                    seriesCopy.XValues = xlWorkSheet.get_Range("A2", "A" + (qtyOrignal + 1));
                    seriesCopy.Values = xlWorkSheet.get_Range(rangeColumn[position] + (qtyOrignal + 2), rangeColumn[position] + row);

                    Excel.Series seriesThreshold = seriesCollection.NewSeries();
                    seriesThreshold.MarkerStyle = Excel.XlMarkerStyle.xlMarkerStyleCircle;
                    seriesThreshold.Name = "Threshold";
                    seriesThreshold.MarkerSize = 5;
                    seriesThreshold.MarkerBackgroundColor = (int)Excel.XlRgbColor.rgbLightGray;
                    seriesThreshold.MarkerForegroundColor = (int)Excel.XlRgbColor.rgbLightGray;
                    seriesThreshold.XValues = xlWorkSheet.get_Range("A2", "A" + row);
                    seriesThreshold.Values = xlWorkSheet.get_Range("D2", "D" + row);
                    position++;

                }
                if (listCondition[1].isActivate && !listCondition[1].method.Equals("STC"))
                {
                    Excel.Series seriesOrig = seriesCollection.NewSeries();
                    seriesOrig.MarkerStyle = Excel.XlMarkerStyle.xlMarkerStyleCircle;
                    seriesOrig.Name = "Original";
                    seriesOrig.MarkerSize = 5;
                    seriesOrig.MarkerBackgroundColor = (int)Excel.XlRgbColor.rgbGreenYellow;
                    seriesOrig.MarkerForegroundColor = (int)Excel.XlRgbColor.rgbGreenYellow;
                    seriesOrig.XValues = xlWorkSheet.get_Range("A2", "A" + (qtyOrignal + 1));
                    seriesOrig.Values = xlWorkSheet.get_Range(rangeColumn[position] + "2", rangeColumn[position] + (qtyOrignal + 1));


                    Excel.Series seriesCopy = seriesCollection.NewSeries();
                    seriesCopy.MarkerStyle = Excel.XlMarkerStyle.xlMarkerStyleCircle;
                    seriesCopy.Name = "Copy";
                    seriesCopy.MarkerSize = 5;
                    seriesCopy.MarkerBackgroundColor = (int)Excel.XlRgbColor.rgbRed;
                    seriesCopy.MarkerForegroundColor = (int)Excel.XlRgbColor.rgbRed;
                    seriesCopy.XValues = xlWorkSheet.get_Range("A2", "A" + (qtyOrignal + 1));
                    seriesCopy.Values = xlWorkSheet.get_Range(rangeColumn[position] + (qtyOrignal + 2), rangeColumn[position] + row);


                    Excel.Series seriesThreshold = seriesCollection.NewSeries();
                    seriesThreshold.MarkerStyle = Excel.XlMarkerStyle.xlMarkerStyleCircle;
                    seriesThreshold.Name = "Threshold";
                    seriesThreshold.MarkerSize = 5;
                    seriesThreshold.MarkerBackgroundColor = (int)Excel.XlRgbColor.rgbLightGray;
                    seriesThreshold.MarkerForegroundColor = (int)Excel.XlRgbColor.rgbLightGray;
                    seriesThreshold.XValues = xlWorkSheet.get_Range("A2", "A" + row);
                    seriesThreshold.Values = xlWorkSheet.get_Range("D2", "D" + row);
                    position++;
                }
                if (listCondition[2].isActivate && !listCondition[2].method.Equals("STC"))
                {
                    Excel.Series seriesOrig = seriesCollection.NewSeries();
                    seriesOrig.MarkerStyle = Excel.XlMarkerStyle.xlMarkerStyleCircle;
                    seriesOrig.Name = "Original";
                    seriesOrig.MarkerSize = 5;
                    seriesOrig.MarkerBackgroundColor = (int)Excel.XlRgbColor.rgbGreenYellow;
                    seriesOrig.MarkerForegroundColor = (int)Excel.XlRgbColor.rgbGreenYellow;
                    seriesOrig.XValues = xlWorkSheet.get_Range("A2", "A" + (qtyOrignal + 1));
                    seriesOrig.Values = xlWorkSheet.get_Range(rangeColumn[position] + "2", rangeColumn[position] + (qtyOrignal + 1));


                    Excel.Series seriesCopy = seriesCollection.NewSeries();
                    seriesCopy.MarkerStyle = Excel.XlMarkerStyle.xlMarkerStyleCircle;
                    seriesCopy.Name = "Copy";
                    seriesCopy.MarkerSize = 5;
                    seriesCopy.MarkerBackgroundColor = (int)Excel.XlRgbColor.rgbRed;
                    seriesCopy.MarkerForegroundColor = (int)Excel.XlRgbColor.rgbRed;
                    seriesCopy.XValues = xlWorkSheet.get_Range("A2", "A" + (qtyOrignal + 1));
                    seriesCopy.Values = xlWorkSheet.get_Range(rangeColumn[position] + (qtyOrignal + 2), rangeColumn[position] + row);


                    Excel.Series seriesThreshold = seriesCollection.NewSeries();
                    seriesThreshold.MarkerStyle = Excel.XlMarkerStyle.xlMarkerStyleCircle;
                    seriesThreshold.Name = "Threshold";
                    seriesThreshold.MarkerSize = 5;
                    seriesThreshold.MarkerBackgroundColor = (int)Excel.XlRgbColor.rgbLightGray;
                    seriesThreshold.MarkerForegroundColor = (int)Excel.XlRgbColor.rgbLightGray;
                    seriesThreshold.XValues = xlWorkSheet.get_Range("A2", "A" + row);
                    seriesThreshold.Values = xlWorkSheet.get_Range("D2", "D" + row);
                    position++;
                }
                if (listCondition[3].isActivate && !listCondition[3].method.Equals("STC"))
                {
                    Excel.Series seriesOrig = seriesCollection.NewSeries();
                    seriesOrig.MarkerStyle = Excel.XlMarkerStyle.xlMarkerStyleCircle;
                    seriesOrig.Name = "Original";
                    seriesOrig.MarkerSize = 5;
                    seriesOrig.MarkerBackgroundColor = (int)Excel.XlRgbColor.rgbGreenYellow;
                    seriesOrig.MarkerForegroundColor = (int)Excel.XlRgbColor.rgbGreenYellow;
                    seriesOrig.XValues = xlWorkSheet.get_Range("A2", "A" + (qtyOrignal + 1));
                    seriesOrig.Values = xlWorkSheet.get_Range(rangeColumn[position] + "2", rangeColumn[position] + (qtyOrignal + 1));


                    Excel.Series seriesCopy = seriesCollection.NewSeries();
                    seriesCopy.MarkerStyle = Excel.XlMarkerStyle.xlMarkerStyleCircle;
                    seriesCopy.Name = "Copy";
                    seriesCopy.MarkerSize = 5;
                    seriesCopy.MarkerBackgroundColor = (int)Excel.XlRgbColor.rgbRed;
                    seriesCopy.MarkerForegroundColor = (int)Excel.XlRgbColor.rgbRed;
                    seriesCopy.XValues = xlWorkSheet.get_Range("A2", "A" + (qtyOrignal + 1));
                    seriesCopy.Values = xlWorkSheet.get_Range(rangeColumn[position] + (qtyOrignal + 2), rangeColumn[position] + row);


                    Excel.Series seriesThreshold = seriesCollection.NewSeries();
                    seriesThreshold.MarkerStyle = Excel.XlMarkerStyle.xlMarkerStyleCircle;
                    seriesThreshold.Name = "Threshold";
                    seriesThreshold.MarkerSize = 5;
                    seriesThreshold.MarkerBackgroundColor = (int)Excel.XlRgbColor.rgbLightGray;
                    seriesThreshold.MarkerForegroundColor = (int)Excel.XlRgbColor.rgbLightGray;
                    seriesThreshold.XValues = xlWorkSheet.get_Range("A2", "A" + row);
                    seriesThreshold.Values = xlWorkSheet.get_Range("D2", "D" + row);
                    position++;
                }
                if (listCondition[4].isActivate && !listCondition[4].method.Equals("STC"))
                {
                    Excel.Series seriesOrig = seriesCollection.NewSeries();
                    seriesOrig.MarkerStyle = Excel.XlMarkerStyle.xlMarkerStyleCircle;
                    seriesOrig.Name = "Original";
                    seriesOrig.MarkerSize = 5;
                    seriesOrig.MarkerBackgroundColor = (int)Excel.XlRgbColor.rgbGreenYellow;
                    seriesOrig.MarkerForegroundColor = (int)Excel.XlRgbColor.rgbGreenYellow;
                    seriesOrig.XValues = xlWorkSheet.get_Range("A2", "A" + (qtyOrignal + 1));
                    seriesOrig.Values = xlWorkSheet.get_Range(rangeColumn[position] + "2", rangeColumn[position] + (qtyOrignal + 1));


                    Excel.Series seriesCopy = seriesCollection.NewSeries();
                    seriesCopy.MarkerStyle = Excel.XlMarkerStyle.xlMarkerStyleCircle;
                    seriesCopy.Name = "Copy";
                    seriesCopy.MarkerSize = 5;
                    seriesCopy.MarkerBackgroundColor = (int)Excel.XlRgbColor.rgbRed;
                    seriesCopy.MarkerForegroundColor = (int)Excel.XlRgbColor.rgbRed;
                    seriesCopy.XValues = xlWorkSheet.get_Range("A2", "A" + (qtyOrignal + 1));
                    seriesCopy.Values = xlWorkSheet.get_Range(rangeColumn[position] + (qtyOrignal + 2), rangeColumn[position] + row);

                    Excel.Series seriesThreshold = seriesCollection.NewSeries();
                    seriesThreshold.MarkerStyle = Excel.XlMarkerStyle.xlMarkerStyleCircle;
                    seriesThreshold.Name = "Threshold";
                    seriesThreshold.MarkerSize = 5;
                    seriesThreshold.MarkerBackgroundColor = (int)Excel.XlRgbColor.rgbLightGray;
                    seriesThreshold.MarkerForegroundColor = (int)Excel.XlRgbColor.rgbLightGray;
                    seriesThreshold.XValues = xlWorkSheet.get_Range("A2", "A" + row);
                    seriesThreshold.Values = xlWorkSheet.get_Range("D2", "D" + row);
                    position++;
                }
                if (i != cbbDeviceName.Items.Count - 1)
                {
                    xlWorkBook.Worksheets.Add(After: xlWorkBook.Sheets[xlWorkBook.Sheets.Count]);
                }


            }

            String fileName = txtFolderPath.Text + @"\" + "Graph";

            try
            {
                xlWorkBook.SaveAs(fileName, Excel.XlFileFormat.xlWorkbookDefault, Missing.Value, Missing.Value, false, false, Excel.XlSaveAsAccessMode.xlNoChange, Excel.XlSaveConflictResolution.xlUserResolution, true, Missing.Value, Missing.Value, Missing.Value);

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            Marshal.ReleaseComObject(xlWorkSheet);
            xlWorkBook.Close(true, misValue, misValue);
            xlApp.Quit();
            MessageBox.Show("Excel file " + fileName + " has been created.");



            Marshal.ReleaseComObject(xlWorkBook);
            Marshal.ReleaseComObject(xlApp);

            progressExport.Visible = false;
        }
    }
}
