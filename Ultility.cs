﻿using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ReadingResult
{
    public static class Ultility
    {
        static Database db = new Database();

        public static int FindPosition(List<String> listData, string keyword)
        {
            int position = -1;
            for (int i = 0; i < listData.Count; i++)
            {
                if (listData[i].Contains(keyword))
                {
                    position = i;
                    i = listData.Count;
                }
            }
            return position;
        }

        public static int FindColumn(List<List<String>> listData, string keyword)
        {
            int position = -1;
            for (int i = 0; i < listData[0].Count; i++)
            {
                if (listData[0][i].Equals(keyword))
                {
                    position = i;
                    i = listData[0].Count;
                }
            }
            return position;
        }

        public static int CountZone(String data)
        {
            int countZone = 0;
            int i = 0;
            while ((i = data.IndexOf("Zone", i)) != -1)
            {
                i += 4;
                countZone++;
            }
            return countZone;
        }

        public static String CheckTheFormat(String path, List<Condition> listCondition)
        {
            List<String> listData = Ultility.GetRawData(path);
            int countZone = 0;
            String[] input = listData[1].Split('"');
            int i = 0;
            while ((i = listData[1].IndexOf("Zone", i)) != -1)
            {
                i += 4;
                countZone++;
            }
            if (input.Length == (countZone * 2 + 3))
            {
                return ";";
            }
            else if (input.Length == (countZone * 2))
            {
                return ";";
            }
            else
            {
                bool isSpecial = false;
                for (int k = 0; k < listCondition.Count; k++)
                {
                    if (listCondition[k].isActivate && !listCondition[k].method.Equals("STC"))
                    {
                        isSpecial = true;
                        k = listCondition.Count;
                    }
                }
                if (isSpecial)
                {
                    return ";";
                }
                else
                {
                    if (listData[1].Contains("\";\""))

                    {
                        return "\";\"";
                    }
                    else if (listData[1].Contains("\";\""))
                    {
                        return "\\\";\\\"";
                    }
                    else
                    {
                        return ";";
                    }
                }


            }
        }

        public static List<String> GetTheValue(String input, String pattern)
        {
            List<String> listParseData = new List<String>();
            String[] array = input.Split(new String[] { pattern }, StringSplitOptions.None);
            for (int i = 0; i < array.Length; i++)
            {
                if (i == 0 && array[i].Length != 0)
                {
                    array[i] = array[i].Replace("\"", "");
                }
                listParseData.Add(array[i]);

            }
            return listParseData;
        }

        public static bool isNoreg(String path, String pattern)
        {
            List<String> listData = Ultility.GetRawData(path);
            for (int i = 0; i < listData.Count; i++)
            {
                if (listData[i].Contains("noreg@scryptotrace.com"))
                {
                    return true;
                }
            }
            return false;
        }

        public static List<Device> GetUnknownListDevice(String path, String pattern)
        {
            Dictionary<int, string> dictionaryEmail = Ultility.GetListDevice(path, pattern);
            List<Device> listUnknownDevice = new List<Device>();
            List<String> listData = Ultility.GetRawData(path);
            foreach (KeyValuePair<int, string> email in dictionaryEmail)
            {
                String code = Ultility.GetTheValue(listData[email.Key], pattern)[14].Replace("\"", "");
                Device device = db.GetDeviceByEmailAndCode(email.Value.Replace("\"", ""), code);
                if (device.id == -1)
                {
                    device.email = email.Value.Replace("\"", "");
                    device.code = code.Replace("\"", "");
                    device.location = Ultility.GetTheValue(listData[email.Key], pattern)[33].Replace("\"", "");
                    device.type = Ultility.GetTheValue(listData[email.Key], pattern)[15].Split('-')[0].Replace(" ", "").Replace("\"", "");
                    device.UDID = "";
                    device.isShowed = true;
                    listUnknownDevice.Add(device);
                }
            }
            return listUnknownDevice;
        }

        public static List<Device> GetKnownDevice(String path, String pattern)
        {
            Dictionary<int, string> dictionaryEmail = Ultility.GetListDevice(path, pattern);
            List<Device> listKnownDevice = new List<Device>();
            List<String> listData = Ultility.GetRawData(path);
            foreach (KeyValuePair<int, string> email in dictionaryEmail)
            {
                String code = Ultility.GetTheValue(listData[email.Key], pattern)[14].Replace("\"", "");
                Device device = db.GetDeviceByEmailAndCode(email.Value.Replace("\"", ""), code);
                if (device.id != -1)
                {
                    device.email = email.Value;
                    device.code = code;
                    device.location = Ultility.GetTheValue(listData[email.Key], pattern)[31].Replace("\"", "");
                    device.type = Ultility.GetTheValue(listData[email.Key], pattern)[15].Replace("\"", "");
                    device.UDID = "";
                    device.isShowed = false;
                    listKnownDevice.Add(device);
                }
            }
            return listKnownDevice;
        }

        public static Dictionary<int, string> GetListDevice(String path, String pattern)
        {
            List<String> listData = Ultility.GetRawData(path);
            Dictionary<int, String> dictionaryEmail = new Dictionary<int, string>();
            List<String> listEmail = new List<String>();

            for (int i = 1; i < listData.Count; i++)
            {
                if (!listEmail.Contains(Ultility.GetTheValue(listData[i], pattern)[5].Replace("\"", "")))
                {
                    listEmail.Add(Ultility.GetTheValue(listData[i], pattern)[5].Replace("\"", ""));
                    dictionaryEmail.Add(i, Ultility.GetTheValue(listData[i], pattern)[5]);
                }
            }

            return dictionaryEmail;
        }

        public static List<String> GetRawData(String path)
        {
            List<String> listData = new List<String>();
            try
            {
                StreamReader sr = new StreamReader(path, Encoding.GetEncoding(1250));

                while (!sr.EndOfStream)
                {
                    string strline = sr.ReadLine();
                    //  strline = strline.Trim().Trim('"');
                    string lineWithoutDouble = "";
                    if (strline.Split(';')[0].Length != 0)
                    {
                        string[] arrayStrline = strline.Split(';');
                        for (int i = 0; i < arrayStrline.Length; i++)
                        {
                            arrayStrline[i].Replace("\"", "");
                            if (i == arrayStrline.Length - 1)
                            {
                                lineWithoutDouble += arrayStrline[i];
                            }
                            else
                            {
                                lineWithoutDouble += arrayStrline[i] + ";";
                            }

                        }
                        listData.Add(lineWithoutDouble);

                    }
                }
                sr.Close();
            }
            catch (IOException ex)
            {
                MessageBox.Show(ex.Message);
            }
            return listData;

        }


        public static List<String> GetRawDataExport(String path, bool isGetTitle)
        {
            StreamReader sr = new StreamReader(path, Encoding.GetEncoding(1250));
            List<String> listData = new List<String>();
            try
            {

                while (!sr.EndOfStream)
                {
                    string strline = sr.ReadLine();
                    if (strline.Split(';')[0].Length != 0)
                    {
                        listData.Add(strline);

                    }
                }
                sr.Close();
            }
            catch (IOException ex)
            {
                MessageBox.Show(ex.Message);
            }
            if (isGetTitle)
            {
                return listData;
            }
            else
            {
                listData.RemoveAt(0);
                return listData;
            }
        }

        public static int GetRowNumber(String path, List<Condition> listCondition)
        {
            List<String> listData = Ultility.GetRawData(path);

            int countZone = 0;
            for (int i = 0; i < listCondition.Count; i++)
            {
                if (listCondition[i].isActivate)
                {
                    countZone++;
                }
            }
            return (listData.Count - 1) * countZone;

        }

        public static DataTable ParseDataRawToTable(FileInfo file, List<Condition> listCondition)
        {

            List<String> listData = Ultility.GetRawData(file.FullName);

            DataTable dt = new DataTable();

            for (int i = 0; i < listData[0].Split(';').Length; i++)
            {
                dt.Columns.Add();
            }
            String[] title = listData[0].Split(';');
            dt.Rows.Add();
            for (int k = 0; k < title.Length; k++)
            {
                dt.Rows[dt.Rows.Count - 1][k] = title[k];
            }
            for (int j = 1; j < listData.Count; j++)
            {
                dt.Rows.Add();
                List<String> data = Ultility.ReadDataInLine(listData[j], listCondition);
                for (int k = 0; k < data.Count; k++)
                {
                    dt.Rows[dt.Rows.Count - 1][k] = data[k].Replace("\"", "");
                }
            }
            return dt;

        }

        public static List<String> ReadDataInLine(String data, List<Condition> listCondition)
        {
            List<String> hashData = new List<String>();
            List<String> splitData = Ultility.ConvertDataStringToList(data);
            int countSpecial = 0;
            String[] conditions = splitData[43].Split('|');
            int countZone = conditions.Length - 1;
            for (int i = 0; i < conditions.Length; i++)
            {

                if (conditions[i].Contains("false"))
                {
                    countSpecial++;
                }
            }
            if (countSpecial == 0)
            {
                int start = -1;
                int end = -1;
                for (int i = 0; i < Ultility.FindPosition(splitData, "Zone=1") - 1; i++)
                {
                    hashData.Add(splitData[i]);
                }
                for (int i = 0; i < countZone; i++)
                {
                    String debugInfo = "";
                    if (countZone - i != 1)
                    {
                        start = Ultility.FindPosition(splitData, "Zone=" + (i + 1));
                        end = Ultility.FindPosition(splitData, "Zone=" + (i + 2)) - 1;
                    }
                    else
                    {
                        start = Ultility.FindPosition(splitData, "Zone=" + (i + 1));
                        end = splitData.Count;
                    }
                    for (int k = start; k < end - 1; k++)
                    {
                        if (k == (end - 1))
                        {
                            debugInfo += splitData[k];
                        }
                        else
                        {
                            debugInfo += splitData[k] + ";";
                        }

                    }
                    hashData.Add(debugInfo.Split(';')[1].Split('=')[1]);
                    hashData.Add(debugInfo.Replace("\"", "").Trim());
                }

            }
            else
            {
                int start;
                int end;
                for (int i = 0; i < 46; i++)
                {
                    hashData.Add(splitData[i]);
                }
                String sigma = "";
                for (int i = 46; i < 51; i++)
                {
                    if (i == 50)
                    {
                        sigma += splitData[i];
                    }
                    else
                    {
                        sigma += splitData[i] + ";";
                    }

                }
                hashData.Add(sigma.Replace("\"", "").Trim());
                for (int i = 51; i < 54; i++)
                {
                    hashData.Add(splitData[i]);
                }
                for (int i = 0; i < countZone; i++)
                {
                    String debugInfo = "";
                    if (countZone - i != 1)
                    {
                        start = Ultility.FindPosition(splitData, "Zone=" + (i + 1));
                        end = Ultility.FindPosition(splitData, "Zone=" + (i + 2)) - 1;
                    }
                    else
                    {
                        start = Ultility.FindPosition(splitData, "Zone=" + (i + 1));
                        end = splitData.Count;
                    }
                    for (int k = start; k < end - 1; k++)
                    {
                        if (k == (end - 1))
                        {
                            debugInfo += splitData[k];
                        }
                        else
                        {
                            debugInfo += splitData[k] + ";";
                        }
                    }
                    if (listCondition[i].method.Equals("STC"))
                    {
                        hashData.Add(debugInfo.Split(';')[1].Split('=')[1]);

                    }else if(listCondition[i].method.Equals("CD1") || listCondition[i].method.Equals("CD3"))
                    {
                        hashData.Add(debugInfo.Split(';')[1]);
                    }
                    else
                    {
                        List<String> fineSigmaValue = Ultility.ConvertDataStringToList(debugInfo);
                        int fineSigmaPosition = Ultility.FindPosition(fineSigmaValue, "FineSigma");
                        if (fineSigmaPosition != -1)
                        {
                            hashData.Add(fineSigmaValue[fineSigmaPosition].Replace("FineSigma=", ""));
                        }
                        else
                        {
                            hashData.Add("-1");
                        }
                    }

                    hashData.Add(debugInfo.Replace("\"", "").Trim());
                }
            }


            return hashData;
        }

        public static List<List<String>> GetCleanData(String path, List<Condition> listCondition)
        {
            List<String> listRawData = Ultility.GetRawData(path);

            List<List<String>> listData = new List<List<String>>();
            if (listRawData.Count > 0)
            {
                listData.Add(Ultility.ConvertDataStringToList(listRawData[0]));
                for (int i = 1; i < listRawData.Count; i++)
                {
                    listData.Add(Ultility.ReadDataInLine(listRawData[i], listCondition));
                }

            }

            return listData;
        }

        public static int GetFalseNegative(List<Result> listSave, List<Condition> listCondition)
        {
            int count = 0;

            for (int i = 0; i < listSave.Count; i++)
            {

                if (listCondition[0].isActivate && listCondition[0].method.Equals("CDS"))
                {
                    Double value = Double.Parse(listSave[i].ResultZone1);
                    if (value > listCondition[0].threshold)
                    {
                        count++;
                    }
                    else if (value == -1)
                    {
                        count--;
                    }
                }

                else if (listCondition[1].isActivate && listCondition[1].method.Equals("CDS"))
                {
                    Double value = Double.Parse(listSave[i].ResultZone2);
                    if (value > listCondition[1].threshold)
                    {
                        count++;
                    }
                    else if (value == -1)
                    {
                        count--;
                    }
                }

                else if (listCondition[2].isActivate && listCondition[2].method.Equals("CDS"))
                {
                    Double value = Double.Parse(listSave[i].ResultZone3);
                    if (value > listCondition[2].threshold)
                    {
                        count++;
                    }
                    else if (value == -1)
                    {
                        count--;
                    }
                }
                else if (listCondition[3].isActivate && listCondition[3].method.Equals("CDS"))
                {
                    Double value = Double.Parse(listSave[i].ResultZone4);
                    if (value > listCondition[3].threshold)
                    {
                        count++;
                    }
                    else if (value == -1)
                    {
                        count--;
                    }
                }
                else if (listCondition[4].isActivate && listCondition[4].method.Equals("CDS"))
                {
                    Double value = Double.Parse(listSave[i].ResultZone5);
                    if (value > listCondition[4].threshold)
                    {
                        count++;
                    }
                    else if (value == -1)
                    {
                        count--;
                    }
                }
            }
            return count;
        }

        public static int GetFalsePositive(List<Result> listSave, List<Condition> listCondition)
        {
            int count = 0;

            for (int i = 0; i < listSave.Count; i++)
            {

                if (listCondition[0].isActivate && listCondition[0].method.Equals("CDS"))
                {
                    Double value = Double.Parse(listSave[i].ResultZone1);
                    if (value < listCondition[0].threshold)
                    {
                        count++;
                    }
                    else if (value == -1)
                    {
                        count--;
                    }
                    else if (value == 0)
                    {
                        count--;
                    }
                }

                else if (listCondition[1].isActivate && listCondition[1].method.Equals("CDS"))
                {
                    Double value = Double.Parse(listSave[i].ResultZone2);
                    if (value < listCondition[1].threshold)
                    {
                        count++;
                    }
                    else if (value == -1)
                    {
                        count--;
                    }
                    else if (value == 0)
                    {
                        count--;
                    }
                }

                else if (listCondition[2].isActivate && listCondition[2].method.Equals("CDS"))
                {
                    Double value = Double.Parse(listSave[i].ResultZone3);
                    if (value < listCondition[2].threshold)
                    {
                        count++;
                    }
                    else if (value == -1)
                    {
                        count--;
                    }
                    else if (value == 0)
                    {
                        count--;
                    }
                }
                else if (listCondition[3].isActivate && listCondition[3].method.Equals("CDS"))
                {
                    Double value = Double.Parse(listSave[i].ResultZone4);
                    if (value < listCondition[3].threshold)
                    {
                        count++;
                    }
                    else if (value == -1)
                    {
                        count--;
                    }
                    else if (value == 0)
                    {
                        count--;
                    }
                }
                else if (listCondition[4].isActivate && listCondition[4].method.Equals("CDS"))
                {
                    Double value = Double.Parse(listSave[i].ResultZone5);
                    if (value < listCondition[4].threshold)
                    {
                        count++;
                    }
                    else if (value == -1)
                    {
                        count--;
                    }
                    else if (value == 0)
                    {
                        count--;
                    }
                }
            }
            return count;
        }

        public static int GetPTA(List<Result> listSave, List<Condition> listCondition)
        {
            int count = 0;

            for (int i = 0; i < listSave.Count; i++)
            {

                if (listCondition[0].isActivate && listCondition[0].method.Equals("CDS"))
                {
                    Double value = Double.Parse(listSave[i].ResultZone1);
                    if (value == 0)
                    {
                        count++;
                    }
                    else if (value == -1)
                    {
                        count++;
                    }
                }

                else if (listCondition[1].isActivate && listCondition[1].method.Equals("CDS"))
                {
                    Double value = Double.Parse(listSave[i].ResultZone2);
                    if (value == 0)
                    {
                        count++;
                    }
                    else if (value == -1)
                    {
                        count++;
                    }
                }

                else if (listCondition[2].isActivate && listCondition[2].method.Equals("CDS"))
                {
                    Double value = Double.Parse(listSave[i].ResultZone3);
                    if (value == 0)
                    {
                        count++;
                    }
                    else if (value == -1)
                    {
                        count++;
                    }
                }
                else if (listCondition[3].isActivate && listCondition[3].method.Equals("CDS"))
                {
                    Double value = Double.Parse(listSave[i].ResultZone4);
                    if (value == 0)
                    {
                        count++;
                    }
                    else if (value == -1)
                    {
                        count++;
                    }
                }
                else if (listCondition[4].isActivate && listCondition[4].method.Equals("CDS"))
                {
                    Double value = Double.Parse(listSave[i].ResultZone5);
                    if (value == 0)
                    {
                        count++;
                    }
                    else if (value == -1)
                    {
                        count++;
                    }
                }
            }
            return count;
        }

        public static List<String> ConvertDataStringToList(String data)
        {
            String[] array = data.Split(';');
            List<String> listString = new List<String>();
            for (int i = 0; i < array.Length; i++)
            {
                listString.Add(array[i]);
            }
            return listString;
        }

        public static List<Result> GetResultListFromDataRaw(List<List<String>> listDataRaw, List<Condition> listCondition, int startIndex, int endIndex, bool isReversed)
        {
            List<List<String>> listData;
            if (!isReversed)
            {
                listData = listDataRaw;
            }
            else
            {
                listData = new List<List<String>>();
                listData.Add(listDataRaw[0]);
                for (int i = listDataRaw.Count - 1; i > 0; i--)
                {
                    listData.Add(listDataRaw[i]);
                }
            }

            List<Result> listResult = new List<Result>();

            for (int correctR = startIndex; correctR < endIndex; correctR++)
            {
                Result result = new Result();
                result.ResultText = listData[correctR][Ultility.FindColumn(listData, "Result Text")].Replace("\"", "") + "(" + listData[correctR][Ultility.FindColumn(listData, "Result Code")].Replace("\"", "") + ")";
                if (listCondition[0].isActivate)
                {
                    int position = Ultility.FindColumn(listData, "Zone Result Zone 1");
                    if (listCondition[0].method.Equals("CDS"))
                    {
                        if (Double.Parse(listData[correctR][position]) != -1 || (Double.Parse(listData[correctR][position]) == -1 && listData[correctR][position].Contains("FineSigma")))
                        {
                            result.ResultZone1 = listData[correctR][position];
                            result.InfoZone1 = listData[correctR][position + 1];
                        }
                        else
                        {
                            result.ResultZone1 = listData[correctR][position + 2];
                            result.InfoZone1 = listData[correctR][position + 3].Replace("Zone 2", "Zone 1");
                        }

                    }
                    else if (listCondition[0].method.Equals("CD1") || listCondition[0].method.Equals("CD3"))
                    {
                        result.ResultZone1 = listData[correctR][position + 1].Split(';')[1].Replace("Code=", "");
                        result.InfoZone1 = listData[correctR][position + 1];
                    }
                    else
                    {
                        result.ResultZone1 = listData[correctR][position].Replace("\\", "").Replace("\"", "");
                        result.InfoZone1 = listData[correctR][position + 1];
                    }

                }
                if (listCondition[1].isActivate)
                {
                    int position = Ultility.FindColumn(listData, "Zone Result Zone 2");
                    if (listCondition[1].method.Equals("CDS"))
                    {
                        if (Double.Parse(listData[correctR][position]) != -1 || (Double.Parse(listData[correctR][position]) == -1 && listData[correctR][position].Contains("FineSigma")))
                        {
                            result.ResultZone2 = listData[correctR][position];
                            result.InfoZone2 = listData[correctR][position + 1];
                        }
                        else
                        {
                            result.ResultZone2 = listData[correctR][position + 2];
                            result.InfoZone2 = listData[correctR][position + 3].Replace("Zone 3", "Zone 2"); ;
                        }

                    }
                    else if (listCondition[1].method.Equals("CD1") || listCondition[1].method.Equals("CD3"))
                    {
                        result.ResultZone2 = listData[correctR][position + 1].Split(';')[1].Replace("Code=", "");
                        result.InfoZone2 = listData[correctR][position + 1];
                    }
                    else
                    {
                        result.ResultZone2 = listData[correctR][position].Replace("\\", "").Replace("\"", "");
                        result.InfoZone2 = listData[correctR][position + 1];
                    }

                }
                if (listCondition[2].isActivate)
                {
                    int position = Ultility.FindColumn(listData, "Zone Result Zone 3");
                    if (listCondition[2].method.Equals("CDS"))
                    {
                        if (Double.Parse(listData[correctR][position]) != -1 || (Double.Parse(listData[correctR][position]) == -1 && listData[correctR][position].Contains("FineSigma")))
                        {
                            result.ResultZone3 = listData[correctR][position];
                            result.InfoZone3 = listData[correctR][position + 1];
                        }
                        else
                        {
                            result.ResultZone3 = listData[correctR][position + 2];
                            result.InfoZone3 = listData[correctR][position + 3].Replace("Zone 4", "Zone 3"); ;
                        }

                    }
                    else if (listCondition[2].method.Equals("CD1") || listCondition[2].method.Equals("CD3"))
                    {
                        result.ResultZone3 = listData[correctR][position + 1].Split(';')[1].Replace("Code=", "");
                        result.InfoZone3 = listData[correctR][position + 1];
                    }
                    else
                    {
                        result.ResultZone3 = listData[correctR][position].Replace("\\", "").Replace("\"", "");
                        result.InfoZone3 = listData[correctR][position + 1];
                    }


                }
                if (listCondition[3].isActivate)
                {
                    int position = Ultility.FindColumn(listData, "Zone Result Zone 4");
                    if (listCondition[3].method.Equals("CDS"))
                    {
                        if (Double.Parse(listData[correctR][position]) != -1 || (Double.Parse(listData[correctR][position]) == -1 && listData[correctR][position].Contains("FineSigma")))
                        {
                            result.ResultZone4 = listData[correctR][position];
                            result.InfoZone4 = listData[correctR][position + 1];
                        }
                        else
                        {
                            result.ResultZone4 = listData[correctR][position + 2];
                            result.InfoZone4 = listData[correctR][position + 3].Replace("Zone 5", "Zone 4");
                        }

                    }
                    else if (listCondition[3].method.Equals("CD1") || listCondition[3].method.Equals("CD3"))
                    {
                        result.ResultZone4 = listData[correctR][position + 1].Split(';')[1].Replace("Code=", "");
                        result.InfoZone4 = listData[correctR][position + 1];
                    }
                    else
                    {
                        result.ResultZone4 = listData[correctR][position].Replace("\\", "").Replace("\"", "");
                        result.InfoZone4 = listData[correctR][position + 1];
                    }

                }
                if (listCondition[4].isActivate)
                {
                    int position = Ultility.FindColumn(listData, "Zone Result Zone 5");
                    if (listCondition[4].method.Equals("CDS"))
                    {
                        if (Double.Parse(listData[correctR][position]) != -1 || (Double.Parse(listData[correctR][position]) == -1 && listData[correctR][position].Contains("FineSigma")))
                        {
                            result.ResultZone5 = listData[correctR][position];
                            result.InfoZone5 = listData[correctR][position + 1];
                        }
                        else
                        {
                            result.ResultZone5 = listData[correctR][position + 2];
                            result.InfoZone5 = listData[correctR][position + 3].Replace("Zone 6", "Zone 5");
                        }

                    }
                    else if (listCondition[4].method.Equals("CD1") || listCondition[4].method.Equals("CD3"))
                    {
                        result.ResultZone5 = listData[correctR][position + 1].Split(';')[1].Replace("Code=", "");
                        result.InfoZone5 = listData[correctR][position + 1];
                    }
                    else
                    {
                        result.ResultZone5 = listData[correctR][position].Replace("\\", "").Replace("\"", "");
                        result.InfoZone5 = listData[correctR][position + 1];
                    }

                }
                listResult.Add(result);
            }

            return listResult;
        }

        public static DataTable GetDtCSVSheetSTCZone(List<ResultForEvaluation> listSave, String zone)
        {
            DataTable dt = new DataTable();
            ResultForEvaluation evaluation = new ResultForEvaluation();
            for (int i = 0; i < listSave.Count; i++)
            {
                if (Int32.Parse(listSave[i].value) != -1)
                {
                    evaluation = listSave[i];
                    i = listSave.Count;
                }
            }
          //  int neededColumn = evaluation.info.Split(';').Length + 4;
            for (int i = 0; i < 8; i++)
            {
                dt.Columns.Add();
            }
            int column;
            //Adding the Rows.
            dt.Rows.Add();
            dt.Rows[0][0] = "Statistic ID";
            dt.Rows[0][1] = "Date and Time";
            dt.Rows[0][2] = "User Email";
            dt.Rows[0][3] = "User Firstname";
            dt.Rows[0][4] = "Product Name";
            dt.Rows[0][5] = "Result Text";
            dt.Rows[0][6] = "Zone Result " + zone.Split('-')[0].Trim();
            dt.Rows[0][7] = "Debug Info " + zone.Split('-')[0].Trim();

            for (int i = 0; i < listSave.Count; i++)
            {
                dt.Rows.Add();
                dt.Rows[dt.Rows.Count - 1][0] = listSave[i].statisticID.Replace("\"", "");
                dt.Rows[dt.Rows.Count - 1][1] = listSave[i].createdDate;
                dt.Rows[dt.Rows.Count - 1][2] = listSave[i].deviceEmail;
                dt.Rows[dt.Rows.Count - 1][3] = listSave[i].deviceName;
                dt.Rows[dt.Rows.Count - 1][4] = listSave[i].version;
                dt.Rows[dt.Rows.Count - 1][5] = listSave[i].productName;
                dt.Rows[dt.Rows.Count - 1][6] = listSave[i].resultText;
                dt.Rows[dt.Rows.Count - 1][7] = listSave[i].value;

                String[] infos = listSave[i].info.Split(';');
                column = 8;
                for (int j = 0; j < infos.Length - 1; j++)
                {
                    dt.Columns.Add();
                    dt.Columns.Add();
                    dt.Rows[dt.Rows.Count - 1][column] = infos[j].Split('=')[0];
                    dt.Rows[dt.Rows.Count - 1][column + 1] = infos[j].Split('=')[1];
                    column += 2;
                }
            }

            return dt;

        }

        public static DataTable GetDtEvaluationSheetSTCZone(List<ResultForEvaluation> listSave, List<ResultForEvaluation> listDrawChart, int edgeNumber, String zone)
        {
            DataTable dt = new DataTable();
            int neededColumn = edgeNumber * 5 + 11;
            for (int i = 0; i < neededColumn; i++)
            {
                dt.Columns.Add();
            }

            int edgeNo = 0;
            for (int k = 0; k < listSave.Count; k++)
            {
                if (Ultility.FindPosition(listSave[k].info.Split(';').ToList(), "E1") > -1)
                {
                    edgeNo++;
                    List<String> listData = listSave[k].info.Split(';').ToList();
                    for (int i = 0; i < listData.Count; i++)
                    {
                        if (listData[i].Contains("E" + edgeNo + "="))
                        {
                            edgeNo++;
                        }
                    }
                    k = listSave.Count;
                }

            }
            edgeNo--;
            //Adding the Rows.
            dt.Rows.Add();
            dt.Rows[0][0] = "No.";
            dt.Rows[0][1] = "Index";
            dt.Rows[0][2] = "Time";
            dt.Rows[0][3] = "Code";
            dt.Rows[0][4] = "Mov";
            dt.Rows[0][5] = "Zone Value";
            dt.Rows[0][6] = "Cal";
            dt.Rows[0][7] = "StdDev";
            dt.Rows[0][8] = "NoRL";
            dt.Rows[0][9] = "NoVL";
            dt.Rows[0][10] = "NoTL";

            int column = 11;
            for (int i = 0; i < edgeNumber; i++)
            {
                dt.Rows[0][column] = "E" + (i + 1);
                dt.Rows[0][column + 1] = "E" + (i + 1) + " Trigger";
                dt.Rows[0][column + 2] = "E" + (i + 1) + " Trigger";
                dt.Rows[0][column + 3] = "E" + (i + 1) + " Mov";
                dt.Rows[0][column + 4] = " Shifting";
                column += 5;
            }
            List<EvaluationDetail> listSaveDetail = new List<EvaluationDetail>();
            for (int i = 0; i < listSave.Count; i++)
            {
                if (!listSave[i].value.Equals("-1"))
                {
                    if (listSave[i].zone == Int32.Parse(zone.Split('-')[0].Trim()))
                    {
                        listSaveDetail.Add(HashInformation(listDrawChart[i].statisticID, listSave[i].info, true, edgeNo));
                    }
                }
                else
                {
                    if (listSave[i].zone == Int32.Parse(zone.Split('-')[0].Trim()))
                    {
                        listSaveDetail.Add(HashInformation(listDrawChart[i].statisticID, listSave[i].info, false, edgeNo));
                    }
                }

            }

            for (int i = 0; i < listSave.Count; i++)
            {
                dt.Rows.Add();
                dt.Rows[dt.Rows.Count - 1][0] = (i + 1);
                dt.Rows[dt.Rows.Count - 1][1] = listSave[i].statisticID.Replace("\"", "");
                dt.Rows[dt.Rows.Count - 1][2] = listSave[i].createdDate;
                dt.Rows[dt.Rows.Count - 1][3] = listSave[i].value;
                if (listSave[i].info.Split(';').Length > 3)
                {
                    dt.Rows[dt.Rows.Count - 1][4] = listSave[i].info.Split(';')[2].Split('=')[1];
                }
                else
                {
                    dt.Rows[dt.Rows.Count - 1][4] = -1;
                }
                dt.Rows[dt.Rows.Count - 1][5] = listSave[i].resultText;
                dt.Rows[dt.Rows.Count - 1][6] = listSaveDetail[i].cal;
                dt.Rows[dt.Rows.Count - 1][7] = listSaveDetail[i].stdDev;
                dt.Rows[dt.Rows.Count - 1][8] = listSaveDetail[i].NoRL;
                dt.Rows[dt.Rows.Count - 1][9] = listSaveDetail[i].NoVL; ;
                dt.Rows[dt.Rows.Count - 1][10] = listSaveDetail[i].NoTL;
                column = 11;
                for (int k = 0; k < listSaveDetail[i].listEdge.Count; k++)
                {
                    dt.Rows[dt.Rows.Count - 1][column] = listSaveDetail[i].listEdge[k].e;
                    dt.Rows[dt.Rows.Count - 1][column + 1] = listSaveDetail[i].listEdge[k].eTriggerNegative;
                    dt.Rows[dt.Rows.Count - 1][column + 2] = listSaveDetail[i].listEdge[k].eTriggerPositive;
                    dt.Rows[dt.Rows.Count - 1][column + 3] = listSaveDetail[i].listEdge[k].eMov;
                    dt.Rows[dt.Rows.Count - 1][column + 4] = listSaveDetail[i].listEdge[k].shiftingPositive;
                    column += 5;
                }

            }

            return dt;
        }

        public static EvaluationDetail HashInformation(String statisticID, String info, bool isData, int edgeNo)
        {
            List<EdgeDetail> listEdge = new List<EdgeDetail>();
            EvaluationDetail eva = new EvaluationDetail();
            List<String> detail = Ultility.ConvertDataStringToList(info);
            if (isData)
            {
                eva.statisticID = statisticID;
                eva.cal = Int32.Parse(Math.Round(Double.Parse(detail[Ultility.FindPosition(detail, "Cal=")].Replace("Cal=", "")), 0).ToString());
                eva.stdDev = Int32.Parse(Math.Round(Double.Parse(detail[Ultility.FindPosition(detail, "StdDev=")].Replace("StdDev=", "")), 0).ToString());
                eva.NoRL = Int32.Parse(detail[Ultility.FindPosition(detail, "NoRL=")].Replace("NoRL=", ""));
                eva.NoVL = Int32.Parse(detail[Ultility.FindPosition(detail, "NoVL=")].Replace("NoVL=", ""));
                eva.NoTL = Int32.Parse(detail[Ultility.FindPosition(detail, "NoTL=")].Replace("NoTL=", ""));
                if(Ultility.FindPosition(detail, "E1") != -1)
                {
                    for (int i = Ultility.FindPosition(detail, "E1"); i < detail.Count - 1; i += 5)
                    {
                        int pos = listEdge.Count + 1;
                        if (detail[i].Split('=')[0].Equals("E" + pos))
                        {
                            EdgeDetail edge = new EdgeDetail(Int32.Parse(detail[i].Replace("E" + pos + "=", "")), Int32.Parse(detail[i + 1].Replace("T1=", "")) * (-1), Int32.Parse(detail[i + 1].Replace("T1=", "")), detail[i + 3].Replace("Mov=", ""), Int32.Parse(detail[i + 2].Replace("T2=", "")));
                            edge.edgeName = "E" + pos;
                            listEdge.Add(edge);
                        }
                    }
                }
                else
                {
                    for (int i = 0; i < edgeNo; i ++)
                    {
                        EdgeDetail edge = new EdgeDetail(0, 0, 0, "0,00", 0);
                        edge.edgeName = "E" + (i+1);
                        listEdge.Add(edge);
                    }
                }
               
                eva.listEdge = listEdge;
            }
            else
            {
                eva.statisticID = statisticID;
                if (info.Contains("Cal"))
                {
                    eva.cal = Int32.Parse(Math.Round(Double.Parse(detail[Ultility.FindPosition(detail, "Cal=")].Replace("Cal=", "")), 0).ToString());
                    eva.stdDev = Int32.Parse(Math.Round(Double.Parse(detail[Ultility.FindPosition(detail, "StdDev=")].Replace("StdDev=", "")), 0).ToString());
                }
                else
                {
                    eva.cal = 0;
                    eva.stdDev = 0;
                }

                eva.NoRL = 0;
                eva.NoVL = 0;
                eva.NoTL = 0;
                if(Ultility.FindPosition(detail, "E1") != -1)
                {
                    for (int i = 0; i < edgeNo; i++)
                    {
                        int pos = listEdge.Count + 1;
                        EdgeDetail edge = new EdgeDetail(0, 0, 0, "0,00", 0);
                        edge.edgeName = "E" + pos;
                        listEdge.Add(edge);
                    }
                }
                else
                {
                    for (int i = 0; i < edgeNo; i++)
                    {
                        EdgeDetail edge = new EdgeDetail(0, 0, 0, "0,00", 0);
                        edge.edgeName = "E" + (i + 1);
                        listEdge.Add(edge);
                    }
                }
             
                eva.listEdge = listEdge;
            }

            return eva;
        }

        public static DataTable GetDtCalibrationSheetSTCZone(List<ResultForEvaluation> listSave)
        {
            DataTable dt = new DataTable();

            for (int i = 0; i < 11; i++)
            {
                dt.Columns.Add();
            }
            //Adding the Rows.
            dt.Rows.Add();
            dt.Rows[0][0] = "No.";
            dt.Rows[0][1] = "Index";
            dt.Rows[0][2] = "Date and Time";
            dt.Rows[0][3] = "Result";
            dt.Rows[0][4] = "Zone Result";
            dt.Rows[0][5] = "Center";
            dt.Rows[0][6] = "StdDev";
            dt.Rows[0][7] = "NoRL";
            dt.Rows[0][8] = "NoVL";
            dt.Rows[0][9] = "NoTL";
            dt.Rows[0][10] = "Cal";

            for (int i = 0; i < listSave.Count; i++)
            {
                dt.Rows.Add();
                dt.Rows[dt.Rows.Count - 1][0] = (i + 1);
                dt.Rows[dt.Rows.Count - 1][1] = listSave[i].statisticID.Replace("\"", "");
                dt.Rows[dt.Rows.Count - 1][2] = listSave[i].createdDate;
                dt.Rows[dt.Rows.Count - 1][3] = listSave[i].resultText;
                dt.Rows[dt.Rows.Count - 1][4] = listSave[i].value;
                dt.Rows[dt.Rows.Count - 1][5] = 0;
                if (listSave[i].info.Split(';').Length > 3)
                {
                    dt.Rows[dt.Rows.Count - 1][6] = Math.Round(Double.Parse(listSave[i].info.Split(';')[4].Split('=')[1]), 0);
                }
                else
                {
                    dt.Rows[dt.Rows.Count - 1][6] = 0;
                }

                if (listSave[i].info.Split(';').Length > 6)
                {
                    dt.Rows[dt.Rows.Count - 1][7] = Math.Round(Double.Parse(listSave[i].info.Split(';')[5].Split('=')[1]), 0);
                    dt.Rows[dt.Rows.Count - 1][8] = Math.Round(Double.Parse(listSave[i].info.Split(';')[6].Split('=')[1]), 0);
                    dt.Rows[dt.Rows.Count - 1][9] = Math.Round(Double.Parse(listSave[i].info.Split(';')[7].Split('=')[1]), 0);
                    dt.Rows[dt.Rows.Count - 1][10] = Math.Round(Double.Parse(listSave[i].info.Split(';')[3].Split('=')[1]), 0);
                }
                else
                {
                    dt.Rows[dt.Rows.Count - 1][7] = 0;
                    dt.Rows[dt.Rows.Count - 1][8] = 0;
                    dt.Rows[dt.Rows.Count - 1][9] = 0;
                    dt.Rows[dt.Rows.Count - 1][10] = 0;

                }


            }

            return dt;

        }

        public static DataTable GetDtEdge(List<ResultForEvaluation> listSave, int edge, string zone)
        {
            DataTable dt = new DataTable();
            int edgeNo = 0;
            for(int k = 0; k < listSave.Count; k++)
            {
                if (Ultility.FindPosition(listSave[k].info.Split(';').ToList(), "E1") > -1)
                {
                    edgeNo++;
                    List<String> listData = listSave[k].info.Split(';').ToList();
                    for(int i = 0;i < listData.Count; i++)
                    {
                        if (listData[i].Contains("E" + edgeNo + "=")){
                            edgeNo++;
                        }
                    }
                    k = listSave.Count;
                }
               
            }
            edgeNo--;
            List<EvaluationDetail> listSaveDetail = new List<EvaluationDetail>();   
            for (int i = 0; i < listSave.Count; i++)
            {
                if (!listSave[i].value.Equals("-1"))
                {
                    if (listSave[i].zone == Int32.Parse(zone.Split('-')[0].Trim()))
                    {
                        listSaveDetail.Add(Ultility.HashInformation(listSave[i].statisticID, listSave[i].info, true, edgeNo));
                    }
                }
                else
                {
                    if (listSave[i].zone == Int32.Parse(zone.Split('-')[0].Trim()))
                    {
                        listSaveDetail.Add(HashInformation(listSave[i].statisticID, listSave[i].info, false, edgeNo));
                    }
                }

            }

            for (int i = 0; i < 11; i++)
            {
                dt.Columns.Add();
            }
            //Adding the Rows.
            dt.Rows.Add();
            dt.Rows[0][0] = "No.";
            dt.Rows[0][1] = "Index";
            dt.Rows[0][2] = "Result";
            dt.Rows[0][3] = "Zone Result";
            dt.Rows[0][4] = "Center";
            dt.Rows[0][5] = "Trigger 1";
            dt.Rows[0][6] = "Trigger 2";
            dt.Rows[0][7] = "Shifting 1";
            dt.Rows[0][8] = "Shifting 2";
            dt.Rows[0][9] = "Cal";
            dt.Rows[0][10] = "E" + (edge + 1);

            for (int i = 0; i < listSave.Count; i++)
            {
                dt.Rows.Add();
                dt.Rows[dt.Rows.Count - 1][0] = (i + 1);
                dt.Rows[dt.Rows.Count - 1][1] = listSave[i].statisticID.Replace("\"", "");
                dt.Rows[dt.Rows.Count - 1][2] = listSave[i].resultText;
                dt.Rows[dt.Rows.Count - 1][3] = listSave[i].value;
                dt.Rows[dt.Rows.Count - 1][4] = 0;
                if (listSaveDetail[i].listEdge.Count > 0)
                {
                    dt.Rows[dt.Rows.Count - 1][5] = listSaveDetail[i].listEdge[edge].eTriggerPositive;
                    dt.Rows[dt.Rows.Count - 1][6] = listSaveDetail[i].listEdge[edge].eTriggerNegative;
                    dt.Rows[dt.Rows.Count - 1][7] = listSaveDetail[i].listEdge[edge].shiftingPositive;
                    dt.Rows[dt.Rows.Count - 1][8] = listSaveDetail[i].listEdge[edge].shiftingNegative;
                    dt.Rows[dt.Rows.Count - 1][9] = listSaveDetail[i].cal;
                    dt.Rows[dt.Rows.Count - 1][10] = listSaveDetail[i].listEdge[edge].e;
                }
             
            }

            return dt;
        }

        public static object[,] ParseDtToArray(DataTable dt)
        {
            object[,] array = new object[dt.Rows.Count, dt.Columns.Count];
            for (int k = 0; k < dt.Rows.Count; k++)
            {
                for (int j = 0; j < dt.Columns.Count; j++)
                {
                    array[k, j] = dt.Rows[k][j];
                }

            }
            return array;
        }


        public static List<Double> GetRangeThreshold(List<DataDrawChart> listOrig, List<DataDrawChart> listCopy, double threshold)
        {
            double aveCopy = 0;
            double aveOrig = 0;
            List<Double> listRange = new List<Double>();

            List<DataDrawChart> listEliminatedCopy = Ultility.EliminateAbnormal(listCopy, false, threshold);
            for (int i = 0; i < listEliminatedCopy.Count; i++)
            {
                aveCopy += listEliminatedCopy[i].pointValue;
            }
            aveCopy = Math.Round(aveCopy / listEliminatedCopy.Count, 1);

            List<DataDrawChart> listEminiatedOrig = Ultility.EliminateAbnormal(listOrig, true, threshold);
            for (int i = 0; i < listEminiatedOrig.Count; i++)
            {
                aveOrig += listEminiatedOrig[i].pointValue;
            }
            aveOrig = Math.Round(aveOrig / listEminiatedOrig.Count, 1);

            for (double i = aveOrig; i <= aveCopy; i += 0.1)
            {
                listRange.Add(Math.Round(i, 2));
            }
            return listRange;

        }

        public static List<DataDrawChart> EliminateAbnormal(List<DataDrawChart> listData, bool isOrig, double threshold)
        {
            if (threshold != 0)
            {
                List<DataDrawChart> ListEliminated = new List<DataDrawChart>();
                if (!isOrig)
                {
                    for (int i = 0; i < listData.Count; i++)
                    {
                        if ((listData[i].pointValue / threshold) * 100 <= 250)
                        {
                            ListEliminated.Add(listData[i]);
                        }
                    }
                }
                else
                {
                    for (int i = 0; i < listData.Count; i++)
                    {
                        if ((threshold / listData[i].pointValue) * 100 > 50)
                        {
                            ListEliminated.Add(listData[i]);
                        }
                    }
                }

                return ListEliminated;
            }
            else
            {
                return listData;
            }

        }

        public static int CalculateFalseNegative(List<DataDrawChart> listOrig, double threshold)
        {

            int count = 0;
            for (int i = 0; i < listOrig.Count; i++)
            {
                if (listOrig[i].pointValue > threshold)
                {
                    count++;
                }
                else if (listOrig[i].pointValue == -1)
                {
                    count--;
                }
            }
            return count;
        }
        public static int CalculateFalsePositive(List<DataDrawChart> listCopy, double threshold)
        {
            int count = 0;
            for (int i = 0; i < listCopy.Count; i++)
            {
                if (listCopy[i].pointValue < threshold)
                {
                    count++;
                }
                else if (listCopy[i].pointValue == 0)
                {
                    count--;
                }
                else if (listCopy[i].pointValue == -1)
                {
                    count--;
                }
            }
            return count;
        }
        public static int CalculatePTA(List<DataDrawChart> listOrig, List<DataDrawChart> listCopy)
        {
            int count = 0;
            for (int i = 0; i < listOrig.Count; i++)
            {
                if (listOrig[i].pointValue == -1)
                {
                    count++;
                }
            }
            for (int i = 0; i < listCopy.Count; i++)
            {
                if (listCopy[i].pointValue == -1)
                {
                    count++;
                }
            }
            return count;
        }

        public static double CalculateMeanF(List<DataDrawChart> listData)
        {

            double sum = 0;
            for (int i = 0; i < listData.Count; i++)
            {
                sum += listData[i].pointValue;
            }
            return Math.Round(sum / listData.Count, 2);
        }

        public static double CalculateMeanE(List<DataDrawChart> listData, bool isOrig, double threshold)
        {
            listData = Ultility.EliminateAbnormal(listData, isOrig, threshold);
            double sum = 0;
            for (int i = 0; i < listData.Count; i++)
            {
                sum += listData[i].pointValue;
            }
            return Math.Round(sum / listData.Count, 2);
        }
    }
}
