﻿namespace ReadingResult
{
    partial class SetupFileForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.cbbProductName = new System.Windows.Forms.ComboBox();
            this.label1 = new System.Windows.Forms.Label();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.btnUpdate = new System.Windows.Forms.Button();
            this.btnReset = new System.Windows.Forms.Button();
            this.groupBox7 = new System.Windows.Forms.GroupBox();
            this.txtCopyLight45Result = new System.Windows.Forms.TextBox();
            this.txtCopyLight45STC = new System.Windows.Forms.TextBox();
            this.txtCopyLight45Others = new System.Windows.Forms.TextBox();
            this.label22 = new System.Windows.Forms.Label();
            this.label19 = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.groupBox6 = new System.Windows.Forms.GroupBox();
            this.label23 = new System.Windows.Forms.Label();
            this.txtCopyOffice45Result = new System.Windows.Forms.TextBox();
            this.txtCopyOffice45STC = new System.Windows.Forms.TextBox();
            this.txtCopyOffice45Others = new System.Windows.Forms.TextBox();
            this.label20 = new System.Windows.Forms.Label();
            this.label17 = new System.Windows.Forms.Label();
            this.groupBox5 = new System.Windows.Forms.GroupBox();
            this.txtCopyOffice0Result = new System.Windows.Forms.TextBox();
            this.txtCopyOffice0STC = new System.Windows.Forms.TextBox();
            this.txtCopyOffice0Others = new System.Windows.Forms.TextBox();
            this.label24 = new System.Windows.Forms.Label();
            this.label21 = new System.Windows.Forms.Label();
            this.label18 = new System.Windows.Forms.Label();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.txtOrigLight45Result = new System.Windows.Forms.TextBox();
            this.txtOrigLight45STC = new System.Windows.Forms.TextBox();
            this.txtOrigLight45Others = new System.Windows.Forms.TextBox();
            this.label13 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.txtOrigOffice45Result = new System.Windows.Forms.TextBox();
            this.txtOrigOffice45STC = new System.Windows.Forms.TextBox();
            this.txtOrigOffice45Others = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.label7 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.txtOrigOffice0Others = new System.Windows.Forms.TextBox();
            this.txtOrigOffice0STC = new System.Windows.Forms.TextBox();
            this.txtOrigOffice0Result = new System.Windows.Forms.TextBox();
            this.label25 = new System.Windows.Forms.Label();
            this.label26 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.txtClmDeviceID = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.cbbSheetName = new System.Windows.Forms.ComboBox();
            this.groupBox1.SuspendLayout();
            this.groupBox7.SuspendLayout();
            this.groupBox6.SuspendLayout();
            this.groupBox5.SuspendLayout();
            this.groupBox4.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.SuspendLayout();
            // 
            // cbbProductName
            // 
            this.cbbProductName.BackColor = System.Drawing.Color.White;
            this.cbbProductName.Font = new System.Drawing.Font("Microsoft Sans Serif", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbbProductName.FormattingEnabled = true;
            this.cbbProductName.Location = new System.Drawing.Point(233, 62);
            this.cbbProductName.Margin = new System.Windows.Forms.Padding(4);
            this.cbbProductName.Name = "cbbProductName";
            this.cbbProductName.Size = new System.Drawing.Size(531, 37);
            this.cbbProductName.TabIndex = 2;
            this.cbbProductName.SelectedIndexChanged += new System.EventHandler(this.cbbProductName_SelectedIndexChanged);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(23, 74);
            this.label1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(173, 29);
            this.label1.TabIndex = 3;
            this.label1.Text = "Product Name:";
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.btnUpdate);
            this.groupBox1.Controls.Add(this.btnReset);
            this.groupBox1.Controls.Add(this.groupBox7);
            this.groupBox1.Controls.Add(this.groupBox6);
            this.groupBox1.Controls.Add(this.groupBox5);
            this.groupBox1.Controls.Add(this.groupBox4);
            this.groupBox1.Controls.Add(this.groupBox3);
            this.groupBox1.Controls.Add(this.groupBox2);
            this.groupBox1.Controls.Add(this.label25);
            this.groupBox1.Controls.Add(this.label26);
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Controls.Add(this.txtClmDeviceID);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this.cbbSheetName);
            this.groupBox1.Controls.Add(this.cbbProductName);
            this.groupBox1.Font = new System.Drawing.Font("Microsoft Sans Serif", 20.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox1.Location = new System.Drawing.Point(44, 15);
            this.groupBox1.Margin = new System.Windows.Forms.Padding(4);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Padding = new System.Windows.Forms.Padding(4);
            this.groupBox1.Size = new System.Drawing.Size(1665, 735);
            this.groupBox1.TabIndex = 3;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Setup File";
            // 
            // btnUpdate
            // 
            this.btnUpdate.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(129)))), ((int)(((byte)(213)))));
            this.btnUpdate.Font = new System.Drawing.Font("Microsoft Sans Serif", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnUpdate.ForeColor = System.Drawing.Color.White;
            this.btnUpdate.Location = new System.Drawing.Point(1381, 132);
            this.btnUpdate.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.btnUpdate.Name = "btnUpdate";
            this.btnUpdate.Size = new System.Drawing.Size(223, 50);
            this.btnUpdate.TabIndex = 35;
            this.btnUpdate.Text = "Update";
            this.btnUpdate.UseVisualStyleBackColor = false;
            this.btnUpdate.Click += new System.EventHandler(this.btnUpdate_Click);
            // 
            // btnReset
            // 
            this.btnReset.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(129)))), ((int)(((byte)(213)))));
            this.btnReset.Font = new System.Drawing.Font("Microsoft Sans Serif", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnReset.ForeColor = System.Drawing.Color.White;
            this.btnReset.Location = new System.Drawing.Point(1135, 132);
            this.btnReset.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.btnReset.Name = "btnReset";
            this.btnReset.Size = new System.Drawing.Size(219, 50);
            this.btnReset.TabIndex = 35;
            this.btnReset.Text = "Reset";
            this.btnReset.UseVisualStyleBackColor = false;
            this.btnReset.Click += new System.EventHandler(this.btnReset_Click);
            // 
            // groupBox7
            // 
            this.groupBox7.Controls.Add(this.txtCopyLight45Result);
            this.groupBox7.Controls.Add(this.txtCopyLight45STC);
            this.groupBox7.Controls.Add(this.txtCopyLight45Others);
            this.groupBox7.Controls.Add(this.label22);
            this.groupBox7.Controls.Add(this.label19);
            this.groupBox7.Controls.Add(this.label16);
            this.groupBox7.Font = new System.Drawing.Font("Microsoft Sans Serif", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox7.Location = new System.Drawing.Point(1135, 469);
            this.groupBox7.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.groupBox7.Name = "groupBox7";
            this.groupBox7.Padding = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.groupBox7.Size = new System.Drawing.Size(469, 244);
            this.groupBox7.TabIndex = 34;
            this.groupBox7.TabStop = false;
            this.groupBox7.Text = "Copy Lightbox 45 degree:";
            // 
            // txtCopyLight45Result
            // 
            this.txtCopyLight45Result.BackColor = System.Drawing.Color.White;
            this.txtCopyLight45Result.Location = new System.Drawing.Point(211, 47);
            this.txtCopyLight45Result.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.txtCopyLight45Result.Name = "txtCopyLight45Result";
            this.txtCopyLight45Result.Size = new System.Drawing.Size(235, 34);
            this.txtCopyLight45Result.TabIndex = 14;
            this.txtCopyLight45Result.TextChanged += new System.EventHandler(this.txtCopyLight45Result_TextChanged);
            // 
            // txtCopyLight45STC
            // 
            this.txtCopyLight45STC.BackColor = System.Drawing.Color.White;
            this.txtCopyLight45STC.Location = new System.Drawing.Point(211, 116);
            this.txtCopyLight45STC.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.txtCopyLight45STC.Name = "txtCopyLight45STC";
            this.txtCopyLight45STC.Size = new System.Drawing.Size(235, 34);
            this.txtCopyLight45STC.TabIndex = 16;
            this.txtCopyLight45STC.TextChanged += new System.EventHandler(this.txtCopyLight45STC_TextChanged);
            // 
            // txtCopyLight45Others
            // 
            this.txtCopyLight45Others.BackColor = System.Drawing.Color.White;
            this.txtCopyLight45Others.Location = new System.Drawing.Point(211, 185);
            this.txtCopyLight45Others.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.txtCopyLight45Others.Name = "txtCopyLight45Others";
            this.txtCopyLight45Others.Size = new System.Drawing.Size(235, 34);
            this.txtCopyLight45Others.TabIndex = 19;
            this.txtCopyLight45Others.TextChanged += new System.EventHandler(this.txtCopyLight45Others_TextChanged);
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label22.Location = new System.Drawing.Point(13, 54);
            this.label22.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(87, 29);
            this.label22.TabIndex = 22;
            this.label22.Text = "Result:";
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label19.Location = new System.Drawing.Point(13, 121);
            this.label19.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(129, 29);
            this.label19.TabIndex = 25;
            this.label19.Text = "STC Zone:";
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label16.Location = new System.Drawing.Point(13, 190);
            this.label16.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(152, 29);
            this.label16.TabIndex = 28;
            this.label16.Text = "Other Zones:";
            // 
            // groupBox6
            // 
            this.groupBox6.Controls.Add(this.label23);
            this.groupBox6.Controls.Add(this.txtCopyOffice45Result);
            this.groupBox6.Controls.Add(this.txtCopyOffice45STC);
            this.groupBox6.Controls.Add(this.txtCopyOffice45Others);
            this.groupBox6.Controls.Add(this.label20);
            this.groupBox6.Controls.Add(this.label17);
            this.groupBox6.Font = new System.Drawing.Font("Microsoft Sans Serif", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox6.Location = new System.Drawing.Point(581, 469);
            this.groupBox6.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.groupBox6.Name = "groupBox6";
            this.groupBox6.Padding = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.groupBox6.Size = new System.Drawing.Size(469, 244);
            this.groupBox6.TabIndex = 33;
            this.groupBox6.TabStop = false;
            this.groupBox6.Text = "Copy Office Light 45 degree:";
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label23.Location = new System.Drawing.Point(7, 59);
            this.label23.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(87, 29);
            this.label23.TabIndex = 21;
            this.label23.Text = "Result:";
            // 
            // txtCopyOffice45Result
            // 
            this.txtCopyOffice45Result.BackColor = System.Drawing.Color.White;
            this.txtCopyOffice45Result.Location = new System.Drawing.Point(211, 47);
            this.txtCopyOffice45Result.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.txtCopyOffice45Result.Name = "txtCopyOffice45Result";
            this.txtCopyOffice45Result.Size = new System.Drawing.Size(235, 34);
            this.txtCopyOffice45Result.TabIndex = 12;
            this.txtCopyOffice45Result.TextChanged += new System.EventHandler(this.txtCopyOffice45Result_TextChanged);
            // 
            // txtCopyOffice45STC
            // 
            this.txtCopyOffice45STC.BackColor = System.Drawing.Color.White;
            this.txtCopyOffice45STC.Location = new System.Drawing.Point(211, 116);
            this.txtCopyOffice45STC.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.txtCopyOffice45STC.Name = "txtCopyOffice45STC";
            this.txtCopyOffice45STC.Size = new System.Drawing.Size(235, 34);
            this.txtCopyOffice45STC.TabIndex = 13;
            this.txtCopyOffice45STC.TextChanged += new System.EventHandler(this.txtCopyOffice45STC_TextChanged);
            // 
            // txtCopyOffice45Others
            // 
            this.txtCopyOffice45Others.BackColor = System.Drawing.Color.White;
            this.txtCopyOffice45Others.Location = new System.Drawing.Point(211, 187);
            this.txtCopyOffice45Others.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.txtCopyOffice45Others.Name = "txtCopyOffice45Others";
            this.txtCopyOffice45Others.Size = new System.Drawing.Size(235, 34);
            this.txtCopyOffice45Others.TabIndex = 18;
            this.txtCopyOffice45Others.TextChanged += new System.EventHandler(this.txtCopyOffice45Others_TextChanged);
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label20.Location = new System.Drawing.Point(13, 126);
            this.label20.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(129, 29);
            this.label20.TabIndex = 24;
            this.label20.Text = "STC Zone:";
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label17.Location = new System.Drawing.Point(13, 194);
            this.label17.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(152, 29);
            this.label17.TabIndex = 27;
            this.label17.Text = "Other Zones:";
            // 
            // groupBox5
            // 
            this.groupBox5.Controls.Add(this.txtCopyOffice0Result);
            this.groupBox5.Controls.Add(this.txtCopyOffice0STC);
            this.groupBox5.Controls.Add(this.txtCopyOffice0Others);
            this.groupBox5.Controls.Add(this.label24);
            this.groupBox5.Controls.Add(this.label21);
            this.groupBox5.Controls.Add(this.label18);
            this.groupBox5.Font = new System.Drawing.Font("Microsoft Sans Serif", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox5.Location = new System.Drawing.Point(28, 469);
            this.groupBox5.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.groupBox5.Name = "groupBox5";
            this.groupBox5.Padding = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.groupBox5.Size = new System.Drawing.Size(469, 244);
            this.groupBox5.TabIndex = 32;
            this.groupBox5.TabStop = false;
            this.groupBox5.Text = "Copy Office Light 0 degree:";
            // 
            // txtCopyOffice0Result
            // 
            this.txtCopyOffice0Result.BackColor = System.Drawing.Color.White;
            this.txtCopyOffice0Result.Location = new System.Drawing.Point(215, 47);
            this.txtCopyOffice0Result.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.txtCopyOffice0Result.Name = "txtCopyOffice0Result";
            this.txtCopyOffice0Result.Size = new System.Drawing.Size(232, 34);
            this.txtCopyOffice0Result.TabIndex = 17;
            this.txtCopyOffice0Result.TextChanged += new System.EventHandler(this.txtCopyOffice0Result_TextChanged);
            // 
            // txtCopyOffice0STC
            // 
            this.txtCopyOffice0STC.BackColor = System.Drawing.Color.White;
            this.txtCopyOffice0STC.Location = new System.Drawing.Point(215, 114);
            this.txtCopyOffice0STC.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.txtCopyOffice0STC.Name = "txtCopyOffice0STC";
            this.txtCopyOffice0STC.Size = new System.Drawing.Size(232, 34);
            this.txtCopyOffice0STC.TabIndex = 11;
            this.txtCopyOffice0STC.TextChanged += new System.EventHandler(this.txtCopyOffice0STC_TextChanged);
            // 
            // txtCopyOffice0Others
            // 
            this.txtCopyOffice0Others.BackColor = System.Drawing.Color.White;
            this.txtCopyOffice0Others.Location = new System.Drawing.Point(215, 187);
            this.txtCopyOffice0Others.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.txtCopyOffice0Others.Name = "txtCopyOffice0Others";
            this.txtCopyOffice0Others.Size = new System.Drawing.Size(232, 34);
            this.txtCopyOffice0Others.TabIndex = 15;
            this.txtCopyOffice0Others.TextChanged += new System.EventHandler(this.txtCopyOffice0Others_TextChanged);
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label24.Location = new System.Drawing.Point(4, 54);
            this.label24.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(87, 29);
            this.label24.TabIndex = 20;
            this.label24.Text = "Result:";
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label21.Location = new System.Drawing.Point(3, 121);
            this.label21.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(129, 29);
            this.label21.TabIndex = 23;
            this.label21.Text = "STC Zone:";
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label18.Location = new System.Drawing.Point(4, 194);
            this.label18.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(152, 29);
            this.label18.TabIndex = 26;
            this.label18.Text = "Other Zones:";
            // 
            // groupBox4
            // 
            this.groupBox4.Controls.Add(this.txtOrigLight45Result);
            this.groupBox4.Controls.Add(this.txtOrigLight45STC);
            this.groupBox4.Controls.Add(this.txtOrigLight45Others);
            this.groupBox4.Controls.Add(this.label13);
            this.groupBox4.Controls.Add(this.label14);
            this.groupBox4.Controls.Add(this.label15);
            this.groupBox4.Font = new System.Drawing.Font("Microsoft Sans Serif", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox4.Location = new System.Drawing.Point(1135, 213);
            this.groupBox4.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Padding = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.groupBox4.Size = new System.Drawing.Size(469, 247);
            this.groupBox4.TabIndex = 31;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = "Original Lightbox 45 degree:";
            // 
            // txtOrigLight45Result
            // 
            this.txtOrigLight45Result.BackColor = System.Drawing.Color.White;
            this.txtOrigLight45Result.Location = new System.Drawing.Point(211, 52);
            this.txtOrigLight45Result.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.txtOrigLight45Result.Name = "txtOrigLight45Result";
            this.txtOrigLight45Result.Size = new System.Drawing.Size(235, 34);
            this.txtOrigLight45Result.TabIndex = 6;
            this.txtOrigLight45Result.TextChanged += new System.EventHandler(this.txtOrigLight45Result_TextChanged);
            // 
            // txtOrigLight45STC
            // 
            this.txtOrigLight45STC.BackColor = System.Drawing.Color.White;
            this.txtOrigLight45STC.Location = new System.Drawing.Point(211, 116);
            this.txtOrigLight45STC.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.txtOrigLight45STC.Name = "txtOrigLight45STC";
            this.txtOrigLight45STC.Size = new System.Drawing.Size(235, 34);
            this.txtOrigLight45STC.TabIndex = 6;
            this.txtOrigLight45STC.TextChanged += new System.EventHandler(this.txtOrigLight45STC_TextChanged);
            // 
            // txtOrigLight45Others
            // 
            this.txtOrigLight45Others.BackColor = System.Drawing.Color.White;
            this.txtOrigLight45Others.Location = new System.Drawing.Point(211, 183);
            this.txtOrigLight45Others.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.txtOrigLight45Others.Name = "txtOrigLight45Others";
            this.txtOrigLight45Others.Size = new System.Drawing.Size(235, 34);
            this.txtOrigLight45Others.TabIndex = 6;
            this.txtOrigLight45Others.TextChanged += new System.EventHandler(this.txtOrigLight45Others_TextChanged);
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label13.Location = new System.Drawing.Point(11, 62);
            this.label13.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(87, 29);
            this.label13.TabIndex = 7;
            this.label13.Text = "Result:";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label14.Location = new System.Drawing.Point(9, 127);
            this.label14.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(129, 29);
            this.label14.TabIndex = 7;
            this.label14.Text = "STC Zone:";
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label15.Location = new System.Drawing.Point(9, 190);
            this.label15.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(152, 29);
            this.label15.TabIndex = 7;
            this.label15.Text = "Other Zones:";
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.txtOrigOffice45Result);
            this.groupBox3.Controls.Add(this.txtOrigOffice45STC);
            this.groupBox3.Controls.Add(this.txtOrigOffice45Others);
            this.groupBox3.Controls.Add(this.label9);
            this.groupBox3.Controls.Add(this.label10);
            this.groupBox3.Controls.Add(this.label11);
            this.groupBox3.Font = new System.Drawing.Font("Microsoft Sans Serif", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox3.Location = new System.Drawing.Point(581, 213);
            this.groupBox3.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Padding = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.groupBox3.Size = new System.Drawing.Size(469, 247);
            this.groupBox3.TabIndex = 30;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Original Office Light 45 degree:";
            // 
            // txtOrigOffice45Result
            // 
            this.txtOrigOffice45Result.BackColor = System.Drawing.Color.White;
            this.txtOrigOffice45Result.Location = new System.Drawing.Point(211, 52);
            this.txtOrigOffice45Result.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.txtOrigOffice45Result.Name = "txtOrigOffice45Result";
            this.txtOrigOffice45Result.Size = new System.Drawing.Size(235, 34);
            this.txtOrigOffice45Result.TabIndex = 6;
            this.txtOrigOffice45Result.TextChanged += new System.EventHandler(this.txtOrigOffice45Result_TextChanged);
            // 
            // txtOrigOffice45STC
            // 
            this.txtOrigOffice45STC.BackColor = System.Drawing.Color.White;
            this.txtOrigOffice45STC.Location = new System.Drawing.Point(211, 116);
            this.txtOrigOffice45STC.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.txtOrigOffice45STC.Name = "txtOrigOffice45STC";
            this.txtOrigOffice45STC.Size = new System.Drawing.Size(235, 34);
            this.txtOrigOffice45STC.TabIndex = 6;
            this.txtOrigOffice45STC.TextChanged += new System.EventHandler(this.txtOrigOffice45STC_TextChanged);
            // 
            // txtOrigOffice45Others
            // 
            this.txtOrigOffice45Others.BackColor = System.Drawing.Color.White;
            this.txtOrigOffice45Others.Location = new System.Drawing.Point(211, 183);
            this.txtOrigOffice45Others.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.txtOrigOffice45Others.Name = "txtOrigOffice45Others";
            this.txtOrigOffice45Others.Size = new System.Drawing.Size(235, 34);
            this.txtOrigOffice45Others.TabIndex = 6;
            this.txtOrigOffice45Others.TextChanged += new System.EventHandler(this.txtOrigOffice45Others_TextChanged);
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.Location = new System.Drawing.Point(5, 62);
            this.label9.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(87, 29);
            this.label9.TabIndex = 7;
            this.label9.Text = "Result:";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.Location = new System.Drawing.Point(12, 127);
            this.label10.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(129, 29);
            this.label10.TabIndex = 7;
            this.label10.Text = "STC Zone:";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.Location = new System.Drawing.Point(12, 188);
            this.label11.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(152, 29);
            this.label11.TabIndex = 7;
            this.label11.Text = "Other Zones:";
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.label7);
            this.groupBox2.Controls.Add(this.label6);
            this.groupBox2.Controls.Add(this.label5);
            this.groupBox2.Controls.Add(this.txtOrigOffice0Others);
            this.groupBox2.Controls.Add(this.txtOrigOffice0STC);
            this.groupBox2.Controls.Add(this.txtOrigOffice0Result);
            this.groupBox2.Font = new System.Drawing.Font("Microsoft Sans Serif", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox2.Location = new System.Drawing.Point(28, 213);
            this.groupBox2.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Padding = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.groupBox2.Size = new System.Drawing.Size(469, 247);
            this.groupBox2.TabIndex = 29;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Original Office Light 0 degree:";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(12, 193);
            this.label7.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(152, 29);
            this.label7.TabIndex = 11;
            this.label7.Text = "Other Zones:";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(11, 124);
            this.label6.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(129, 29);
            this.label6.TabIndex = 12;
            this.label6.Text = "STC Zone:";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(11, 58);
            this.label5.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(87, 29);
            this.label5.TabIndex = 13;
            this.label5.Text = "Result:";
            // 
            // txtOrigOffice0Others
            // 
            this.txtOrigOffice0Others.BackColor = System.Drawing.Color.White;
            this.txtOrigOffice0Others.Location = new System.Drawing.Point(215, 185);
            this.txtOrigOffice0Others.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.txtOrigOffice0Others.Name = "txtOrigOffice0Others";
            this.txtOrigOffice0Others.Size = new System.Drawing.Size(232, 34);
            this.txtOrigOffice0Others.TabIndex = 8;
            this.txtOrigOffice0Others.TextChanged += new System.EventHandler(this.txtOrigOffice0Others_TextChanged);
            // 
            // txtOrigOffice0STC
            // 
            this.txtOrigOffice0STC.BackColor = System.Drawing.Color.White;
            this.txtOrigOffice0STC.Location = new System.Drawing.Point(215, 117);
            this.txtOrigOffice0STC.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.txtOrigOffice0STC.Name = "txtOrigOffice0STC";
            this.txtOrigOffice0STC.Size = new System.Drawing.Size(232, 34);
            this.txtOrigOffice0STC.TabIndex = 9;
            this.txtOrigOffice0STC.TextChanged += new System.EventHandler(this.txtOrigOffice0STC_TextChanged);
            // 
            // txtOrigOffice0Result
            // 
            this.txtOrigOffice0Result.BackColor = System.Drawing.Color.White;
            this.txtOrigOffice0Result.Location = new System.Drawing.Point(215, 50);
            this.txtOrigOffice0Result.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.txtOrigOffice0Result.Name = "txtOrigOffice0Result";
            this.txtOrigOffice0Result.Size = new System.Drawing.Size(232, 34);
            this.txtOrigOffice0Result.TabIndex = 10;
            this.txtOrigOffice0Result.TextChanged += new System.EventHandler(this.txtOrigOffice0Result_TextChanged);
            // 
            // label25
            // 
            this.label25.AutoSize = true;
            this.label25.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label25.Location = new System.Drawing.Point(996, 546);
            this.label25.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(0, 29);
            this.label25.TabIndex = 10;
            // 
            // label26
            // 
            this.label26.AutoSize = true;
            this.label26.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label26.Location = new System.Drawing.Point(497, 546);
            this.label26.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(0, 29);
            this.label26.TabIndex = 9;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(21, 284);
            this.label4.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(0, 29);
            this.label4.TabIndex = 5;
            // 
            // txtClmDeviceID
            // 
            this.txtClmDeviceID.BackColor = System.Drawing.Color.White;
            this.txtClmDeviceID.Location = new System.Drawing.Point(233, 132);
            this.txtClmDeviceID.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.txtClmDeviceID.Name = "txtClmDeviceID";
            this.txtClmDeviceID.Size = new System.Drawing.Size(100, 46);
            this.txtClmDeviceID.TabIndex = 4;
            this.txtClmDeviceID.TextChanged += new System.EventHandler(this.txtClmDeviceID_TextChanged);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(21, 144);
            this.label3.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(122, 29);
            this.label3.TabIndex = 3;
            this.label3.Text = "Device ID:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(823, 64);
            this.label2.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(153, 29);
            this.label2.TabIndex = 3;
            this.label2.Text = "Sheet Name:";
            // 
            // cbbSheetName
            // 
            this.cbbSheetName.BackColor = System.Drawing.Color.White;
            this.cbbSheetName.Font = new System.Drawing.Font("Microsoft Sans Serif", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbbSheetName.FormattingEnabled = true;
            this.cbbSheetName.Location = new System.Drawing.Point(1020, 59);
            this.cbbSheetName.Margin = new System.Windows.Forms.Padding(4);
            this.cbbSheetName.Name = "cbbSheetName";
            this.cbbSheetName.Size = new System.Drawing.Size(583, 37);
            this.cbbSheetName.TabIndex = 2;
            this.cbbSheetName.SelectedIndexChanged += new System.EventHandler(this.cbbSheetName_SelectedIndexChanged);
            // 
            // SetupFileForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(1752, 764);
            this.Controls.Add(this.groupBox1);
            this.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.Name = "SetupFileForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "U-nica Mini Tool v2.14";
            this.TopMost = true;
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox7.ResumeLayout(false);
            this.groupBox7.PerformLayout();
            this.groupBox6.ResumeLayout(false);
            this.groupBox6.PerformLayout();
            this.groupBox5.ResumeLayout(false);
            this.groupBox5.PerformLayout();
            this.groupBox4.ResumeLayout(false);
            this.groupBox4.PerformLayout();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.ComboBox cbbProductName;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.ComboBox cbbSheetName;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox txtClmDeviceID;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.TextBox txtOrigOffice45Others;
        private System.Windows.Forms.TextBox txtOrigOffice45STC;
        private System.Windows.Forms.TextBox txtOrigOffice45Result;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.TextBox txtOrigLight45Others;
        private System.Windows.Forms.TextBox txtOrigLight45STC;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.TextBox txtOrigLight45Result;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.TextBox txtCopyLight45Others;
        private System.Windows.Forms.TextBox txtCopyOffice45Others;
        private System.Windows.Forms.TextBox txtCopyLight45STC;
        private System.Windows.Forms.TextBox txtCopyOffice0Others;
        private System.Windows.Forms.TextBox txtCopyLight45Result;
        private System.Windows.Forms.TextBox txtCopyOffice45STC;
        private System.Windows.Forms.TextBox txtCopyOffice45Result;
        private System.Windows.Forms.Label label25;
        private System.Windows.Forms.TextBox txtCopyOffice0STC;
        private System.Windows.Forms.Label label26;
        private System.Windows.Forms.TextBox txtCopyOffice0Result;
        private System.Windows.Forms.GroupBox groupBox5;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox txtOrigOffice0Others;
        private System.Windows.Forms.TextBox txtOrigOffice0STC;
        private System.Windows.Forms.TextBox txtOrigOffice0Result;
        private System.Windows.Forms.GroupBox groupBox7;
        private System.Windows.Forms.GroupBox groupBox6;
        private System.Windows.Forms.Button btnReset;
        private System.Windows.Forms.Button btnUpdate;
    }
}