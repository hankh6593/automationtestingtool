﻿namespace ReadingResult
{
    partial class RegressionTestForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle5 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle6 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle7 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle8 = new System.Windows.Forms.DataGridViewCellStyle();
            this.ResultOpenDialog = new System.Windows.Forms.OpenFileDialog();
            this.lbPath = new System.Windows.Forms.Label();
            this.txtResultPath = new System.Windows.Forms.TextBox();
            this.btnGetResult = new System.Windows.Forms.Button();
            this.btnClear = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.lblFinalResult = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.cbbProductName = new System.Windows.Forms.ComboBox();
            this.label6 = new System.Windows.Forms.Label();
            this.ImageOpenDialog = new System.Windows.Forms.FolderBrowserDialog();
            this.btnBrowseResult = new System.Windows.Forms.Button();
            this.lbMessage = new System.Windows.Forms.Label();
            this.regressionTestToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.configurationToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.evaluationHistoryToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.otherZonesResultToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.collectResultToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.evaluationToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.automationTestToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.label8 = new System.Windows.Forms.Label();
            this.btnRename = new System.Windows.Forms.Button();
            this.btnEvaluation = new System.Windows.Forms.Button();
            this.txtTestQty = new System.Windows.Forms.TextBox();
            this.txtDeliminator = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.dgvResult = new System.Windows.Forms.DataGridView();
            this.btnUploadData = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.progressBar = new System.Windows.Forms.ProgressBar();
            this.jSONGeneratorToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.menuStrip1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvResult)).BeginInit();
            this.SuspendLayout();
            // 
            // ResultOpenDialog
            // 
            this.ResultOpenDialog.FileName = "openFileDialog1";
            // 
            // lbPath
            // 
            this.lbPath.AutoSize = true;
            this.lbPath.Font = new System.Drawing.Font("Microsoft Sans Serif", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbPath.Location = new System.Drawing.Point(25, 173);
            this.lbPath.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lbPath.Name = "lbPath";
            this.lbPath.Size = new System.Drawing.Size(128, 29);
            this.lbPath.TabIndex = 1;
            this.lbPath.Text = "*Input File:";
            // 
            // txtResultPath
            // 
            this.txtResultPath.BackColor = System.Drawing.Color.White;
            this.txtResultPath.Font = new System.Drawing.Font("Microsoft Sans Serif", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtResultPath.Location = new System.Drawing.Point(250, 168);
            this.txtResultPath.Margin = new System.Windows.Forms.Padding(4);
            this.txtResultPath.Multiline = true;
            this.txtResultPath.Name = "txtResultPath";
            this.txtResultPath.ReadOnly = true;
            this.txtResultPath.Size = new System.Drawing.Size(1247, 111);
            this.txtResultPath.TabIndex = 2;
            // 
            // btnGetResult
            // 
            this.btnGetResult.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(129)))), ((int)(((byte)(213)))));
            this.btnGetResult.Font = new System.Drawing.Font("Microsoft Sans Serif", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnGetResult.ForeColor = System.Drawing.Color.White;
            this.btnGetResult.Location = new System.Drawing.Point(249, 460);
            this.btnGetResult.Margin = new System.Windows.Forms.Padding(4);
            this.btnGetResult.Name = "btnGetResult";
            this.btnGetResult.Size = new System.Drawing.Size(195, 58);
            this.btnGetResult.TabIndex = 3;
            this.btnGetResult.Text = "Read File";
            this.btnGetResult.UseVisualStyleBackColor = false;
            this.btnGetResult.Click += new System.EventHandler(this.btnGetResult_Click);
            // 
            // btnClear
            // 
            this.btnClear.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(129)))), ((int)(((byte)(213)))));
            this.btnClear.Font = new System.Drawing.Font("Microsoft Sans Serif", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnClear.ForeColor = System.Drawing.Color.White;
            this.btnClear.Location = new System.Drawing.Point(779, 460);
            this.btnClear.Margin = new System.Windows.Forms.Padding(4);
            this.btnClear.Name = "btnClear";
            this.btnClear.Size = new System.Drawing.Size(195, 58);
            this.btnClear.TabIndex = 4;
            this.btnClear.Text = "Clear All Data";
            this.btnClear.UseVisualStyleBackColor = false;
            this.btnClear.Click += new System.EventHandler(this.btnClear_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(-7, 899);
            this.label1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(0, 29);
            this.label1.TabIndex = 10;
            // 
            // lblFinalResult
            // 
            this.lblFinalResult.AutoSize = true;
            this.lblFinalResult.Font = new System.Drawing.Font("Microsoft Sans Serif", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblFinalResult.Location = new System.Drawing.Point(30, 534);
            this.lblFinalResult.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblFinalResult.Name = "lblFinalResult";
            this.lblFinalResult.Size = new System.Drawing.Size(141, 29);
            this.lblFinalResult.TabIndex = 11;
            this.lblFinalResult.Text = "Test Result:";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.label4.Location = new System.Drawing.Point(29, 60);
            this.label4.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(572, 29);
            this.label4.TabIndex = 17;
            this.label4.Text = "Please close the .csv file before clicking \"Read File\".";
            // 
            // cbbProductName
            // 
            this.cbbProductName.BackColor = System.Drawing.Color.White;
            this.cbbProductName.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbbProductName.FormattingEnabled = true;
            this.cbbProductName.Location = new System.Drawing.Point(249, 317);
            this.cbbProductName.Margin = new System.Windows.Forms.Padding(4);
            this.cbbProductName.Name = "cbbProductName";
            this.cbbProductName.Size = new System.Drawing.Size(1249, 37);
            this.cbbProductName.TabIndex = 20;
            this.cbbProductName.SelectedIndexChanged += new System.EventHandler(this.CbbProductName_SelectedIndexChanged);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(29, 320);
            this.label6.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(183, 29);
            this.label6.TabIndex = 23;
            this.label6.Text = "*Product Name:";
            // 
            // btnBrowseResult
            // 
            this.btnBrowseResult.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(129)))), ((int)(((byte)(213)))));
            this.btnBrowseResult.FlatAppearance.BorderSize = 0;
            this.btnBrowseResult.Font = new System.Drawing.Font("Microsoft Sans Serif", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnBrowseResult.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.btnBrowseResult.Location = new System.Drawing.Point(1557, 168);
            this.btnBrowseResult.Margin = new System.Windows.Forms.Padding(4);
            this.btnBrowseResult.Name = "btnBrowseResult";
            this.btnBrowseResult.Size = new System.Drawing.Size(195, 58);
            this.btnBrowseResult.TabIndex = 3;
            this.btnBrowseResult.Text = "Browse";
            this.btnBrowseResult.UseVisualStyleBackColor = false;
            this.btnBrowseResult.Click += new System.EventHandler(this.btnBrowseResult_Click);
            // 
            // lbMessage
            // 
            this.lbMessage.AutoSize = true;
            this.lbMessage.Font = new System.Drawing.Font("Microsoft Sans Serif", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbMessage.ForeColor = System.Drawing.Color.Red;
            this.lbMessage.Location = new System.Drawing.Point(246, 534);
            this.lbMessage.Name = "lbMessage";
            this.lbMessage.Size = new System.Drawing.Size(0, 29);
            this.lbMessage.TabIndex = 27;
            // 
            // regressionTestToolStripMenuItem
            // 
            this.regressionTestToolStripMenuItem.ForeColor = System.Drawing.Color.White;
            this.regressionTestToolStripMenuItem.Name = "regressionTestToolStripMenuItem";
            this.regressionTestToolStripMenuItem.Size = new System.Drawing.Size(138, 31);
            this.regressionTestToolStripMenuItem.Text = "Evaluation";
            this.regressionTestToolStripMenuItem.Click += new System.EventHandler(this.regressionTestToolStripMenuItem_Click);
            // 
            // configurationToolStripMenuItem
            // 
            this.configurationToolStripMenuItem.Font = new System.Drawing.Font("Arial", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.configurationToolStripMenuItem.ForeColor = System.Drawing.Color.White;
            this.configurationToolStripMenuItem.Name = "configurationToolStripMenuItem";
            this.configurationToolStripMenuItem.Size = new System.Drawing.Size(170, 31);
            this.configurationToolStripMenuItem.Text = "Configuration";
            this.configurationToolStripMenuItem.Click += new System.EventHandler(this.configurationToolStripMenuItem_Click);
            // 
            // menuStrip1
            // 
            this.menuStrip1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(12)))), ((int)(((byte)(123)))), ((int)(((byte)(177)))));
            this.menuStrip1.Font = new System.Drawing.Font("Arial", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.menuStrip1.ImageScalingSize = new System.Drawing.Size(20, 20);
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.regressionTestToolStripMenuItem,
            this.evaluationHistoryToolStripMenuItem,
            this.otherZonesResultToolStripMenuItem,
            this.collectResultToolStripMenuItem,
            this.evaluationToolStripMenuItem,
            this.automationTestToolStripMenuItem,
            this.configurationToolStripMenuItem,
            this.jSONGeneratorToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Padding = new System.Windows.Forms.Padding(5, 6, 0, 6);
            this.menuStrip1.Size = new System.Drawing.Size(1786, 43);
            this.menuStrip1.TabIndex = 24;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // evaluationHistoryToolStripMenuItem
            // 
            this.evaluationHistoryToolStripMenuItem.ForeColor = System.Drawing.Color.White;
            this.evaluationHistoryToolStripMenuItem.Name = "evaluationHistoryToolStripMenuItem";
            this.evaluationHistoryToolStripMenuItem.Size = new System.Drawing.Size(219, 31);
            this.evaluationHistoryToolStripMenuItem.Text = "Evaluation History";
            this.evaluationHistoryToolStripMenuItem.Click += new System.EventHandler(this.evaluationHistoryToolStripMenuItem_Click);
            // 
            // otherZonesResultToolStripMenuItem
            // 
            this.otherZonesResultToolStripMenuItem.ForeColor = System.Drawing.Color.White;
            this.otherZonesResultToolStripMenuItem.Name = "otherZonesResultToolStripMenuItem";
            this.otherZonesResultToolStripMenuItem.Size = new System.Drawing.Size(244, 31);
            this.otherZonesResultToolStripMenuItem.Text = "Non-STC Evaluation";
            this.otherZonesResultToolStripMenuItem.Click += new System.EventHandler(this.otherZonesResultToolStripMenuItem_Click);
            // 
            // collectResultToolStripMenuItem
            // 
            this.collectResultToolStripMenuItem.ForeColor = System.Drawing.Color.White;
            this.collectResultToolStripMenuItem.Name = "collectResultToolStripMenuItem";
            this.collectResultToolStripMenuItem.Size = new System.Drawing.Size(176, 31);
            this.collectResultToolStripMenuItem.Text = "Collect Result";
            this.collectResultToolStripMenuItem.Click += new System.EventHandler(this.collectResultToolStripMenuItem_Click);
            // 
            // evaluationToolStripMenuItem
            // 
            this.evaluationToolStripMenuItem.Font = new System.Drawing.Font("Arial", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.evaluationToolStripMenuItem.ForeColor = System.Drawing.Color.White;
            this.evaluationToolStripMenuItem.Name = "evaluationToolStripMenuItem";
            this.evaluationToolStripMenuItem.Size = new System.Drawing.Size(155, 31);
            this.evaluationToolStripMenuItem.Text = "Comparison";
            this.evaluationToolStripMenuItem.Click += new System.EventHandler(this.evaluationToolStripMenuItem_Click);
            // 
            // automationTestToolStripMenuItem
            // 
            this.automationTestToolStripMenuItem.ForeColor = System.Drawing.Color.White;
            this.automationTestToolStripMenuItem.Name = "automationTestToolStripMenuItem";
            this.automationTestToolStripMenuItem.Size = new System.Drawing.Size(197, 31);
            this.automationTestToolStripMenuItem.Text = "Automation Test";
            this.automationTestToolStripMenuItem.Click += new System.EventHandler(this.automationTestToolStripMenuItem_Click);
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.Location = new System.Drawing.Point(30, 394);
            this.label8.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(109, 29);
            this.label8.TabIndex = 30;
            this.label8.Text = "Test Qty:";
            // 
            // btnRename
            // 
            this.btnRename.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(129)))), ((int)(((byte)(213)))));
            this.btnRename.Cursor = System.Windows.Forms.Cursors.AppStarting;
            this.btnRename.Font = new System.Drawing.Font("Microsoft Sans Serif", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnRename.ForeColor = System.Drawing.Color.White;
            this.btnRename.Location = new System.Drawing.Point(497, 460);
            this.btnRename.Margin = new System.Windows.Forms.Padding(4);
            this.btnRename.Name = "btnRename";
            this.btnRename.Size = new System.Drawing.Size(220, 58);
            this.btnRename.TabIndex = 25;
            this.btnRename.Text = "Rename File";
            this.btnRename.UseVisualStyleBackColor = false;
            this.btnRename.Click += new System.EventHandler(this.btnRename_Click);
            // 
            // btnEvaluation
            // 
            this.btnEvaluation.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(129)))), ((int)(((byte)(213)))));
            this.btnEvaluation.Cursor = System.Windows.Forms.Cursors.AppStarting;
            this.btnEvaluation.Font = new System.Drawing.Font("Microsoft Sans Serif", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnEvaluation.ForeColor = System.Drawing.Color.White;
            this.btnEvaluation.Location = new System.Drawing.Point(1557, 899);
            this.btnEvaluation.Margin = new System.Windows.Forms.Padding(4);
            this.btnEvaluation.Name = "btnEvaluation";
            this.btnEvaluation.Size = new System.Drawing.Size(195, 58);
            this.btnEvaluation.TabIndex = 25;
            this.btnEvaluation.Text = "Analyze";
            this.btnEvaluation.UseVisualStyleBackColor = false;
            this.btnEvaluation.Click += new System.EventHandler(this.btnEvaluation_Click);
            // 
            // txtTestQty
            // 
            this.txtTestQty.BackColor = System.Drawing.Color.White;
            this.txtTestQty.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTestQty.Location = new System.Drawing.Point(250, 388);
            this.txtTestQty.Margin = new System.Windows.Forms.Padding(4);
            this.txtTestQty.Name = "txtTestQty";
            this.txtTestQty.Size = new System.Drawing.Size(137, 34);
            this.txtTestQty.TabIndex = 31;
            // 
            // txtDeliminator
            // 
            this.txtDeliminator.BackColor = System.Drawing.Color.White;
            this.txtDeliminator.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtDeliminator.Location = new System.Drawing.Point(691, 391);
            this.txtDeliminator.Margin = new System.Windows.Forms.Padding(4);
            this.txtDeliminator.Name = "txtDeliminator";
            this.txtDeliminator.Size = new System.Drawing.Size(99, 34);
            this.txtDeliminator.TabIndex = 33;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Microsoft Sans Serif", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.Location = new System.Drawing.Point(424, 391);
            this.label9.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(234, 29);
            this.label9.TabIndex = 32;
            this.label9.Text = "Seperate Result Qty:";
            // 
            // dgvResult
            // 
            this.dgvResult.BackgroundColor = System.Drawing.Color.White;
            dataGridViewCellStyle5.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle5.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(34)))), ((int)(((byte)(189)))), ((int)(((byte)(196)))));
            dataGridViewCellStyle5.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle5.ForeColor = System.Drawing.Color.White;
            dataGridViewCellStyle5.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle5.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle5.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgvResult.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle5;
            this.dgvResult.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            dataGridViewCellStyle6.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle6.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle6.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle6.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle6.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle6.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle6.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgvResult.DefaultCellStyle = dataGridViewCellStyle6;
            this.dgvResult.GridColor = System.Drawing.Color.Gray;
            this.dgvResult.Location = new System.Drawing.Point(35, 590);
            this.dgvResult.Margin = new System.Windows.Forms.Padding(4);
            this.dgvResult.Name = "dgvResult";
            this.dgvResult.RowHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.Sunken;
            dataGridViewCellStyle7.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle7.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(34)))), ((int)(((byte)(189)))), ((int)(((byte)(196)))));
            dataGridViewCellStyle7.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle7.ForeColor = System.Drawing.SystemColors.InactiveBorder;
            dataGridViewCellStyle7.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle7.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle7.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgvResult.RowHeadersDefaultCellStyle = dataGridViewCellStyle7;
            this.dgvResult.RowHeadersWidth = 51;
            dataGridViewCellStyle8.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle8.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle8.ForeColor = System.Drawing.Color.Black;
            this.dgvResult.RowsDefaultCellStyle = dataGridViewCellStyle8;
            this.dgvResult.Size = new System.Drawing.Size(1717, 289);
            this.dgvResult.TabIndex = 36;
            // 
            // btnUploadData
            // 
            this.btnUploadData.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(129)))), ((int)(((byte)(213)))));
            this.btnUploadData.Font = new System.Drawing.Font("Microsoft Sans Serif", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnUploadData.ForeColor = System.Drawing.Color.White;
            this.btnUploadData.Location = new System.Drawing.Point(1299, 899);
            this.btnUploadData.Margin = new System.Windows.Forms.Padding(4);
            this.btnUploadData.Name = "btnUploadData";
            this.btnUploadData.Size = new System.Drawing.Size(195, 58);
            this.btnUploadData.TabIndex = 37;
            this.btnUploadData.Text = "Upload Data";
            this.btnUploadData.UseVisualStyleBackColor = false;
            this.btnUploadData.Click += new System.EventHandler(this.btnUploadData_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.label2.Location = new System.Drawing.Point(29, 110);
            this.label2.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(205, 29);
            this.label2.TabIndex = 17;
            this.label2.Text = "* Field is required";
            // 
            // progressBar
            // 
            this.progressBar.Location = new System.Drawing.Point(1048, 460);
            this.progressBar.Name = "progressBar";
            this.progressBar.Size = new System.Drawing.Size(704, 58);
            this.progressBar.TabIndex = 38;
            // 
            // jSONGeneratorToolStripMenuItem
            // 
            this.jSONGeneratorToolStripMenuItem.ForeColor = System.Drawing.Color.White;
            this.jSONGeneratorToolStripMenuItem.Name = "jSONGeneratorToolStripMenuItem";
            this.jSONGeneratorToolStripMenuItem.Size = new System.Drawing.Size(205, 31);
            this.jSONGeneratorToolStripMenuItem.Text = "JSON Generator";
            this.jSONGeneratorToolStripMenuItem.Click += new System.EventHandler(this.jSONGeneratorToolStripMenuItem_Click);
            // 
            // RegressionTestForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(1786, 987);
            this.Controls.Add(this.progressBar);
            this.Controls.Add(this.btnUploadData);
            this.Controls.Add(this.dgvResult);
            this.Controls.Add(this.txtDeliminator);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.txtTestQty);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.lbMessage);
            this.Controls.Add(this.btnRename);
            this.Controls.Add(this.btnEvaluation);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.cbbProductName);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.lblFinalResult);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.btnClear);
            this.Controls.Add(this.btnBrowseResult);
            this.Controls.Add(this.btnGetResult);
            this.Controls.Add(this.txtResultPath);
            this.Controls.Add(this.lbPath);
            this.Controls.Add(this.menuStrip1);
            this.MainMenuStrip = this.menuStrip1;
            this.Margin = new System.Windows.Forms.Padding(4);
            this.Name = "RegressionTestForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "U-nica Mini Tool v2.14";
            this.TransparencyKey = System.Drawing.Color.Maroon;
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.ReadResultForm_FormClosed);
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvResult)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.OpenFileDialog ResultOpenDialog;
        private System.Windows.Forms.Label lbPath;
        private System.Windows.Forms.TextBox txtResultPath;
        private System.Windows.Forms.Button btnGetResult;
        private System.Windows.Forms.Button btnClear;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label lblFinalResult;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.ComboBox cbbProductName;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.FolderBrowserDialog ImageOpenDialog;
        private System.Windows.Forms.Button btnBrowseResult;
        private System.Windows.Forms.Label lbMessage;
        private System.Windows.Forms.ToolStripMenuItem regressionTestToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem configurationToolStripMenuItem;
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Button btnRename;
        private System.Windows.Forms.Button btnEvaluation;
        private System.Windows.Forms.TextBox txtTestQty;
        private System.Windows.Forms.TextBox txtDeliminator;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.DataGridView dgvResult;
        private System.Windows.Forms.ToolStripMenuItem evaluationToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem otherZonesResultToolStripMenuItem;
        private System.Windows.Forms.Button btnUploadData;
        private System.Windows.Forms.ToolStripMenuItem evaluationHistoryToolStripMenuItem;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.ProgressBar progressBar;
        private System.Windows.Forms.ToolStripMenuItem collectResultToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem automationTestToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem jSONGeneratorToolStripMenuItem;
    }
}

