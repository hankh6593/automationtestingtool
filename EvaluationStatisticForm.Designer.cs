﻿namespace ReadingResult
{
    partial class EvaluationStatisticForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataVisualization.Charting.ChartArea chartArea24 = new System.Windows.Forms.DataVisualization.Charting.ChartArea();
            System.Windows.Forms.DataVisualization.Charting.Legend legend24 = new System.Windows.Forms.DataVisualization.Charting.Legend();
            System.Windows.Forms.DataVisualization.Charting.Series series24 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.Windows.Forms.DataVisualization.Charting.ChartArea chartArea25 = new System.Windows.Forms.DataVisualization.Charting.ChartArea();
            System.Windows.Forms.DataVisualization.Charting.Legend legend25 = new System.Windows.Forms.DataVisualization.Charting.Legend();
            System.Windows.Forms.DataVisualization.Charting.Series series25 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.Windows.Forms.DataVisualization.Charting.ChartArea chartArea26 = new System.Windows.Forms.DataVisualization.Charting.ChartArea();
            System.Windows.Forms.DataVisualization.Charting.Legend legend26 = new System.Windows.Forms.DataVisualization.Charting.Legend();
            System.Windows.Forms.DataVisualization.Charting.Series series26 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.Windows.Forms.DataVisualization.Charting.ChartArea chartArea27 = new System.Windows.Forms.DataVisualization.Charting.ChartArea();
            System.Windows.Forms.DataVisualization.Charting.Legend legend27 = new System.Windows.Forms.DataVisualization.Charting.Legend();
            System.Windows.Forms.DataVisualization.Charting.Series series27 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.Windows.Forms.DataVisualization.Charting.ChartArea chartArea28 = new System.Windows.Forms.DataVisualization.Charting.ChartArea();
            System.Windows.Forms.DataVisualization.Charting.Legend legend28 = new System.Windows.Forms.DataVisualization.Charting.Legend();
            System.Windows.Forms.DataVisualization.Charting.Series series28 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.Windows.Forms.DataVisualization.Charting.ChartArea chartArea29 = new System.Windows.Forms.DataVisualization.Charting.ChartArea();
            System.Windows.Forms.DataVisualization.Charting.Legend legend29 = new System.Windows.Forms.DataVisualization.Charting.Legend();
            System.Windows.Forms.DataVisualization.Charting.Series series29 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.Windows.Forms.DataVisualization.Charting.ChartArea chartArea30 = new System.Windows.Forms.DataVisualization.Charting.ChartArea();
            System.Windows.Forms.DataVisualization.Charting.Legend legend30 = new System.Windows.Forms.DataVisualization.Charting.Legend();
            System.Windows.Forms.DataVisualization.Charting.Series series30 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.Windows.Forms.DataVisualization.Charting.ChartArea chartArea31 = new System.Windows.Forms.DataVisualization.Charting.ChartArea();
            System.Windows.Forms.DataVisualization.Charting.Legend legend31 = new System.Windows.Forms.DataVisualization.Charting.Legend();
            System.Windows.Forms.DataVisualization.Charting.Series series31 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.Windows.Forms.DataVisualization.Charting.ChartArea chartArea32 = new System.Windows.Forms.DataVisualization.Charting.ChartArea();
            System.Windows.Forms.DataVisualization.Charting.Legend legend32 = new System.Windows.Forms.DataVisualization.Charting.Legend();
            System.Windows.Forms.DataVisualization.Charting.Series series32 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.Windows.Forms.DataVisualization.Charting.ChartArea chartArea33 = new System.Windows.Forms.DataVisualization.Charting.ChartArea();
            System.Windows.Forms.DataVisualization.Charting.Legend legend33 = new System.Windows.Forms.DataVisualization.Charting.Legend();
            System.Windows.Forms.DataVisualization.Charting.Series series33 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.Windows.Forms.DataVisualization.Charting.ChartArea chartArea34 = new System.Windows.Forms.DataVisualization.Charting.ChartArea();
            System.Windows.Forms.DataVisualization.Charting.Legend legend34 = new System.Windows.Forms.DataVisualization.Charting.Legend();
            System.Windows.Forms.DataVisualization.Charting.Series series34 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.Windows.Forms.DataVisualization.Charting.ChartArea chartArea35 = new System.Windows.Forms.DataVisualization.Charting.ChartArea();
            System.Windows.Forms.DataVisualization.Charting.Legend legend35 = new System.Windows.Forms.DataVisualization.Charting.Legend();
            System.Windows.Forms.DataVisualization.Charting.Series series35 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.Windows.Forms.DataVisualization.Charting.ChartArea chartArea36 = new System.Windows.Forms.DataVisualization.Charting.ChartArea();
            System.Windows.Forms.DataVisualization.Charting.Legend legend36 = new System.Windows.Forms.DataVisualization.Charting.Legend();
            System.Windows.Forms.DataVisualization.Charting.Series series36 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.Windows.Forms.DataVisualization.Charting.ChartArea chartArea37 = new System.Windows.Forms.DataVisualization.Charting.ChartArea();
            System.Windows.Forms.DataVisualization.Charting.Legend legend37 = new System.Windows.Forms.DataVisualization.Charting.Legend();
            System.Windows.Forms.DataVisualization.Charting.Series series37 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.Windows.Forms.DataVisualization.Charting.ChartArea chartArea38 = new System.Windows.Forms.DataVisualization.Charting.ChartArea();
            System.Windows.Forms.DataVisualization.Charting.Legend legend38 = new System.Windows.Forms.DataVisualization.Charting.Legend();
            System.Windows.Forms.DataVisualization.Charting.Series series38 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.Windows.Forms.DataVisualization.Charting.ChartArea chartArea39 = new System.Windows.Forms.DataVisualization.Charting.ChartArea();
            System.Windows.Forms.DataVisualization.Charting.Legend legend39 = new System.Windows.Forms.DataVisualization.Charting.Legend();
            System.Windows.Forms.DataVisualization.Charting.Series series39 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.Windows.Forms.DataVisualization.Charting.ChartArea chartArea40 = new System.Windows.Forms.DataVisualization.Charting.ChartArea();
            System.Windows.Forms.DataVisualization.Charting.Legend legend40 = new System.Windows.Forms.DataVisualization.Charting.Legend();
            System.Windows.Forms.DataVisualization.Charting.Series series40 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.Windows.Forms.DataVisualization.Charting.ChartArea chartArea41 = new System.Windows.Forms.DataVisualization.Charting.ChartArea();
            System.Windows.Forms.DataVisualization.Charting.Legend legend41 = new System.Windows.Forms.DataVisualization.Charting.Legend();
            System.Windows.Forms.DataVisualization.Charting.Series series41 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.Windows.Forms.DataVisualization.Charting.ChartArea chartArea42 = new System.Windows.Forms.DataVisualization.Charting.ChartArea();
            System.Windows.Forms.DataVisualization.Charting.Legend legend42 = new System.Windows.Forms.DataVisualization.Charting.Legend();
            System.Windows.Forms.DataVisualization.Charting.Series series42 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.Windows.Forms.DataVisualization.Charting.ChartArea chartArea43 = new System.Windows.Forms.DataVisualization.Charting.ChartArea();
            System.Windows.Forms.DataVisualization.Charting.Legend legend43 = new System.Windows.Forms.DataVisualization.Charting.Legend();
            System.Windows.Forms.DataVisualization.Charting.Series series43 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.Windows.Forms.DataVisualization.Charting.ChartArea chartArea44 = new System.Windows.Forms.DataVisualization.Charting.ChartArea();
            System.Windows.Forms.DataVisualization.Charting.Legend legend44 = new System.Windows.Forms.DataVisualization.Charting.Legend();
            System.Windows.Forms.DataVisualization.Charting.Series series44 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.Windows.Forms.DataVisualization.Charting.ChartArea chartArea45 = new System.Windows.Forms.DataVisualization.Charting.ChartArea();
            System.Windows.Forms.DataVisualization.Charting.Legend legend45 = new System.Windows.Forms.DataVisualization.Charting.Legend();
            System.Windows.Forms.DataVisualization.Charting.Series series45 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.Windows.Forms.DataVisualization.Charting.ChartArea chartArea46 = new System.Windows.Forms.DataVisualization.Charting.ChartArea();
            System.Windows.Forms.DataVisualization.Charting.Legend legend46 = new System.Windows.Forms.DataVisualization.Charting.Legend();
            System.Windows.Forms.DataVisualization.Charting.Series series46 = new System.Windows.Forms.DataVisualization.Charting.Series();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.v = new System.Windows.Forms.TabPage();
            this.label14 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.btnClear = new System.Windows.Forms.Button();
            this.chartStatistic = new System.Windows.Forms.DataVisualization.Charting.Chart();
            this.cbbData = new System.Windows.Forms.ComboBox();
            this.txtEdge = new System.Windows.Forms.TextBox();
            this.txtZone = new System.Windows.Forms.TextBox();
            this.txtDevice = new System.Windows.Forms.TextBox();
            this.txtProduct = new System.Windows.Forms.TextBox();
            this.txtVersion = new System.Windows.Forms.TextBox();
            this.dgvDataStatistic = new System.Windows.Forms.DataGridView();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.btnBack = new System.Windows.Forms.Button();
            this.btnNext = new System.Windows.Forms.Button();
            this.panel2 = new System.Windows.Forms.Panel();
            this.Chart11B = new System.Windows.Forms.DataVisualization.Charting.Chart();
            this.Chart10B = new System.Windows.Forms.DataVisualization.Charting.Chart();
            this.Chart9B = new System.Windows.Forms.DataVisualization.Charting.Chart();
            this.Chart8B = new System.Windows.Forms.DataVisualization.Charting.Chart();
            this.Chart7B = new System.Windows.Forms.DataVisualization.Charting.Chart();
            this.Chart6B = new System.Windows.Forms.DataVisualization.Charting.Chart();
            this.Chart5B = new System.Windows.Forms.DataVisualization.Charting.Chart();
            this.Chart4B = new System.Windows.Forms.DataVisualization.Charting.Chart();
            this.Chart3B = new System.Windows.Forms.DataVisualization.Charting.Chart();
            this.Chart2B = new System.Windows.Forms.DataVisualization.Charting.Chart();
            this.Chart1B = new System.Windows.Forms.DataVisualization.Charting.Chart();
            this.cbbZoneCompare = new System.Windows.Forms.ComboBox();
            this.cbbEdgeCompare = new System.Windows.Forms.ComboBox();
            this.panel1 = new System.Windows.Forms.Panel();
            this.Chart11A = new System.Windows.Forms.DataVisualization.Charting.Chart();
            this.Chart10A = new System.Windows.Forms.DataVisualization.Charting.Chart();
            this.Chart9A = new System.Windows.Forms.DataVisualization.Charting.Chart();
            this.Chart8A = new System.Windows.Forms.DataVisualization.Charting.Chart();
            this.Chart7A = new System.Windows.Forms.DataVisualization.Charting.Chart();
            this.Chart6A = new System.Windows.Forms.DataVisualization.Charting.Chart();
            this.Chart5A = new System.Windows.Forms.DataVisualization.Charting.Chart();
            this.Chart4A = new System.Windows.Forms.DataVisualization.Charting.Chart();
            this.Chart3A = new System.Windows.Forms.DataVisualization.Charting.Chart();
            this.Chart2A = new System.Windows.Forms.DataVisualization.Charting.Chart();
            this.Chart1A = new System.Windows.Forms.DataVisualization.Charting.Chart();
            this.label17 = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.cbbDeviceCompare = new System.Windows.Forms.ComboBox();
            this.cbbProductCompare = new System.Windows.Forms.ComboBox();
            this.cbbVersionB = new System.Windows.Forms.ComboBox();
            this.label11 = new System.Windows.Forms.Label();
            this.cbbVersionA = new System.Windows.Forms.ComboBox();
            this.label4 = new System.Windows.Forms.Label();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.regressionTestToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.evaluationHistoryToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.otherZonesResultToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.collectResultToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.evaluationToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.automationTestToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.configurationToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.jSONGeneratorToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.tabControl1.SuspendLayout();
            this.v.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chartStatistic)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvDataStatistic)).BeginInit();
            this.tabPage1.SuspendLayout();
            this.panel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Chart11B)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Chart10B)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Chart9B)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Chart8B)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Chart7B)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Chart6B)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Chart5B)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Chart4B)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Chart3B)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Chart2B)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Chart1B)).BeginInit();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Chart11A)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Chart10A)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Chart9A)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Chart8A)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Chart7A)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Chart6A)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Chart5A)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Chart4A)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Chart3A)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Chart2A)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Chart1A)).BeginInit();
            this.menuStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.v);
            this.tabControl1.Controls.Add(this.tabPage1);
            this.tabControl1.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tabControl1.Location = new System.Drawing.Point(16, 122);
            this.tabControl1.Margin = new System.Windows.Forms.Padding(4);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(2100, 938);
            this.tabControl1.TabIndex = 26;
            // 
            // v
            // 
            this.v.Controls.Add(this.label14);
            this.v.Controls.Add(this.label13);
            this.v.Controls.Add(this.label12);
            this.v.Controls.Add(this.label10);
            this.v.Controls.Add(this.label9);
            this.v.Controls.Add(this.label8);
            this.v.Controls.Add(this.btnClear);
            this.v.Controls.Add(this.chartStatistic);
            this.v.Controls.Add(this.cbbData);
            this.v.Controls.Add(this.txtEdge);
            this.v.Controls.Add(this.txtZone);
            this.v.Controls.Add(this.txtDevice);
            this.v.Controls.Add(this.txtProduct);
            this.v.Controls.Add(this.txtVersion);
            this.v.Controls.Add(this.dgvDataStatistic);
            this.v.Location = new System.Drawing.Point(4, 38);
            this.v.Margin = new System.Windows.Forms.Padding(4);
            this.v.Name = "v";
            this.v.Padding = new System.Windows.Forms.Padding(4);
            this.v.Size = new System.Drawing.Size(2092, 896);
            this.v.TabIndex = 1;
            this.v.Text = "Filtered Comparison";
            this.v.UseVisualStyleBackColor = true;
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label14.Location = new System.Drawing.Point(941, 36);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(65, 29);
            this.label14.TabIndex = 28;
            this.label14.Text = "Line:";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label13.Location = new System.Drawing.Point(755, 36);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(77, 29);
            this.label13.TabIndex = 28;
            this.label13.Text = "Edge:";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.Location = new System.Drawing.Point(573, 36);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(74, 29);
            this.label12.TabIndex = 28;
            this.label12.Text = "Zone:";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.Location = new System.Drawing.Point(387, 36);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(93, 29);
            this.label10.TabIndex = 28;
            this.label10.Text = "Device:";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.Location = new System.Drawing.Point(208, 36);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(102, 29);
            this.label9.TabIndex = 28;
            this.label9.Text = "Product:";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.Location = new System.Drawing.Point(31, 36);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(101, 29);
            this.label8.TabIndex = 28;
            this.label8.Text = "Version:";
            // 
            // btnClear
            // 
            this.btnClear.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(129)))), ((int)(((byte)(213)))));
            this.btnClear.ForeColor = System.Drawing.Color.White;
            this.btnClear.Location = new System.Drawing.Point(1284, 74);
            this.btnClear.Margin = new System.Windows.Forms.Padding(4);
            this.btnClear.Name = "btnClear";
            this.btnClear.Size = new System.Drawing.Size(179, 57);
            this.btnClear.TabIndex = 5;
            this.btnClear.Text = "Clear";
            this.btnClear.UseVisualStyleBackColor = false;
            this.btnClear.Click += new System.EventHandler(this.btnClear_Click);
            // 
            // chartStatistic
            // 
            chartArea24.Name = "ChartArea1";
            this.chartStatistic.ChartAreas.Add(chartArea24);
            legend24.Name = "Legend1";
            this.chartStatistic.Legends.Add(legend24);
            this.chartStatistic.Location = new System.Drawing.Point(969, 151);
            this.chartStatistic.Margin = new System.Windows.Forms.Padding(4);
            this.chartStatistic.Name = "chartStatistic";
            series24.ChartArea = "ChartArea1";
            series24.Legend = "Legend1";
            series24.Name = "Series1";
            this.chartStatistic.Series.Add(series24);
            this.chartStatistic.Size = new System.Drawing.Size(1105, 723);
            this.chartStatistic.TabIndex = 4;
            this.chartStatistic.Text = "chart1";
            // 
            // cbbData
            // 
            this.cbbData.BackColor = System.Drawing.Color.White;
            this.cbbData.FormattingEnabled = true;
            this.cbbData.Items.AddRange(new object[] {
            "Trigger 1",
            "Trigger 2",
            "Shifting 1",
            "Shifting 2",
            "Cal",
            "E"});
            this.cbbData.Location = new System.Drawing.Point(947, 84);
            this.cbbData.Margin = new System.Windows.Forms.Padding(4);
            this.cbbData.Name = "cbbData";
            this.cbbData.Size = new System.Drawing.Size(272, 37);
            this.cbbData.TabIndex = 3;
            // 
            // txtEdge
            // 
            this.txtEdge.BackColor = System.Drawing.Color.White;
            this.txtEdge.Location = new System.Drawing.Point(760, 84);
            this.txtEdge.Margin = new System.Windows.Forms.Padding(4);
            this.txtEdge.Name = "txtEdge";
            this.txtEdge.Size = new System.Drawing.Size(132, 34);
            this.txtEdge.TabIndex = 2;
            this.txtEdge.TextChanged += new System.EventHandler(this.txtEdge_TextChanged);
            // 
            // txtZone
            // 
            this.txtZone.BackColor = System.Drawing.Color.White;
            this.txtZone.Location = new System.Drawing.Point(579, 84);
            this.txtZone.Margin = new System.Windows.Forms.Padding(4);
            this.txtZone.Name = "txtZone";
            this.txtZone.Size = new System.Drawing.Size(132, 34);
            this.txtZone.TabIndex = 2;
            this.txtZone.TextChanged += new System.EventHandler(this.txtZone_TextChanged);
            // 
            // txtDevice
            // 
            this.txtDevice.BackColor = System.Drawing.Color.White;
            this.txtDevice.Location = new System.Drawing.Point(392, 84);
            this.txtDevice.Margin = new System.Windows.Forms.Padding(4);
            this.txtDevice.Name = "txtDevice";
            this.txtDevice.Size = new System.Drawing.Size(132, 34);
            this.txtDevice.TabIndex = 2;
            this.txtDevice.TextChanged += new System.EventHandler(this.txtDevice_TextChanged);
            // 
            // txtProduct
            // 
            this.txtProduct.BackColor = System.Drawing.Color.White;
            this.txtProduct.Location = new System.Drawing.Point(213, 84);
            this.txtProduct.Margin = new System.Windows.Forms.Padding(4);
            this.txtProduct.Name = "txtProduct";
            this.txtProduct.Size = new System.Drawing.Size(132, 34);
            this.txtProduct.TabIndex = 2;
            this.txtProduct.TextChanged += new System.EventHandler(this.txtProduct_TextChanged);
            // 
            // txtVersion
            // 
            this.txtVersion.BackColor = System.Drawing.Color.White;
            this.txtVersion.Location = new System.Drawing.Point(36, 84);
            this.txtVersion.Margin = new System.Windows.Forms.Padding(4);
            this.txtVersion.Name = "txtVersion";
            this.txtVersion.Size = new System.Drawing.Size(132, 34);
            this.txtVersion.TabIndex = 1;
            this.txtVersion.TextChanged += new System.EventHandler(this.txtVersion_TextChanged);
            // 
            // dgvDataStatistic
            // 
            this.dgvDataStatistic.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvDataStatistic.Location = new System.Drawing.Point(36, 151);
            this.dgvDataStatistic.Margin = new System.Windows.Forms.Padding(4);
            this.dgvDataStatistic.MultiSelect = false;
            this.dgvDataStatistic.Name = "dgvDataStatistic";
            this.dgvDataStatistic.ReadOnly = true;
            this.dgvDataStatistic.RowHeadersWidth = 51;
            this.dgvDataStatistic.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvDataStatistic.Size = new System.Drawing.Size(900, 723);
            this.dgvDataStatistic.TabIndex = 0;
            this.dgvDataStatistic.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvDataStatistic_CellClick);
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(this.btnBack);
            this.tabPage1.Controls.Add(this.btnNext);
            this.tabPage1.Controls.Add(this.panel2);
            this.tabPage1.Controls.Add(this.cbbZoneCompare);
            this.tabPage1.Controls.Add(this.cbbEdgeCompare);
            this.tabPage1.Controls.Add(this.panel1);
            this.tabPage1.Controls.Add(this.label17);
            this.tabPage1.Controls.Add(this.label16);
            this.tabPage1.Controls.Add(this.label15);
            this.tabPage1.Controls.Add(this.cbbDeviceCompare);
            this.tabPage1.Controls.Add(this.cbbProductCompare);
            this.tabPage1.Controls.Add(this.cbbVersionB);
            this.tabPage1.Controls.Add(this.label11);
            this.tabPage1.Controls.Add(this.cbbVersionA);
            this.tabPage1.Location = new System.Drawing.Point(4, 38);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(2092, 896);
            this.tabPage1.TabIndex = 2;
            this.tabPage1.Text = "Version Comparison";
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // btnBack
            // 
            this.btnBack.Location = new System.Drawing.Point(935, 243);
            this.btnBack.Name = "btnBack";
            this.btnBack.Size = new System.Drawing.Size(115, 41);
            this.btnBack.TabIndex = 13;
            this.btnBack.Text = "Back";
            this.btnBack.UseVisualStyleBackColor = true;
            this.btnBack.Click += new System.EventHandler(this.btnBack_Click);
            // 
            // btnNext
            // 
            this.btnNext.Location = new System.Drawing.Point(1068, 243);
            this.btnNext.Name = "btnNext";
            this.btnNext.Size = new System.Drawing.Size(113, 41);
            this.btnNext.TabIndex = 12;
            this.btnNext.Text = "Next";
            this.btnNext.UseVisualStyleBackColor = true;
            this.btnNext.Click += new System.EventHandler(this.btnNext_Click);
            // 
            // panel2
            // 
            this.panel2.AutoScroll = true;
            this.panel2.AutoScrollMinSize = new System.Drawing.Size(0, 484);
            this.panel2.Controls.Add(this.Chart11B);
            this.panel2.Controls.Add(this.Chart10B);
            this.panel2.Controls.Add(this.Chart9B);
            this.panel2.Controls.Add(this.Chart8B);
            this.panel2.Controls.Add(this.Chart7B);
            this.panel2.Controls.Add(this.Chart6B);
            this.panel2.Controls.Add(this.Chart5B);
            this.panel2.Controls.Add(this.Chart4B);
            this.panel2.Controls.Add(this.Chart3B);
            this.panel2.Controls.Add(this.Chart2B);
            this.panel2.Controls.Add(this.Chart1B);
            this.panel2.Location = new System.Drawing.Point(1187, 115);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(870, 613);
            this.panel2.TabIndex = 11;
            // 
            // Chart11B
            // 
            chartArea25.Name = "ChartArea1";
            this.Chart11B.ChartAreas.Add(chartArea25);
            legend25.Name = "Legend1";
            this.Chart11B.Legends.Add(legend25);
            this.Chart11B.Location = new System.Drawing.Point(15, 4929);
            this.Chart11B.Name = "Chart11B";
            series25.ChartArea = "ChartArea1";
            series25.Legend = "Legend1";
            series25.Name = "Series1";
            this.Chart11B.Series.Add(series25);
            this.Chart11B.Size = new System.Drawing.Size(831, 484);
            this.Chart11B.TabIndex = 10;
            this.Chart11B.Text = "chart3";
            // 
            // Chart10B
            // 
            chartArea26.Name = "ChartArea1";
            this.Chart10B.ChartAreas.Add(chartArea26);
            legend26.Name = "Legend1";
            this.Chart10B.Legends.Add(legend26);
            this.Chart10B.Location = new System.Drawing.Point(15, 4439);
            this.Chart10B.Name = "Chart10B";
            series26.ChartArea = "ChartArea1";
            series26.Legend = "Legend1";
            series26.Name = "Series1";
            this.Chart10B.Series.Add(series26);
            this.Chart10B.Size = new System.Drawing.Size(831, 484);
            this.Chart10B.TabIndex = 9;
            this.Chart10B.Text = "chart3";
            // 
            // Chart9B
            // 
            chartArea27.Name = "ChartArea1";
            this.Chart9B.ChartAreas.Add(chartArea27);
            legend27.Name = "Legend1";
            this.Chart9B.Legends.Add(legend27);
            this.Chart9B.Location = new System.Drawing.Point(15, 3949);
            this.Chart9B.Name = "Chart9B";
            series27.ChartArea = "ChartArea1";
            series27.Legend = "Legend1";
            series27.Name = "Series1";
            this.Chart9B.Series.Add(series27);
            this.Chart9B.Size = new System.Drawing.Size(831, 484);
            this.Chart9B.TabIndex = 8;
            this.Chart9B.Text = "chart3";
            // 
            // Chart8B
            // 
            chartArea28.Name = "ChartArea1";
            this.Chart8B.ChartAreas.Add(chartArea28);
            legend28.Name = "Legend1";
            this.Chart8B.Legends.Add(legend28);
            this.Chart8B.Location = new System.Drawing.Point(15, 3459);
            this.Chart8B.Name = "Chart8B";
            series28.ChartArea = "ChartArea1";
            series28.Legend = "Legend1";
            series28.Name = "Series1";
            this.Chart8B.Series.Add(series28);
            this.Chart8B.Size = new System.Drawing.Size(831, 484);
            this.Chart8B.TabIndex = 7;
            this.Chart8B.Text = "chart1";
            // 
            // Chart7B
            // 
            chartArea29.Name = "ChartArea1";
            this.Chart7B.ChartAreas.Add(chartArea29);
            legend29.Name = "Legend1";
            this.Chart7B.Legends.Add(legend29);
            this.Chart7B.Location = new System.Drawing.Point(15, 2969);
            this.Chart7B.Name = "Chart7B";
            series29.ChartArea = "ChartArea1";
            series29.Legend = "Legend1";
            series29.Name = "Series1";
            this.Chart7B.Series.Add(series29);
            this.Chart7B.Size = new System.Drawing.Size(831, 484);
            this.Chart7B.TabIndex = 6;
            this.Chart7B.Text = "chart1";
            // 
            // Chart6B
            // 
            chartArea30.Name = "ChartArea1";
            this.Chart6B.ChartAreas.Add(chartArea30);
            legend30.Name = "Legend1";
            this.Chart6B.Legends.Add(legend30);
            this.Chart6B.Location = new System.Drawing.Point(15, 2479);
            this.Chart6B.Name = "Chart6B";
            series30.ChartArea = "ChartArea1";
            series30.Legend = "Legend1";
            series30.Name = "Series1";
            this.Chart6B.Series.Add(series30);
            this.Chart6B.Size = new System.Drawing.Size(831, 484);
            this.Chart6B.TabIndex = 5;
            this.Chart6B.Text = "chart1";
            // 
            // Chart5B
            // 
            chartArea31.Name = "ChartArea1";
            this.Chart5B.ChartAreas.Add(chartArea31);
            legend31.Name = "Legend1";
            this.Chart5B.Legends.Add(legend31);
            this.Chart5B.Location = new System.Drawing.Point(18, 1989);
            this.Chart5B.Name = "Chart5B";
            series31.ChartArea = "ChartArea1";
            series31.Legend = "Legend1";
            series31.Name = "Series1";
            this.Chart5B.Series.Add(series31);
            this.Chart5B.Size = new System.Drawing.Size(831, 484);
            this.Chart5B.TabIndex = 4;
            this.Chart5B.Text = "chart1";
            // 
            // Chart4B
            // 
            chartArea32.Name = "ChartArea1";
            this.Chart4B.ChartAreas.Add(chartArea32);
            legend32.Name = "Legend1";
            this.Chart4B.Legends.Add(legend32);
            this.Chart4B.Location = new System.Drawing.Point(18, 1499);
            this.Chart4B.Name = "Chart4B";
            series32.ChartArea = "ChartArea1";
            series32.Legend = "Legend1";
            series32.Name = "Series1";
            this.Chart4B.Series.Add(series32);
            this.Chart4B.Size = new System.Drawing.Size(831, 484);
            this.Chart4B.TabIndex = 3;
            this.Chart4B.Text = "chart1";
            // 
            // Chart3B
            // 
            chartArea33.Name = "ChartArea1";
            this.Chart3B.ChartAreas.Add(chartArea33);
            legend33.Name = "Legend1";
            this.Chart3B.Legends.Add(legend33);
            this.Chart3B.Location = new System.Drawing.Point(18, 1009);
            this.Chart3B.Name = "Chart3B";
            series33.ChartArea = "ChartArea1";
            series33.Legend = "Legend1";
            series33.Name = "Series1";
            this.Chart3B.Series.Add(series33);
            this.Chart3B.Size = new System.Drawing.Size(831, 484);
            this.Chart3B.TabIndex = 2;
            this.Chart3B.Text = "chart1";
            // 
            // Chart2B
            // 
            chartArea34.Name = "ChartArea1";
            this.Chart2B.ChartAreas.Add(chartArea34);
            legend34.Name = "Legend1";
            this.Chart2B.Legends.Add(legend34);
            this.Chart2B.Location = new System.Drawing.Point(18, 509);
            this.Chart2B.Name = "Chart2B";
            series34.ChartArea = "ChartArea1";
            series34.Legend = "Legend1";
            series34.Name = "Series1";
            this.Chart2B.Series.Add(series34);
            this.Chart2B.Size = new System.Drawing.Size(831, 484);
            this.Chart2B.TabIndex = 1;
            this.Chart2B.Text = "chart1";
            // 
            // Chart1B
            // 
            chartArea35.Name = "ChartArea1";
            this.Chart1B.ChartAreas.Add(chartArea35);
            legend35.Name = "Legend1";
            this.Chart1B.Legends.Add(legend35);
            this.Chart1B.Location = new System.Drawing.Point(18, 19);
            this.Chart1B.Name = "Chart1B";
            series35.ChartArea = "ChartArea1";
            series35.Legend = "Legend1";
            series35.Name = "Series1";
            this.Chart1B.Series.Add(series35);
            this.Chart1B.Size = new System.Drawing.Size(831, 484);
            this.Chart1B.TabIndex = 0;
            this.Chart1B.Text = "chart1";
            // 
            // cbbZoneCompare
            // 
            this.cbbZoneCompare.FormattingEnabled = true;
            this.cbbZoneCompare.Location = new System.Drawing.Point(935, 115);
            this.cbbZoneCompare.Name = "cbbZoneCompare";
            this.cbbZoneCompare.Size = new System.Drawing.Size(246, 37);
            this.cbbZoneCompare.TabIndex = 6;
            this.cbbZoneCompare.SelectedIndexChanged += new System.EventHandler(this.cbbZoneCompare_SelectedIndexChanged);
            // 
            // cbbEdgeCompare
            // 
            this.cbbEdgeCompare.FormattingEnabled = true;
            this.cbbEdgeCompare.Location = new System.Drawing.Point(935, 177);
            this.cbbEdgeCompare.Name = "cbbEdgeCompare";
            this.cbbEdgeCompare.Size = new System.Drawing.Size(246, 37);
            this.cbbEdgeCompare.TabIndex = 6;
            this.cbbEdgeCompare.SelectedIndexChanged += new System.EventHandler(this.cbbEdgeCompare_SelectedIndexChanged);
            // 
            // panel1
            // 
            this.panel1.AutoScroll = true;
            this.panel1.AutoScrollMinSize = new System.Drawing.Size(0, 484);
            this.panel1.Controls.Add(this.Chart11A);
            this.panel1.Controls.Add(this.Chart10A);
            this.panel1.Controls.Add(this.Chart9A);
            this.panel1.Controls.Add(this.Chart8A);
            this.panel1.Controls.Add(this.Chart7A);
            this.panel1.Controls.Add(this.Chart6A);
            this.panel1.Controls.Add(this.Chart5A);
            this.panel1.Controls.Add(this.Chart4A);
            this.panel1.Controls.Add(this.Chart3A);
            this.panel1.Controls.Add(this.Chart2A);
            this.panel1.Controls.Add(this.Chart1A);
            this.panel1.Location = new System.Drawing.Point(33, 115);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(870, 613);
            this.panel1.TabIndex = 4;
            // 
            // Chart11A
            // 
            chartArea36.Name = "ChartArea1";
            this.Chart11A.ChartAreas.Add(chartArea36);
            legend36.Name = "Legend1";
            this.Chart11A.Legends.Add(legend36);
            this.Chart11A.Location = new System.Drawing.Point(3, 4919);
            this.Chart11A.Name = "Chart11A";
            series36.ChartArea = "ChartArea1";
            series36.Legend = "Legend1";
            series36.Name = "Series1";
            this.Chart11A.Series.Add(series36);
            this.Chart11A.Size = new System.Drawing.Size(831, 484);
            this.Chart11A.TabIndex = 10;
            this.Chart11A.Text = "chart3";
            // 
            // Chart10A
            // 
            chartArea37.Name = "ChartArea1";
            this.Chart10A.ChartAreas.Add(chartArea37);
            legend37.Name = "Legend1";
            this.Chart10A.Legends.Add(legend37);
            this.Chart10A.Location = new System.Drawing.Point(15, 4429);
            this.Chart10A.Name = "Chart10A";
            series37.ChartArea = "ChartArea1";
            series37.Legend = "Legend1";
            series37.Name = "Series1";
            this.Chart10A.Series.Add(series37);
            this.Chart10A.Size = new System.Drawing.Size(831, 484);
            this.Chart10A.TabIndex = 9;
            this.Chart10A.Text = "chart3";
            // 
            // Chart9A
            // 
            chartArea38.Name = "ChartArea1";
            this.Chart9A.ChartAreas.Add(chartArea38);
            legend38.Name = "Legend1";
            this.Chart9A.Legends.Add(legend38);
            this.Chart9A.Location = new System.Drawing.Point(15, 3939);
            this.Chart9A.Name = "Chart9A";
            series38.ChartArea = "ChartArea1";
            series38.Legend = "Legend1";
            series38.Name = "Series1";
            this.Chart9A.Series.Add(series38);
            this.Chart9A.Size = new System.Drawing.Size(831, 484);
            this.Chart9A.TabIndex = 8;
            this.Chart9A.Text = "chart3";
            // 
            // Chart8A
            // 
            chartArea39.Name = "ChartArea1";
            this.Chart8A.ChartAreas.Add(chartArea39);
            legend39.Name = "Legend1";
            this.Chart8A.Legends.Add(legend39);
            this.Chart8A.Location = new System.Drawing.Point(15, 3449);
            this.Chart8A.Name = "Chart8A";
            series39.ChartArea = "ChartArea1";
            series39.Legend = "Legend1";
            series39.Name = "Series1";
            this.Chart8A.Series.Add(series39);
            this.Chart8A.Size = new System.Drawing.Size(831, 484);
            this.Chart8A.TabIndex = 7;
            this.Chart8A.Text = "chart1";
            // 
            // Chart7A
            // 
            chartArea40.Name = "ChartArea1";
            this.Chart7A.ChartAreas.Add(chartArea40);
            legend40.Name = "Legend1";
            this.Chart7A.Legends.Add(legend40);
            this.Chart7A.Location = new System.Drawing.Point(15, 2959);
            this.Chart7A.Name = "Chart7A";
            series40.ChartArea = "ChartArea1";
            series40.Legend = "Legend1";
            series40.Name = "Series1";
            this.Chart7A.Series.Add(series40);
            this.Chart7A.Size = new System.Drawing.Size(831, 484);
            this.Chart7A.TabIndex = 6;
            this.Chart7A.Text = "chart1";
            // 
            // Chart6A
            // 
            chartArea41.Name = "ChartArea1";
            this.Chart6A.ChartAreas.Add(chartArea41);
            legend41.Name = "Legend1";
            this.Chart6A.Legends.Add(legend41);
            this.Chart6A.Location = new System.Drawing.Point(15, 2469);
            this.Chart6A.Name = "Chart6A";
            series41.ChartArea = "ChartArea1";
            series41.Legend = "Legend1";
            series41.Name = "Series1";
            this.Chart6A.Series.Add(series41);
            this.Chart6A.Size = new System.Drawing.Size(831, 484);
            this.Chart6A.TabIndex = 5;
            this.Chart6A.Text = "chart1";
            // 
            // Chart5A
            // 
            chartArea42.Name = "ChartArea1";
            this.Chart5A.ChartAreas.Add(chartArea42);
            legend42.Name = "Legend1";
            this.Chart5A.Legends.Add(legend42);
            this.Chart5A.Location = new System.Drawing.Point(15, 1979);
            this.Chart5A.Name = "Chart5A";
            series42.ChartArea = "ChartArea1";
            series42.Legend = "Legend1";
            series42.Name = "Series1";
            this.Chart5A.Series.Add(series42);
            this.Chart5A.Size = new System.Drawing.Size(831, 484);
            this.Chart5A.TabIndex = 4;
            this.Chart5A.Text = "chart1";
            // 
            // Chart4A
            // 
            chartArea43.Name = "ChartArea1";
            this.Chart4A.ChartAreas.Add(chartArea43);
            legend43.Name = "Legend1";
            this.Chart4A.Legends.Add(legend43);
            this.Chart4A.Location = new System.Drawing.Point(18, 1489);
            this.Chart4A.Name = "Chart4A";
            series43.ChartArea = "ChartArea1";
            series43.Legend = "Legend1";
            series43.Name = "Series1";
            this.Chart4A.Series.Add(series43);
            this.Chart4A.Size = new System.Drawing.Size(831, 484);
            this.Chart4A.TabIndex = 3;
            this.Chart4A.Text = "chart1";
            // 
            // Chart3A
            // 
            chartArea44.Name = "ChartArea1";
            this.Chart3A.ChartAreas.Add(chartArea44);
            legend44.Name = "Legend1";
            this.Chart3A.Legends.Add(legend44);
            this.Chart3A.Location = new System.Drawing.Point(18, 999);
            this.Chart3A.Name = "Chart3A";
            series44.ChartArea = "ChartArea1";
            series44.Legend = "Legend1";
            series44.Name = "Series1";
            this.Chart3A.Series.Add(series44);
            this.Chart3A.Size = new System.Drawing.Size(831, 484);
            this.Chart3A.TabIndex = 2;
            this.Chart3A.Text = "chart1";
            // 
            // Chart2A
            // 
            chartArea45.Name = "ChartArea1";
            this.Chart2A.ChartAreas.Add(chartArea45);
            legend45.Name = "Legend1";
            this.Chart2A.Legends.Add(legend45);
            this.Chart2A.Location = new System.Drawing.Point(18, 509);
            this.Chart2A.Name = "Chart2A";
            series45.ChartArea = "ChartArea1";
            series45.Legend = "Legend1";
            series45.Name = "Series1";
            this.Chart2A.Series.Add(series45);
            this.Chart2A.Size = new System.Drawing.Size(831, 484);
            this.Chart2A.TabIndex = 1;
            this.Chart2A.Text = "chart1";
            // 
            // Chart1A
            // 
            chartArea46.Name = "ChartArea1";
            this.Chart1A.ChartAreas.Add(chartArea46);
            legend46.Name = "Legend1";
            this.Chart1A.Legends.Add(legend46);
            this.Chart1A.Location = new System.Drawing.Point(18, 19);
            this.Chart1A.Name = "Chart1A";
            series46.ChartArea = "ChartArea1";
            series46.Legend = "Legend1";
            series46.Name = "Series1";
            this.Chart1A.Series.Add(series46);
            this.Chart1A.Size = new System.Drawing.Size(831, 484);
            this.Chart1A.TabIndex = 0;
            this.Chart1A.Text = "chart1";
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Location = new System.Drawing.Point(1540, 41);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(93, 29);
            this.label17.TabIndex = 3;
            this.label17.Text = "Device:";
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Location = new System.Drawing.Point(918, 38);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(102, 29);
            this.label16.TabIndex = 3;
            this.label16.Text = "Product:";
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(471, 38);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(101, 29);
            this.label15.TabIndex = 3;
            this.label15.Text = "Version:";
            // 
            // cbbDeviceCompare
            // 
            this.cbbDeviceCompare.FormattingEnabled = true;
            this.cbbDeviceCompare.Location = new System.Drawing.Point(1672, 38);
            this.cbbDeviceCompare.Name = "cbbDeviceCompare";
            this.cbbDeviceCompare.Size = new System.Drawing.Size(385, 37);
            this.cbbDeviceCompare.TabIndex = 2;
            this.cbbDeviceCompare.SelectedIndexChanged += new System.EventHandler(this.cbbDeviceCompare_SelectedIndexChanged);
            // 
            // cbbProductCompare
            // 
            this.cbbProductCompare.FormattingEnabled = true;
            this.cbbProductCompare.Location = new System.Drawing.Point(1050, 35);
            this.cbbProductCompare.Name = "cbbProductCompare";
            this.cbbProductCompare.Size = new System.Drawing.Size(406, 37);
            this.cbbProductCompare.TabIndex = 2;
            this.cbbProductCompare.SelectedIndexChanged += new System.EventHandler(this.comboProductCompare_SelectedIndexChanged);
            // 
            // cbbVersionB
            // 
            this.cbbVersionB.FormattingEnabled = true;
            this.cbbVersionB.Location = new System.Drawing.Point(603, 35);
            this.cbbVersionB.Name = "cbbVersionB";
            this.cbbVersionB.Size = new System.Drawing.Size(248, 37);
            this.cbbVersionB.TabIndex = 2;
            this.cbbVersionB.SelectedIndexChanged += new System.EventHandler(this.cbbVersionB_SelectedIndexChanged);
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(28, 38);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(101, 29);
            this.label11.TabIndex = 1;
            this.label11.Text = "Version:";
            // 
            // cbbVersionA
            // 
            this.cbbVersionA.FormattingEnabled = true;
            this.cbbVersionA.Location = new System.Drawing.Point(162, 35);
            this.cbbVersionA.Name = "cbbVersionA";
            this.cbbVersionA.Size = new System.Drawing.Size(248, 37);
            this.cbbVersionA.TabIndex = 0;
            this.cbbVersionA.SelectedIndexChanged += new System.EventHandler(this.cbbVersionA_SelectedIndexChanged);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(15, 63);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(175, 36);
            this.label4.TabIndex = 27;
            this.label4.Text = "Comparison";
            // 
            // menuStrip1
            // 
            this.menuStrip1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(12)))), ((int)(((byte)(123)))), ((int)(((byte)(177)))));
            this.menuStrip1.Font = new System.Drawing.Font("Arial", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.menuStrip1.ImageScalingSize = new System.Drawing.Size(20, 20);
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.regressionTestToolStripMenuItem,
            this.evaluationHistoryToolStripMenuItem,
            this.otherZonesResultToolStripMenuItem,
            this.collectResultToolStripMenuItem,
            this.evaluationToolStripMenuItem,
            this.automationTestToolStripMenuItem,
            this.configurationToolStripMenuItem,
            this.jSONGeneratorToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Padding = new System.Windows.Forms.Padding(5, 6, 0, 6);
            this.menuStrip1.Size = new System.Drawing.Size(2156, 43);
            this.menuStrip1.TabIndex = 28;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // regressionTestToolStripMenuItem
            // 
            this.regressionTestToolStripMenuItem.ForeColor = System.Drawing.Color.White;
            this.regressionTestToolStripMenuItem.Name = "regressionTestToolStripMenuItem";
            this.regressionTestToolStripMenuItem.Size = new System.Drawing.Size(138, 31);
            this.regressionTestToolStripMenuItem.Text = "Evaluation";
            this.regressionTestToolStripMenuItem.Click += new System.EventHandler(this.regressionTestToolStripMenuItem_Click_1);
            // 
            // evaluationHistoryToolStripMenuItem
            // 
            this.evaluationHistoryToolStripMenuItem.ForeColor = System.Drawing.SystemColors.Control;
            this.evaluationHistoryToolStripMenuItem.Name = "evaluationHistoryToolStripMenuItem";
            this.evaluationHistoryToolStripMenuItem.Size = new System.Drawing.Size(219, 31);
            this.evaluationHistoryToolStripMenuItem.Text = "Evaluation History";
            this.evaluationHistoryToolStripMenuItem.Click += new System.EventHandler(this.evaluationHistoryToolStripMenuItem_Click);
            // 
            // otherZonesResultToolStripMenuItem
            // 
            this.otherZonesResultToolStripMenuItem.ForeColor = System.Drawing.Color.White;
            this.otherZonesResultToolStripMenuItem.Name = "otherZonesResultToolStripMenuItem";
            this.otherZonesResultToolStripMenuItem.Size = new System.Drawing.Size(244, 31);
            this.otherZonesResultToolStripMenuItem.Text = "Non-STC Evaluation";
            this.otherZonesResultToolStripMenuItem.Click += new System.EventHandler(this.otherZonesResultToolStripMenuItem_Click);
            // 
            // collectResultToolStripMenuItem
            // 
            this.collectResultToolStripMenuItem.ForeColor = System.Drawing.Color.White;
            this.collectResultToolStripMenuItem.Name = "collectResultToolStripMenuItem";
            this.collectResultToolStripMenuItem.Size = new System.Drawing.Size(176, 31);
            this.collectResultToolStripMenuItem.Text = "Collect Result";
            this.collectResultToolStripMenuItem.Click += new System.EventHandler(this.collectResultToolStripMenuItem_Click);
            // 
            // evaluationToolStripMenuItem
            // 
            this.evaluationToolStripMenuItem.Font = new System.Drawing.Font("Arial", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.evaluationToolStripMenuItem.ForeColor = System.Drawing.Color.White;
            this.evaluationToolStripMenuItem.Name = "evaluationToolStripMenuItem";
            this.evaluationToolStripMenuItem.Size = new System.Drawing.Size(155, 31);
            this.evaluationToolStripMenuItem.Text = "Comparison";
            this.evaluationToolStripMenuItem.Click += new System.EventHandler(this.evaluationToolStripMenuItem_Click_1);
            // 
            // automationTestToolStripMenuItem
            // 
            this.automationTestToolStripMenuItem.ForeColor = System.Drawing.Color.White;
            this.automationTestToolStripMenuItem.Name = "automationTestToolStripMenuItem";
            this.automationTestToolStripMenuItem.Size = new System.Drawing.Size(197, 31);
            this.automationTestToolStripMenuItem.Text = "Automation Test";
            this.automationTestToolStripMenuItem.Click += new System.EventHandler(this.automationTestToolStripMenuItem_Click);
            // 
            // configurationToolStripMenuItem
            // 
            this.configurationToolStripMenuItem.Font = new System.Drawing.Font("Arial", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.configurationToolStripMenuItem.ForeColor = System.Drawing.Color.White;
            this.configurationToolStripMenuItem.Name = "configurationToolStripMenuItem";
            this.configurationToolStripMenuItem.Size = new System.Drawing.Size(170, 31);
            this.configurationToolStripMenuItem.Text = "Configuration";
            this.configurationToolStripMenuItem.Click += new System.EventHandler(this.configurationToolStripMenuItem_Click_1);
            // 
            // jSONGeneratorToolStripMenuItem
            // 
            this.jSONGeneratorToolStripMenuItem.ForeColor = System.Drawing.Color.White;
            this.jSONGeneratorToolStripMenuItem.Name = "jSONGeneratorToolStripMenuItem";
            this.jSONGeneratorToolStripMenuItem.Size = new System.Drawing.Size(205, 31);
            this.jSONGeneratorToolStripMenuItem.Text = "JSON Generator";
            this.jSONGeneratorToolStripMenuItem.Click += new System.EventHandler(this.jSONGeneratorToolStripMenuItem_Click);
            // 
            // EvaluationStatisticForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(2156, 1091);
            this.Controls.Add(this.menuStrip1);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.tabControl1);
            this.Margin = new System.Windows.Forms.Padding(4);
            this.Name = "EvaluationStatisticForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "U-nica Mini Tool v2.14";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.EvaluationStatisticForm_FormClosed);
            this.tabControl1.ResumeLayout(false);
            this.v.ResumeLayout(false);
            this.v.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chartStatistic)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvDataStatistic)).EndInit();
            this.tabPage1.ResumeLayout(false);
            this.tabPage1.PerformLayout();
            this.panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.Chart11B)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Chart10B)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Chart9B)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Chart8B)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Chart7B)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Chart6B)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Chart5B)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Chart4B)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Chart3B)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Chart2B)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Chart1B)).EndInit();
            this.panel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.Chart11A)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Chart10A)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Chart9A)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Chart8A)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Chart7A)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Chart6A)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Chart5A)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Chart4A)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Chart3A)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Chart2A)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Chart1A)).EndInit();
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage v;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.DataGridView dgvDataStatistic;
        private System.Windows.Forms.TextBox txtProduct;
        private System.Windows.Forms.TextBox txtVersion;
        private System.Windows.Forms.TextBox txtDevice;
        private System.Windows.Forms.TextBox txtEdge;
        private System.Windows.Forms.TextBox txtZone;
        private System.Windows.Forms.ComboBox cbbData;
        private System.Windows.Forms.DataVisualization.Charting.Chart chartStatistic;
        private System.Windows.Forms.Button btnClear;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem regressionTestToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem evaluationToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem configurationToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem otherZonesResultToolStripMenuItem;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.ComboBox cbbVersionA;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.ComboBox cbbDeviceCompare;
        private System.Windows.Forms.ComboBox cbbProductCompare;
        private System.Windows.Forms.ComboBox cbbVersionB;
        private System.Windows.Forms.ComboBox cbbEdgeCompare;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.ComboBox cbbZoneCompare;
        private System.Windows.Forms.DataVisualization.Charting.Chart Chart1A;
        private System.Windows.Forms.DataVisualization.Charting.Chart Chart2A;
        private System.Windows.Forms.DataVisualization.Charting.Chart Chart7A;
        private System.Windows.Forms.DataVisualization.Charting.Chart Chart6A;
        private System.Windows.Forms.DataVisualization.Charting.Chart Chart5A;
        private System.Windows.Forms.DataVisualization.Charting.Chart Chart4A;
        private System.Windows.Forms.DataVisualization.Charting.Chart Chart3A;
        private System.Windows.Forms.DataVisualization.Charting.Chart Chart9A;
        private System.Windows.Forms.DataVisualization.Charting.Chart Chart8A;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.DataVisualization.Charting.Chart Chart11B;
        private System.Windows.Forms.DataVisualization.Charting.Chart Chart10B;
        private System.Windows.Forms.DataVisualization.Charting.Chart Chart9B;
        private System.Windows.Forms.DataVisualization.Charting.Chart Chart8B;
        private System.Windows.Forms.DataVisualization.Charting.Chart Chart7B;
        private System.Windows.Forms.DataVisualization.Charting.Chart Chart6B;
        private System.Windows.Forms.DataVisualization.Charting.Chart Chart5B;
        private System.Windows.Forms.DataVisualization.Charting.Chart Chart4B;
        private System.Windows.Forms.DataVisualization.Charting.Chart Chart3B;
        private System.Windows.Forms.DataVisualization.Charting.Chart Chart2B;
        private System.Windows.Forms.DataVisualization.Charting.Chart Chart1B;
        private System.Windows.Forms.DataVisualization.Charting.Chart Chart11A;
        private System.Windows.Forms.DataVisualization.Charting.Chart Chart10A;
        private System.Windows.Forms.Button btnBack;
        private System.Windows.Forms.Button btnNext;
        private System.Windows.Forms.ToolStripMenuItem evaluationHistoryToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem collectResultToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem automationTestToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem jSONGeneratorToolStripMenuItem;
    }
}