﻿namespace ReadingResult
{
    partial class OtherZoneAnalyze
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle9 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle10 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle11 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle12 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataVisualization.Charting.ChartArea chartArea2 = new System.Windows.Forms.DataVisualization.Charting.ChartArea();
            System.Windows.Forms.DataVisualization.Charting.Legend legend2 = new System.Windows.Forms.DataVisualization.Charting.Legend();
            System.Windows.Forms.DataVisualization.Charting.Series series2 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle13 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle14 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle15 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle16 = new System.Windows.Forms.DataGridViewCellStyle();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.regressionTestToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.evaluationHistoryToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.otherZonesResultToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.collectResultToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.evaluationToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.automationTestToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.configurationToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.jSONGeneratorToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.lbMessage = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.cbbProductName = new System.Windows.Forms.ComboBox();
            this.label5 = new System.Windows.Forms.Label();
            this.cbbDeviceName = new System.Windows.Forms.ComboBox();
            this.label4 = new System.Windows.Forms.Label();
            this.btnClear = new System.Windows.Forms.Button();
            this.btnBrowseResult = new System.Windows.Forms.Button();
            this.btnGetResult = new System.Windows.Forms.Button();
            this.txtFolderPath = new System.Windows.Forms.TextBox();
            this.lbPath = new System.Windows.Forms.Label();
            this.dgvResult = new System.Windows.Forms.DataGridView();
            this.ChartCDS = new System.Windows.Forms.DataVisualization.Charting.Chart();
            this.FolderDialog = new System.Windows.Forms.FolderBrowserDialog();
            this.btnExport = new System.Windows.Forms.Button();
            this.progressExport = new System.Windows.Forms.ProgressBar();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.txtThreshold = new System.Windows.Forms.TextBox();
            this.btnDraw = new System.Windows.Forms.Button();
            this.btnReset = new System.Windows.Forms.Button();
            this.btnUpdate = new System.Windows.Forms.Button();
            this.dgvCalculateThreshold = new System.Windows.Forms.DataGridView();
            this.label7 = new System.Windows.Forms.Label();
            this.txtFalseNegative = new System.Windows.Forms.TextBox();
            this.txtFalsePositive = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.txtPTA = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.panel1 = new System.Windows.Forms.Panel();
            this.btnExportGraph = new System.Windows.Forms.Button();
            this.menuStrip1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvResult)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChartCDS)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvCalculateThreshold)).BeginInit();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // menuStrip1
            // 
            this.menuStrip1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(12)))), ((int)(((byte)(123)))), ((int)(((byte)(177)))));
            this.menuStrip1.Font = new System.Drawing.Font("Arial", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.menuStrip1.ImageScalingSize = new System.Drawing.Size(20, 20);
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.regressionTestToolStripMenuItem,
            this.evaluationHistoryToolStripMenuItem,
            this.otherZonesResultToolStripMenuItem,
            this.collectResultToolStripMenuItem,
            this.evaluationToolStripMenuItem,
            this.automationTestToolStripMenuItem,
            this.configurationToolStripMenuItem,
            this.jSONGeneratorToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Padding = new System.Windows.Forms.Padding(4, 5, 0, 5);
            this.menuStrip1.Size = new System.Drawing.Size(1911, 36);
            this.menuStrip1.TabIndex = 28;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // regressionTestToolStripMenuItem
            // 
            this.regressionTestToolStripMenuItem.ForeColor = System.Drawing.Color.White;
            this.regressionTestToolStripMenuItem.Name = "regressionTestToolStripMenuItem";
            this.regressionTestToolStripMenuItem.Size = new System.Drawing.Size(108, 26);
            this.regressionTestToolStripMenuItem.Text = "Evaluation";
            this.regressionTestToolStripMenuItem.Click += new System.EventHandler(this.regressionTestToolStripMenuItem_Click);
            // 
            // evaluationHistoryToolStripMenuItem
            // 
            this.evaluationHistoryToolStripMenuItem.ForeColor = System.Drawing.Color.White;
            this.evaluationHistoryToolStripMenuItem.Name = "evaluationHistoryToolStripMenuItem";
            this.evaluationHistoryToolStripMenuItem.Size = new System.Drawing.Size(171, 26);
            this.evaluationHistoryToolStripMenuItem.Text = "Evaluation History";
            this.evaluationHistoryToolStripMenuItem.Click += new System.EventHandler(this.evaluationHistoryToolStripMenuItem_Click);
            // 
            // otherZonesResultToolStripMenuItem
            // 
            this.otherZonesResultToolStripMenuItem.ForeColor = System.Drawing.Color.White;
            this.otherZonesResultToolStripMenuItem.Name = "otherZonesResultToolStripMenuItem";
            this.otherZonesResultToolStripMenuItem.Size = new System.Drawing.Size(192, 26);
            this.otherZonesResultToolStripMenuItem.Text = "Non-STC Evaluation";
            this.otherZonesResultToolStripMenuItem.Click += new System.EventHandler(this.otherZonesResultToolStripMenuItem_Click);
            // 
            // collectResultToolStripMenuItem
            // 
            this.collectResultToolStripMenuItem.ForeColor = System.Drawing.Color.White;
            this.collectResultToolStripMenuItem.Name = "collectResultToolStripMenuItem";
            this.collectResultToolStripMenuItem.Size = new System.Drawing.Size(140, 26);
            this.collectResultToolStripMenuItem.Text = "Collect Result";
            this.collectResultToolStripMenuItem.Click += new System.EventHandler(this.collectResultToolStripMenuItem_Click);
            // 
            // evaluationToolStripMenuItem
            // 
            this.evaluationToolStripMenuItem.Font = new System.Drawing.Font("Arial", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.evaluationToolStripMenuItem.ForeColor = System.Drawing.Color.White;
            this.evaluationToolStripMenuItem.Name = "evaluationToolStripMenuItem";
            this.evaluationToolStripMenuItem.Size = new System.Drawing.Size(125, 26);
            this.evaluationToolStripMenuItem.Text = "Comparison";
            this.evaluationToolStripMenuItem.Click += new System.EventHandler(this.evaluationToolStripMenuItem_Click);
            // 
            // automationTestToolStripMenuItem
            // 
            this.automationTestToolStripMenuItem.ForeColor = System.Drawing.Color.White;
            this.automationTestToolStripMenuItem.Name = "automationTestToolStripMenuItem";
            this.automationTestToolStripMenuItem.Size = new System.Drawing.Size(158, 26);
            this.automationTestToolStripMenuItem.Text = "Automation Test";
            this.automationTestToolStripMenuItem.Click += new System.EventHandler(this.automationTestToolStripMenuItem_Click);
            // 
            // configurationToolStripMenuItem
            // 
            this.configurationToolStripMenuItem.Font = new System.Drawing.Font("Arial", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.configurationToolStripMenuItem.ForeColor = System.Drawing.Color.White;
            this.configurationToolStripMenuItem.Name = "configurationToolStripMenuItem";
            this.configurationToolStripMenuItem.Size = new System.Drawing.Size(134, 26);
            this.configurationToolStripMenuItem.Text = "Configuration";
            this.configurationToolStripMenuItem.Click += new System.EventHandler(this.configurationToolStripMenuItem_Click);
            // 
            // jSONGeneratorToolStripMenuItem
            // 
            this.jSONGeneratorToolStripMenuItem.ForeColor = System.Drawing.Color.White;
            this.jSONGeneratorToolStripMenuItem.Name = "jSONGeneratorToolStripMenuItem";
            this.jSONGeneratorToolStripMenuItem.Size = new System.Drawing.Size(163, 26);
            this.jSONGeneratorToolStripMenuItem.Text = "JSON Generator";
            this.jSONGeneratorToolStripMenuItem.Click += new System.EventHandler(this.jSONGeneratorToolStripMenuItem_Click);
            // 
            // lbMessage
            // 
            this.lbMessage.AutoSize = true;
            this.lbMessage.Font = new System.Drawing.Font("Microsoft Sans Serif", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbMessage.ForeColor = System.Drawing.Color.Red;
            this.lbMessage.Location = new System.Drawing.Point(785, 61);
            this.lbMessage.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lbMessage.Name = "lbMessage";
            this.lbMessage.Size = new System.Drawing.Size(0, 24);
            this.lbMessage.TabIndex = 50;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(23, 172);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(136, 24);
            this.label6.TabIndex = 47;
            this.label6.Text = "Product Name:";
            // 
            // cbbProductName
            // 
            this.cbbProductName.BackColor = System.Drawing.Color.White;
            this.cbbProductName.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbbProductName.FormattingEnabled = true;
            this.cbbProductName.Location = new System.Drawing.Point(207, 164);
            this.cbbProductName.Name = "cbbProductName";
            this.cbbProductName.Size = new System.Drawing.Size(982, 32);
            this.cbbProductName.TabIndex = 46;
            this.cbbProductName.SelectedIndexChanged += new System.EventHandler(this.cbbProductName_SelectedIndexChanged);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(23, 305);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(129, 24);
            this.label5.TabIndex = 45;
            this.label5.Text = "Device Name:";
            // 
            // cbbDeviceName
            // 
            this.cbbDeviceName.BackColor = System.Drawing.Color.White;
            this.cbbDeviceName.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbbDeviceName.FormattingEnabled = true;
            this.cbbDeviceName.Location = new System.Drawing.Point(158, 302);
            this.cbbDeviceName.Name = "cbbDeviceName";
            this.cbbDeviceName.Size = new System.Drawing.Size(236, 32);
            this.cbbDeviceName.TabIndex = 44;
            this.cbbDeviceName.SelectedIndexChanged += new System.EventHandler(this.cbbDeviceName_SelectedIndexChanged);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.label4.Location = new System.Drawing.Point(12, 61);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(718, 24);
            this.label4.TabIndex = 43;
            this.label4.Text = "* Please select folder which includes files format [DeviceID]_[Location][Orig/Cop" +
    "y].csv";
            // 
            // btnClear
            // 
            this.btnClear.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(129)))), ((int)(((byte)(213)))));
            this.btnClear.Font = new System.Drawing.Font("Microsoft Sans Serif", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnClear.ForeColor = System.Drawing.Color.White;
            this.btnClear.Location = new System.Drawing.Point(1219, 164);
            this.btnClear.Name = "btnClear";
            this.btnClear.Size = new System.Drawing.Size(146, 35);
            this.btnClear.TabIndex = 41;
            this.btnClear.Text = "Clear All Data";
            this.btnClear.UseVisualStyleBackColor = false;
            this.btnClear.Click += new System.EventHandler(this.btnClear_Click);
            // 
            // btnBrowseResult
            // 
            this.btnBrowseResult.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(129)))), ((int)(((byte)(213)))));
            this.btnBrowseResult.FlatAppearance.BorderSize = 0;
            this.btnBrowseResult.Font = new System.Drawing.Font("Microsoft Sans Serif", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnBrowseResult.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.btnBrowseResult.Location = new System.Drawing.Point(1219, 107);
            this.btnBrowseResult.Name = "btnBrowseResult";
            this.btnBrowseResult.Size = new System.Drawing.Size(146, 36);
            this.btnBrowseResult.TabIndex = 40;
            this.btnBrowseResult.Text = "Browse";
            this.btnBrowseResult.UseVisualStyleBackColor = false;
            this.btnBrowseResult.Click += new System.EventHandler(this.btnBrowseResult_Click);
            // 
            // btnGetResult
            // 
            this.btnGetResult.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(129)))), ((int)(((byte)(213)))));
            this.btnGetResult.Font = new System.Drawing.Font("Microsoft Sans Serif", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnGetResult.ForeColor = System.Drawing.Color.White;
            this.btnGetResult.Location = new System.Drawing.Point(207, 215);
            this.btnGetResult.Name = "btnGetResult";
            this.btnGetResult.Size = new System.Drawing.Size(175, 43);
            this.btnGetResult.TabIndex = 39;
            this.btnGetResult.Text = "Read Data";
            this.btnGetResult.UseVisualStyleBackColor = false;
            this.btnGetResult.Click += new System.EventHandler(this.btnGetResult_Click);
            // 
            // txtFolderPath
            // 
            this.txtFolderPath.BackColor = System.Drawing.Color.White;
            this.txtFolderPath.Font = new System.Drawing.Font("Microsoft Sans Serif", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtFolderPath.Location = new System.Drawing.Point(207, 105);
            this.txtFolderPath.Multiline = true;
            this.txtFolderPath.Name = "txtFolderPath";
            this.txtFolderPath.ReadOnly = true;
            this.txtFolderPath.Size = new System.Drawing.Size(982, 38);
            this.txtFolderPath.TabIndex = 38;
            // 
            // lbPath
            // 
            this.lbPath.AutoSize = true;
            this.lbPath.Font = new System.Drawing.Font("Microsoft Sans Serif", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbPath.Location = new System.Drawing.Point(22, 111);
            this.lbPath.Name = "lbPath";
            this.lbPath.Size = new System.Drawing.Size(127, 24);
            this.lbPath.TabIndex = 37;
            this.lbPath.Text = "Result Folder:";
            // 
            // dgvResult
            // 
            this.dgvResult.BackgroundColor = System.Drawing.Color.White;
            dataGridViewCellStyle9.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle9.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(34)))), ((int)(((byte)(189)))), ((int)(((byte)(196)))));
            dataGridViewCellStyle9.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle9.ForeColor = System.Drawing.Color.White;
            dataGridViewCellStyle9.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle9.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle9.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgvResult.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle9;
            this.dgvResult.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            dataGridViewCellStyle10.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle10.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle10.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle10.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle10.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle10.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle10.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgvResult.DefaultCellStyle = dataGridViewCellStyle10;
            this.dgvResult.GridColor = System.Drawing.Color.Gray;
            this.dgvResult.Location = new System.Drawing.Point(28, 462);
            this.dgvResult.Name = "dgvResult";
            this.dgvResult.RowHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.Sunken;
            dataGridViewCellStyle11.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle11.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(34)))), ((int)(((byte)(189)))), ((int)(((byte)(196)))));
            dataGridViewCellStyle11.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle11.ForeColor = System.Drawing.SystemColors.InactiveBorder;
            dataGridViewCellStyle11.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle11.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle11.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgvResult.RowHeadersDefaultCellStyle = dataGridViewCellStyle11;
            this.dgvResult.RowHeadersWidth = 51;
            dataGridViewCellStyle12.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle12.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle12.ForeColor = System.Drawing.Color.Black;
            this.dgvResult.RowsDefaultCellStyle = dataGridViewCellStyle12;
            this.dgvResult.Size = new System.Drawing.Size(366, 561);
            this.dgvResult.TabIndex = 61;
            // 
            // ChartCDS
            // 
            chartArea2.Name = "ChartArea1";
            this.ChartCDS.ChartAreas.Add(chartArea2);
            legend2.Name = "Legend1";
            this.ChartCDS.Legends.Add(legend2);
            this.ChartCDS.Location = new System.Drawing.Point(14, 13);
            this.ChartCDS.Margin = new System.Windows.Forms.Padding(0);
            this.ChartCDS.Name = "ChartCDS";
            series2.ChartArea = "ChartArea1";
            series2.Legend = "Legend1";
            series2.Name = "Series1";
            this.ChartCDS.Series.Add(series2);
            this.ChartCDS.Size = new System.Drawing.Size(1048, 657);
            this.ChartCDS.TabIndex = 62;
            // 
            // btnExport
            // 
            this.btnExport.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(129)))), ((int)(((byte)(213)))));
            this.btnExport.Cursor = System.Windows.Forms.Cursors.AppStarting;
            this.btnExport.Font = new System.Drawing.Font("Microsoft Sans Serif", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnExport.ForeColor = System.Drawing.Color.White;
            this.btnExport.Location = new System.Drawing.Point(398, 215);
            this.btnExport.Name = "btnExport";
            this.btnExport.Size = new System.Drawing.Size(176, 43);
            this.btnExport.TabIndex = 63;
            this.btnExport.Text = "Export Data Set";
            this.btnExport.UseVisualStyleBackColor = false;
            this.btnExport.Click += new System.EventHandler(this.btnExport_Click);
            // 
            // progressExport
            // 
            this.progressExport.Location = new System.Drawing.Point(789, 215);
            this.progressExport.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.progressExport.Name = "progressExport";
            this.progressExport.Size = new System.Drawing.Size(399, 43);
            this.progressExport.TabIndex = 66;
            // 
            // label2
            // 
            this.label2.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label2.Location = new System.Drawing.Point(25, 283);
            this.label2.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(1337, 1);
            this.label2.TabIndex = 67;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(24, 362);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(101, 24);
            this.label1.TabIndex = 68;
            this.label1.Text = "Threshold:";
            // 
            // txtThreshold
            // 
            this.txtThreshold.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtThreshold.Location = new System.Drawing.Point(158, 353);
            this.txtThreshold.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.txtThreshold.Name = "txtThreshold";
            this.txtThreshold.Size = new System.Drawing.Size(236, 29);
            this.txtThreshold.TabIndex = 69;
            // 
            // btnDraw
            // 
            this.btnDraw.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(129)))), ((int)(((byte)(213)))));
            this.btnDraw.Font = new System.Drawing.Font("Microsoft Sans Serif", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnDraw.ForeColor = System.Drawing.Color.White;
            this.btnDraw.Location = new System.Drawing.Point(94, 401);
            this.btnDraw.Name = "btnDraw";
            this.btnDraw.Size = new System.Drawing.Size(89, 40);
            this.btnDraw.TabIndex = 70;
            this.btnDraw.Text = "Draw";
            this.btnDraw.UseVisualStyleBackColor = false;
            this.btnDraw.Click += new System.EventHandler(this.btnDraw_Click);
            // 
            // btnReset
            // 
            this.btnReset.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(129)))), ((int)(((byte)(213)))));
            this.btnReset.Font = new System.Drawing.Font("Microsoft Sans Serif", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnReset.ForeColor = System.Drawing.Color.White;
            this.btnReset.Location = new System.Drawing.Point(305, 401);
            this.btnReset.Name = "btnReset";
            this.btnReset.Size = new System.Drawing.Size(89, 41);
            this.btnReset.TabIndex = 71;
            this.btnReset.Text = "Reset";
            this.btnReset.UseVisualStyleBackColor = false;
            this.btnReset.Click += new System.EventHandler(this.btnReset_Click);
            // 
            // btnUpdate
            // 
            this.btnUpdate.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(129)))), ((int)(((byte)(213)))));
            this.btnUpdate.Font = new System.Drawing.Font("Microsoft Sans Serif", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnUpdate.ForeColor = System.Drawing.Color.White;
            this.btnUpdate.Location = new System.Drawing.Point(199, 402);
            this.btnUpdate.Name = "btnUpdate";
            this.btnUpdate.Size = new System.Drawing.Size(89, 40);
            this.btnUpdate.TabIndex = 70;
            this.btnUpdate.Text = "Update";
            this.btnUpdate.UseVisualStyleBackColor = false;
            this.btnUpdate.Click += new System.EventHandler(this.btnUpdate_Click);
            // 
            // dgvCalculateThreshold
            // 
            this.dgvCalculateThreshold.BackgroundColor = System.Drawing.Color.White;
            dataGridViewCellStyle13.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle13.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(34)))), ((int)(((byte)(189)))), ((int)(((byte)(196)))));
            dataGridViewCellStyle13.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle13.ForeColor = System.Drawing.Color.White;
            dataGridViewCellStyle13.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle13.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle13.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgvCalculateThreshold.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle13;
            this.dgvCalculateThreshold.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            dataGridViewCellStyle14.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle14.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle14.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle14.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle14.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle14.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle14.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgvCalculateThreshold.DefaultCellStyle = dataGridViewCellStyle14;
            this.dgvCalculateThreshold.GridColor = System.Drawing.Color.Gray;
            this.dgvCalculateThreshold.Location = new System.Drawing.Point(422, 462);
            this.dgvCalculateThreshold.Name = "dgvCalculateThreshold";
            this.dgvCalculateThreshold.RowHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.Sunken;
            dataGridViewCellStyle15.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle15.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(34)))), ((int)(((byte)(189)))), ((int)(((byte)(196)))));
            dataGridViewCellStyle15.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle15.ForeColor = System.Drawing.SystemColors.InactiveBorder;
            dataGridViewCellStyle15.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle15.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle15.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgvCalculateThreshold.RowHeadersDefaultCellStyle = dataGridViewCellStyle15;
            this.dgvCalculateThreshold.RowHeadersWidth = 51;
            dataGridViewCellStyle16.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle16.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle16.ForeColor = System.Drawing.Color.Black;
            this.dgvCalculateThreshold.RowsDefaultCellStyle = dataGridViewCellStyle16;
            this.dgvCalculateThreshold.Size = new System.Drawing.Size(301, 561);
            this.dgvCalculateThreshold.TabIndex = 72;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(418, 305);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(140, 24);
            this.label7.TabIndex = 68;
            this.label7.Text = "False Negative:";
            // 
            // txtFalseNegative
            // 
            this.txtFalseNegative.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtFalseNegative.Location = new System.Drawing.Point(559, 301);
            this.txtFalseNegative.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.txtFalseNegative.Name = "txtFalseNegative";
            this.txtFalseNegative.ReadOnly = true;
            this.txtFalseNegative.Size = new System.Drawing.Size(164, 29);
            this.txtFalseNegative.TabIndex = 69;
            // 
            // txtFalsePositive
            // 
            this.txtFalsePositive.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtFalsePositive.Location = new System.Drawing.Point(559, 353);
            this.txtFalsePositive.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.txtFalsePositive.Name = "txtFalsePositive";
            this.txtFalsePositive.ReadOnly = true;
            this.txtFalsePositive.Size = new System.Drawing.Size(164, 29);
            this.txtFalsePositive.TabIndex = 74;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.Location = new System.Drawing.Point(418, 358);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(130, 24);
            this.label8.TabIndex = 73;
            this.label8.Text = "False Positive:";
            // 
            // txtPTA
            // 
            this.txtPTA.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtPTA.Location = new System.Drawing.Point(559, 402);
            this.txtPTA.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.txtPTA.Name = "txtPTA";
            this.txtPTA.ReadOnly = true;
            this.txtPTA.Size = new System.Drawing.Size(164, 29);
            this.txtPTA.TabIndex = 76;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Microsoft Sans Serif", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.Location = new System.Drawing.Point(418, 410);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(52, 24);
            this.label9.TabIndex = 75;
            this.label9.Text = "PTA:";
            // 
            // panel1
            // 
            this.panel1.AutoScroll = true;
            this.panel1.Controls.Add(this.ChartCDS);
            this.panel1.Location = new System.Drawing.Point(741, 300);
            this.panel1.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(1154, 723);
            this.panel1.TabIndex = 77;
            // 
            // btnExportGraph
            // 
            this.btnExportGraph.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(129)))), ((int)(((byte)(213)))));
            this.btnExportGraph.Cursor = System.Windows.Forms.Cursors.AppStarting;
            this.btnExportGraph.Font = new System.Drawing.Font("Microsoft Sans Serif", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnExportGraph.ForeColor = System.Drawing.Color.White;
            this.btnExportGraph.Location = new System.Drawing.Point(590, 215);
            this.btnExportGraph.Name = "btnExportGraph";
            this.btnExportGraph.Size = new System.Drawing.Size(176, 43);
            this.btnExportGraph.TabIndex = 78;
            this.btnExportGraph.Text = "Export Graph";
            this.btnExportGraph.UseVisualStyleBackColor = false;
            this.btnExportGraph.Click += new System.EventHandler(this.btnExportGraph_Click);
            // 
            // OtherZoneAnalyze
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(1911, 862);
            this.Controls.Add(this.btnExportGraph);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.txtPTA);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.txtFalsePositive);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.dgvCalculateThreshold);
            this.Controls.Add(this.btnReset);
            this.Controls.Add(this.btnUpdate);
            this.Controls.Add(this.btnDraw);
            this.Controls.Add(this.txtFalseNegative);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.txtThreshold);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.progressExport);
            this.Controls.Add(this.btnExport);
            this.Controls.Add(this.dgvResult);
            this.Controls.Add(this.lbMessage);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.cbbProductName);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.cbbDeviceName);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.btnClear);
            this.Controls.Add(this.btnBrowseResult);
            this.Controls.Add(this.btnGetResult);
            this.Controls.Add(this.txtFolderPath);
            this.Controls.Add(this.lbPath);
            this.Controls.Add(this.menuStrip1);
            this.Name = "OtherZoneAnalyze";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "U-nica Mini Tool v2.14";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.OtherZoneAnalyze_FormClosed);
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvResult)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChartCDS)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvCalculateThreshold)).EndInit();
            this.panel1.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem regressionTestToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem otherZonesResultToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem evaluationToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem configurationToolStripMenuItem;
        private System.Windows.Forms.Label lbMessage;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.ComboBox cbbProductName;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.ComboBox cbbDeviceName;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Button btnClear;
        private System.Windows.Forms.Button btnBrowseResult;
        private System.Windows.Forms.Button btnGetResult;
        private System.Windows.Forms.TextBox txtFolderPath;
        private System.Windows.Forms.Label lbPath;
        private System.Windows.Forms.DataGridView dgvResult;
        private System.Windows.Forms.DataVisualization.Charting.Chart ChartCDS;
        private System.Windows.Forms.FolderBrowserDialog FolderDialog;
        private System.Windows.Forms.Button btnExport;
        private System.Windows.Forms.ProgressBar progressExport;
        private System.Windows.Forms.ToolStripMenuItem evaluationHistoryToolStripMenuItem;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox txtThreshold;
        private System.Windows.Forms.Button btnDraw;
        private System.Windows.Forms.Button btnReset;
        private System.Windows.Forms.Button btnUpdate;
        private System.Windows.Forms.DataGridView dgvCalculateThreshold;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox txtFalseNegative;
        private System.Windows.Forms.TextBox txtFalsePositive;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.TextBox txtPTA;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.ToolStripMenuItem collectResultToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem automationTestToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem jSONGeneratorToolStripMenuItem;
        private System.Windows.Forms.Button btnExportGraph;
    }
}