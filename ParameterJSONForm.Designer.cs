﻿namespace ReadingResult
{
    partial class ParameterJSONForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle13 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle14 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle15 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle16 = new System.Windows.Forms.DataGridViewCellStyle();
            this.label2 = new System.Windows.Forms.Label();
            this.dgvDevice = new System.Windows.Forms.DataGridView();
            this.lbMessage = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.cbbProductName = new System.Windows.Forms.ComboBox();
            this.label4 = new System.Windows.Forms.Label();
            this.btnClear = new System.Windows.Forms.Button();
            this.btnBrowseResult = new System.Windows.Forms.Button();
            this.btnGetResult = new System.Windows.Forms.Button();
            this.txtFolderPath = new System.Windows.Forms.TextBox();
            this.lbPath = new System.Windows.Forms.Label();
            this.configurationToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.automationTestToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.evaluationToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.collectResultToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.otherZonesResultToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.evaluationHistoryToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.regressionTestToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.FolderDialog = new System.Windows.Forms.FolderBrowserDialog();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.jSONToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.label3 = new System.Windows.Forms.Label();
            this.txtJSONPath = new System.Windows.Forms.TextBox();
            this.btnBrowseJSON = new System.Windows.Forms.Button();
            this.openFileDialog1 = new System.Windows.Forms.OpenFileDialog();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.btnExportJSON = new System.Windows.Forms.Button();
            this.btnReset = new System.Windows.Forms.Button();
            this.btnSet = new System.Windows.Forms.Button();
            this.txtComment = new System.Windows.Forms.TextBox();
            this.txtVersion = new System.Windows.Forms.TextBox();
            this.txtThresholdBlur = new System.Windows.Forms.TextBox();
            this.txtThreshold = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.txtDeviceName = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.btnMergeData = new System.Windows.Forms.Button();
            this.label9 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.txtRawResult = new System.Windows.Forms.TextBox();
            this.btnBrowseRaw = new System.Windows.Forms.Button();
            this.label12 = new System.Windows.Forms.Label();
            this.txtMeanOrig = new System.Windows.Forms.TextBox();
            this.cbDeviceID = new System.Windows.Forms.CheckBox();
            this.label14 = new System.Windows.Forms.Label();
            this.txtLocation = new System.Windows.Forms.TextBox();
            this.label13 = new System.Windows.Forms.Label();
            this.txtDev = new System.Windows.Forms.TextBox();
            this.label15 = new System.Windows.Forms.Label();
            this.txtPercentage = new System.Windows.Forms.TextBox();
            this.lblPercentage = new System.Windows.Forms.Label();
            this.txtPStd = new System.Windows.Forms.TextBox();
            ((System.ComponentModel.ISupportInitialize)(this.dgvDevice)).BeginInit();
            this.menuStrip1.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // label2
            // 
            this.label2.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label2.Location = new System.Drawing.Point(0, 463);
            this.label2.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(1620, 10);
            this.label2.TabIndex = 93;
            // 
            // dgvDevice
            // 
            this.dgvDevice.BackgroundColor = System.Drawing.Color.White;
            dataGridViewCellStyle13.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle13.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(34)))), ((int)(((byte)(189)))), ((int)(((byte)(196)))));
            dataGridViewCellStyle13.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle13.ForeColor = System.Drawing.Color.White;
            dataGridViewCellStyle13.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle13.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle13.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgvDevice.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle13;
            this.dgvDevice.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            dataGridViewCellStyle14.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle14.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle14.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle14.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle14.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle14.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle14.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgvDevice.DefaultCellStyle = dataGridViewCellStyle14;
            this.dgvDevice.GridColor = System.Drawing.Color.Gray;
            this.dgvDevice.Location = new System.Drawing.Point(30, 502);
            this.dgvDevice.MultiSelect = false;
            this.dgvDevice.Name = "dgvDevice";
            this.dgvDevice.RowHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.Sunken;
            dataGridViewCellStyle15.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle15.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(34)))), ((int)(((byte)(189)))), ((int)(((byte)(196)))));
            dataGridViewCellStyle15.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle15.ForeColor = System.Drawing.SystemColors.InactiveBorder;
            dataGridViewCellStyle15.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle15.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle15.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgvDevice.RowHeadersDefaultCellStyle = dataGridViewCellStyle15;
            this.dgvDevice.RowHeadersWidth = 51;
            dataGridViewCellStyle16.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle16.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle16.ForeColor = System.Drawing.Color.Black;
            this.dgvDevice.RowsDefaultCellStyle = dataGridViewCellStyle16;
            this.dgvDevice.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvDevice.Size = new System.Drawing.Size(340, 511);
            this.dgvDevice.TabIndex = 90;
            this.dgvDevice.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvDevice_CellClick);
            // 
            // lbMessage
            // 
            this.lbMessage.AutoSize = true;
            this.lbMessage.Font = new System.Drawing.Font("Microsoft Sans Serif", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbMessage.ForeColor = System.Drawing.Color.Red;
            this.lbMessage.Location = new System.Drawing.Point(709, 198);
            this.lbMessage.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lbMessage.Name = "lbMessage";
            this.lbMessage.Size = new System.Drawing.Size(0, 24);
            this.lbMessage.TabIndex = 89;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(32, 344);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(143, 24);
            this.label6.TabIndex = 88;
            this.label6.Text = "*Product Name:";
            // 
            // cbbProductName
            // 
            this.cbbProductName.BackColor = System.Drawing.Color.White;
            this.cbbProductName.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbbProductName.FormattingEnabled = true;
            this.cbbProductName.Location = new System.Drawing.Point(265, 340);
            this.cbbProductName.Name = "cbbProductName";
            this.cbbProductName.Size = new System.Drawing.Size(772, 32);
            this.cbbProductName.TabIndex = 87;
            this.cbbProductName.SelectedIndexChanged += new System.EventHandler(this.cbbProductName_SelectedIndexChanged);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.label4.Location = new System.Drawing.Point(32, 227);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(678, 24);
            this.label4.TabIndex = 84;
            this.label4.Text = "* Please select folder which includes files format [Device Model]_[Orig/Copy].csv" +
    "";
            // 
            // btnClear
            // 
            this.btnClear.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(129)))), ((int)(((byte)(213)))));
            this.btnClear.Font = new System.Drawing.Font("Microsoft Sans Serif", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnClear.ForeColor = System.Drawing.Color.White;
            this.btnClear.Location = new System.Drawing.Point(1066, 339);
            this.btnClear.Name = "btnClear";
            this.btnClear.Size = new System.Drawing.Size(161, 40);
            this.btnClear.TabIndex = 83;
            this.btnClear.Text = "Clear All Data";
            this.btnClear.UseVisualStyleBackColor = false;
            // 
            // btnBrowseResult
            // 
            this.btnBrowseResult.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(129)))), ((int)(((byte)(213)))));
            this.btnBrowseResult.FlatAppearance.BorderSize = 0;
            this.btnBrowseResult.Font = new System.Drawing.Font("Microsoft Sans Serif", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnBrowseResult.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.btnBrowseResult.Location = new System.Drawing.Point(1066, 279);
            this.btnBrowseResult.Name = "btnBrowseResult";
            this.btnBrowseResult.Size = new System.Drawing.Size(161, 44);
            this.btnBrowseResult.TabIndex = 82;
            this.btnBrowseResult.Text = "Browse";
            this.btnBrowseResult.UseVisualStyleBackColor = false;
            this.btnBrowseResult.Click += new System.EventHandler(this.btnBrowseResult_Click);
            // 
            // btnGetResult
            // 
            this.btnGetResult.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(129)))), ((int)(((byte)(213)))));
            this.btnGetResult.Font = new System.Drawing.Font("Microsoft Sans Serif", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnGetResult.ForeColor = System.Drawing.Color.White;
            this.btnGetResult.Location = new System.Drawing.Point(1267, 279);
            this.btnGetResult.Name = "btnGetResult";
            this.btnGetResult.Size = new System.Drawing.Size(155, 161);
            this.btnGetResult.TabIndex = 81;
            this.btnGetResult.Text = "Read Data";
            this.btnGetResult.UseVisualStyleBackColor = false;
            this.btnGetResult.Click += new System.EventHandler(this.btnGetResult_Click);
            // 
            // txtFolderPath
            // 
            this.txtFolderPath.BackColor = System.Drawing.Color.White;
            this.txtFolderPath.Font = new System.Drawing.Font("Microsoft Sans Serif", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtFolderPath.Location = new System.Drawing.Point(265, 282);
            this.txtFolderPath.Multiline = true;
            this.txtFolderPath.Name = "txtFolderPath";
            this.txtFolderPath.ReadOnly = true;
            this.txtFolderPath.Size = new System.Drawing.Size(772, 32);
            this.txtFolderPath.TabIndex = 80;
            // 
            // lbPath
            // 
            this.lbPath.AutoSize = true;
            this.lbPath.Font = new System.Drawing.Font("Microsoft Sans Serif", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbPath.Location = new System.Drawing.Point(31, 285);
            this.lbPath.Name = "lbPath";
            this.lbPath.Size = new System.Drawing.Size(134, 24);
            this.lbPath.TabIndex = 79;
            this.lbPath.Text = "*Result Folder:";
            // 
            // configurationToolStripMenuItem
            // 
            this.configurationToolStripMenuItem.Font = new System.Drawing.Font("Arial", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.configurationToolStripMenuItem.ForeColor = System.Drawing.Color.White;
            this.configurationToolStripMenuItem.Name = "configurationToolStripMenuItem";
            this.configurationToolStripMenuItem.Size = new System.Drawing.Size(134, 26);
            this.configurationToolStripMenuItem.Text = "Configuration";
            this.configurationToolStripMenuItem.Click += new System.EventHandler(this.configurationToolStripMenuItem_Click);
            // 
            // automationTestToolStripMenuItem
            // 
            this.automationTestToolStripMenuItem.ForeColor = System.Drawing.Color.White;
            this.automationTestToolStripMenuItem.Name = "automationTestToolStripMenuItem";
            this.automationTestToolStripMenuItem.Size = new System.Drawing.Size(158, 26);
            this.automationTestToolStripMenuItem.Text = "Automation Test";
            this.automationTestToolStripMenuItem.Click += new System.EventHandler(this.automationTestToolStripMenuItem_Click);
            // 
            // evaluationToolStripMenuItem
            // 
            this.evaluationToolStripMenuItem.Font = new System.Drawing.Font("Arial", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.evaluationToolStripMenuItem.ForeColor = System.Drawing.Color.White;
            this.evaluationToolStripMenuItem.Name = "evaluationToolStripMenuItem";
            this.evaluationToolStripMenuItem.Size = new System.Drawing.Size(125, 26);
            this.evaluationToolStripMenuItem.Text = "Comparison";
            this.evaluationToolStripMenuItem.Click += new System.EventHandler(this.evaluationToolStripMenuItem_Click);
            // 
            // collectResultToolStripMenuItem
            // 
            this.collectResultToolStripMenuItem.ForeColor = System.Drawing.Color.White;
            this.collectResultToolStripMenuItem.Name = "collectResultToolStripMenuItem";
            this.collectResultToolStripMenuItem.Size = new System.Drawing.Size(140, 26);
            this.collectResultToolStripMenuItem.Text = "Collect Result";
            this.collectResultToolStripMenuItem.Click += new System.EventHandler(this.collectResultToolStripMenuItem_Click);
            // 
            // otherZonesResultToolStripMenuItem
            // 
            this.otherZonesResultToolStripMenuItem.ForeColor = System.Drawing.Color.White;
            this.otherZonesResultToolStripMenuItem.Name = "otherZonesResultToolStripMenuItem";
            this.otherZonesResultToolStripMenuItem.Size = new System.Drawing.Size(192, 26);
            this.otherZonesResultToolStripMenuItem.Text = "Non-STC Evaluation";
            this.otherZonesResultToolStripMenuItem.Click += new System.EventHandler(this.otherZonesResultToolStripMenuItem_Click);
            // 
            // evaluationHistoryToolStripMenuItem
            // 
            this.evaluationHistoryToolStripMenuItem.ForeColor = System.Drawing.Color.White;
            this.evaluationHistoryToolStripMenuItem.Name = "evaluationHistoryToolStripMenuItem";
            this.evaluationHistoryToolStripMenuItem.Size = new System.Drawing.Size(171, 26);
            this.evaluationHistoryToolStripMenuItem.Text = "Evaluation History";
            this.evaluationHistoryToolStripMenuItem.Click += new System.EventHandler(this.evaluationHistoryToolStripMenuItem_Click);
            // 
            // regressionTestToolStripMenuItem
            // 
            this.regressionTestToolStripMenuItem.ForeColor = System.Drawing.Color.White;
            this.regressionTestToolStripMenuItem.Name = "regressionTestToolStripMenuItem";
            this.regressionTestToolStripMenuItem.Size = new System.Drawing.Size(108, 26);
            this.regressionTestToolStripMenuItem.Text = "Evaluation";
            this.regressionTestToolStripMenuItem.Click += new System.EventHandler(this.regressionTestToolStripMenuItem_Click);
            // 
            // menuStrip1
            // 
            this.menuStrip1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(12)))), ((int)(((byte)(123)))), ((int)(((byte)(177)))));
            this.menuStrip1.Font = new System.Drawing.Font("Arial", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.menuStrip1.ImageScalingSize = new System.Drawing.Size(20, 20);
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.regressionTestToolStripMenuItem,
            this.evaluationHistoryToolStripMenuItem,
            this.otherZonesResultToolStripMenuItem,
            this.collectResultToolStripMenuItem,
            this.evaluationToolStripMenuItem,
            this.automationTestToolStripMenuItem,
            this.configurationToolStripMenuItem,
            this.jSONToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Padding = new System.Windows.Forms.Padding(3, 4, 0, 4);
            this.menuStrip1.Size = new System.Drawing.Size(1620, 34);
            this.menuStrip1.TabIndex = 78;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // jSONToolStripMenuItem
            // 
            this.jSONToolStripMenuItem.ForeColor = System.Drawing.Color.White;
            this.jSONToolStripMenuItem.Name = "jSONToolStripMenuItem";
            this.jSONToolStripMenuItem.Size = new System.Drawing.Size(163, 26);
            this.jSONToolStripMenuItem.Text = "JSON Generator";
            this.jSONToolStripMenuItem.Click += new System.EventHandler(this.jSONToolStripMenuItem_Click);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(32, 401);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(135, 24);
            this.label3.TabIndex = 79;
            this.label3.Text = "*Device JSON:";
            // 
            // txtJSONPath
            // 
            this.txtJSONPath.BackColor = System.Drawing.Color.White;
            this.txtJSONPath.Font = new System.Drawing.Font("Microsoft Sans Serif", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtJSONPath.Location = new System.Drawing.Point(265, 395);
            this.txtJSONPath.Multiline = true;
            this.txtJSONPath.Name = "txtJSONPath";
            this.txtJSONPath.ReadOnly = true;
            this.txtJSONPath.Size = new System.Drawing.Size(772, 35);
            this.txtJSONPath.TabIndex = 80;
            // 
            // btnBrowseJSON
            // 
            this.btnBrowseJSON.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(129)))), ((int)(((byte)(213)))));
            this.btnBrowseJSON.FlatAppearance.BorderSize = 0;
            this.btnBrowseJSON.Font = new System.Drawing.Font("Microsoft Sans Serif", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnBrowseJSON.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.btnBrowseJSON.Location = new System.Drawing.Point(1066, 397);
            this.btnBrowseJSON.Name = "btnBrowseJSON";
            this.btnBrowseJSON.Size = new System.Drawing.Size(161, 41);
            this.btnBrowseJSON.TabIndex = 82;
            this.btnBrowseJSON.Text = "Browse";
            this.btnBrowseJSON.UseVisualStyleBackColor = false;
            this.btnBrowseJSON.Click += new System.EventHandler(this.btnBrowseJSON_Click);
            // 
            // openFileDialog1
            // 
            this.openFileDialog1.FileName = "openFileDialog1";
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.btnExportJSON);
            this.groupBox1.Controls.Add(this.btnReset);
            this.groupBox1.Controls.Add(this.btnSet);
            this.groupBox1.Controls.Add(this.txtComment);
            this.groupBox1.Controls.Add(this.txtVersion);
            this.groupBox1.Controls.Add(this.txtThresholdBlur);
            this.groupBox1.Controls.Add(this.txtThreshold);
            this.groupBox1.Controls.Add(this.label7);
            this.groupBox1.Controls.Add(this.label11);
            this.groupBox1.Controls.Add(this.label8);
            this.groupBox1.Controls.Add(this.txtDeviceName);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this.label5);
            this.groupBox1.Font = new System.Drawing.Font("Microsoft Sans Serif", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox1.Location = new System.Drawing.Point(879, 487);
            this.groupBox1.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Padding = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.groupBox1.Size = new System.Drawing.Size(716, 567);
            this.groupBox1.TabIndex = 102;
            this.groupBox1.TabStop = false;
            // 
            // btnExportJSON
            // 
            this.btnExportJSON.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(129)))), ((int)(((byte)(213)))));
            this.btnExportJSON.ForeColor = System.Drawing.Color.White;
            this.btnExportJSON.Location = new System.Drawing.Point(201, 505);
            this.btnExportJSON.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.btnExportJSON.Name = "btnExportJSON";
            this.btnExportJSON.Size = new System.Drawing.Size(471, 46);
            this.btnExportJSON.TabIndex = 6;
            this.btnExportJSON.Text = "Export JSON";
            this.btnExportJSON.UseVisualStyleBackColor = false;
            this.btnExportJSON.Click += new System.EventHandler(this.btnExportJSON_Click);
            // 
            // btnReset
            // 
            this.btnReset.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(129)))), ((int)(((byte)(213)))));
            this.btnReset.ForeColor = System.Drawing.Color.White;
            this.btnReset.Location = new System.Drawing.Point(201, 437);
            this.btnReset.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.btnReset.Name = "btnReset";
            this.btnReset.Size = new System.Drawing.Size(218, 46);
            this.btnReset.TabIndex = 5;
            this.btnReset.Text = "Reset";
            this.btnReset.UseVisualStyleBackColor = false;
            this.btnReset.Click += new System.EventHandler(this.btnReset_Click);
            // 
            // btnSet
            // 
            this.btnSet.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(129)))), ((int)(((byte)(213)))));
            this.btnSet.ForeColor = System.Drawing.Color.White;
            this.btnSet.Location = new System.Drawing.Point(444, 437);
            this.btnSet.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.btnSet.Name = "btnSet";
            this.btnSet.Size = new System.Drawing.Size(228, 46);
            this.btnSet.TabIndex = 4;
            this.btnSet.Text = "Set";
            this.btnSet.UseVisualStyleBackColor = false;
            this.btnSet.Click += new System.EventHandler(this.btnSet_Click);
            // 
            // txtComment
            // 
            this.txtComment.BackColor = System.Drawing.Color.White;
            this.txtComment.Location = new System.Drawing.Point(201, 308);
            this.txtComment.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.txtComment.Multiline = true;
            this.txtComment.Name = "txtComment";
            this.txtComment.Size = new System.Drawing.Size(477, 107);
            this.txtComment.TabIndex = 3;
            // 
            // txtVersion
            // 
            this.txtVersion.BackColor = System.Drawing.Color.White;
            this.txtVersion.Location = new System.Drawing.Point(201, 240);
            this.txtVersion.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.txtVersion.Name = "txtVersion";
            this.txtVersion.Size = new System.Drawing.Size(477, 28);
            this.txtVersion.TabIndex = 3;
            this.txtVersion.Text = "1.0";
            // 
            // txtThresholdBlur
            // 
            this.txtThresholdBlur.BackColor = System.Drawing.Color.White;
            this.txtThresholdBlur.Location = new System.Drawing.Point(201, 176);
            this.txtThresholdBlur.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.txtThresholdBlur.Name = "txtThresholdBlur";
            this.txtThresholdBlur.Size = new System.Drawing.Size(477, 28);
            this.txtThresholdBlur.TabIndex = 3;
            this.txtThresholdBlur.Text = "100";
            // 
            // txtThreshold
            // 
            this.txtThreshold.BackColor = System.Drawing.Color.White;
            this.txtThreshold.Location = new System.Drawing.Point(201, 113);
            this.txtThreshold.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.txtThreshold.Name = "txtThreshold";
            this.txtThreshold.Size = new System.Drawing.Size(477, 28);
            this.txtThreshold.TabIndex = 3;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(17, 311);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(97, 24);
            this.label7.TabIndex = 1;
            this.label7.Text = "Comment:";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(17, 244);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(80, 24);
            this.label11.TabIndex = 1;
            this.label11.Text = "Version:";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(17, 181);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(139, 24);
            this.label8.TabIndex = 1;
            this.label8.Text = "Threshold Blur:";
            // 
            // txtDeviceName
            // 
            this.txtDeviceName.BackColor = System.Drawing.Color.White;
            this.txtDeviceName.Location = new System.Drawing.Point(201, 44);
            this.txtDeviceName.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.txtDeviceName.Name = "txtDeviceName";
            this.txtDeviceName.Size = new System.Drawing.Size(477, 28);
            this.txtDeviceName.TabIndex = 2;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(17, 118);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(101, 24);
            this.label1.TabIndex = 1;
            this.label1.Text = "Threshold:";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(17, 49);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(129, 24);
            this.label5.TabIndex = 0;
            this.label5.Text = "Device Name:";
            // 
            // btnMergeData
            // 
            this.btnMergeData.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(129)))), ((int)(((byte)(213)))));
            this.btnMergeData.Font = new System.Drawing.Font("Microsoft Sans Serif", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnMergeData.ForeColor = System.Drawing.Color.White;
            this.btnMergeData.Location = new System.Drawing.Point(864, 169);
            this.btnMergeData.Name = "btnMergeData";
            this.btnMergeData.Size = new System.Drawing.Size(166, 41);
            this.btnMergeData.TabIndex = 81;
            this.btnMergeData.Text = "Merge Data";
            this.btnMergeData.UseVisualStyleBackColor = false;
            this.btnMergeData.Click += new System.EventHandler(this.btnMergeData_Click);
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Microsoft Sans Serif", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.label9.Location = new System.Drawing.Point(25, 69);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(780, 24);
            this.label9.TabIndex = 84;
            this.label9.Text = "* Please select folder which includes files format [DeviceID]_[Orig/Copy]_[Sequen" +
    "ce No.].csv";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Microsoft Sans Serif", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.Location = new System.Drawing.Point(24, 123);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(176, 24);
            this.label10.TabIndex = 79;
            this.label10.Text = "*Raw Result Folder:";
            // 
            // txtRawResult
            // 
            this.txtRawResult.BackColor = System.Drawing.Color.White;
            this.txtRawResult.Font = new System.Drawing.Font("Microsoft Sans Serif", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtRawResult.Location = new System.Drawing.Point(258, 120);
            this.txtRawResult.Multiline = true;
            this.txtRawResult.Name = "txtRawResult";
            this.txtRawResult.ReadOnly = true;
            this.txtRawResult.Size = new System.Drawing.Size(571, 32);
            this.txtRawResult.TabIndex = 80;
            // 
            // btnBrowseRaw
            // 
            this.btnBrowseRaw.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(129)))), ((int)(((byte)(213)))));
            this.btnBrowseRaw.FlatAppearance.BorderSize = 0;
            this.btnBrowseRaw.Font = new System.Drawing.Font("Microsoft Sans Serif", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnBrowseRaw.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.btnBrowseRaw.Location = new System.Drawing.Point(864, 115);
            this.btnBrowseRaw.Name = "btnBrowseRaw";
            this.btnBrowseRaw.Size = new System.Drawing.Size(161, 37);
            this.btnBrowseRaw.TabIndex = 82;
            this.btnBrowseRaw.Text = "Browse";
            this.btnBrowseRaw.UseVisualStyleBackColor = false;
            this.btnBrowseRaw.Click += new System.EventHandler(this.btnBrowseRaw_Click);
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("Microsoft Sans Serif", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.Location = new System.Drawing.Point(407, 505);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(129, 24);
            this.label12.TabIndex = 0;
            this.label12.Text = "Mean Orignal:";
            // 
            // txtMeanOrig
            // 
            this.txtMeanOrig.BackColor = System.Drawing.Color.White;
            this.txtMeanOrig.Font = new System.Drawing.Font("Microsoft Sans Serif", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtMeanOrig.Location = new System.Drawing.Point(598, 502);
            this.txtMeanOrig.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.txtMeanOrig.Name = "txtMeanOrig";
            this.txtMeanOrig.ReadOnly = true;
            this.txtMeanOrig.Size = new System.Drawing.Size(231, 28);
            this.txtMeanOrig.TabIndex = 2;
            // 
            // cbDeviceID
            // 
            this.cbDeviceID.AutoSize = true;
            this.cbDeviceID.Font = new System.Drawing.Font("Microsoft Sans Serif", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbDeviceID.Location = new System.Drawing.Point(258, 174);
            this.cbDeviceID.Name = "cbDeviceID";
            this.cbDeviceID.Size = new System.Drawing.Size(178, 28);
            this.cbDeviceID.TabIndex = 103;
            this.cbDeviceID.Text = "DeviceID Format?";
            this.cbDeviceID.UseVisualStyleBackColor = true;
            this.cbDeviceID.CheckedChanged += new System.EventHandler(this.cbDeviceID_CheckedChanged);
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Font = new System.Drawing.Font("Microsoft Sans Serif", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label14.Location = new System.Drawing.Point(493, 175);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(86, 24);
            this.label14.TabIndex = 79;
            this.label14.Text = "Location:";
            // 
            // txtLocation
            // 
            this.txtLocation.BackColor = System.Drawing.Color.White;
            this.txtLocation.Font = new System.Drawing.Font("Microsoft Sans Serif", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtLocation.Location = new System.Drawing.Point(623, 174);
            this.txtLocation.Multiline = true;
            this.txtLocation.Name = "txtLocation";
            this.txtLocation.Size = new System.Drawing.Size(206, 32);
            this.txtLocation.TabIndex = 80;
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Font = new System.Drawing.Font("Microsoft Sans Serif", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label13.Location = new System.Drawing.Point(407, 563);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(80, 24);
            this.label13.TabIndex = 0;
            this.label13.Text = "Std Dev:";
            // 
            // txtDev
            // 
            this.txtDev.BackColor = System.Drawing.Color.White;
            this.txtDev.Font = new System.Drawing.Font("Microsoft Sans Serif", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtDev.Location = new System.Drawing.Point(598, 560);
            this.txtDev.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.txtDev.Name = "txtDev";
            this.txtDev.ReadOnly = true;
            this.txtDev.Size = new System.Drawing.Size(231, 28);
            this.txtDev.TabIndex = 2;
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Font = new System.Drawing.Font("Microsoft Sans Serif", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label15.Location = new System.Drawing.Point(407, 621);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(112, 24);
            this.label15.TabIndex = 0;
            this.label15.Text = "Percentage:";
            // 
            // txtPercentage
            // 
            this.txtPercentage.BackColor = System.Drawing.Color.White;
            this.txtPercentage.Font = new System.Drawing.Font("Microsoft Sans Serif", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtPercentage.Location = new System.Drawing.Point(598, 618);
            this.txtPercentage.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.txtPercentage.Name = "txtPercentage";
            this.txtPercentage.Size = new System.Drawing.Size(231, 28);
            this.txtPercentage.TabIndex = 2;
            this.txtPercentage.Text = "300";
            this.txtPercentage.TextChanged += new System.EventHandler(this.txtPercentage_TextChanged);
            // 
            // lblPercentage
            // 
            this.lblPercentage.AutoSize = true;
            this.lblPercentage.Font = new System.Drawing.Font("Microsoft Sans Serif", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblPercentage.Location = new System.Drawing.Point(407, 683);
            this.lblPercentage.Name = "lblPercentage";
            this.lblPercentage.Size = new System.Drawing.Size(95, 24);
            this.lblPercentage.TabIndex = 0;
            this.lblPercentage.Text = "%Std Dev:";
            // 
            // txtPStd
            // 
            this.txtPStd.BackColor = System.Drawing.Color.White;
            this.txtPStd.Font = new System.Drawing.Font("Microsoft Sans Serif", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtPStd.Location = new System.Drawing.Point(598, 680);
            this.txtPStd.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.txtPStd.Name = "txtPStd";
            this.txtPStd.ReadOnly = true;
            this.txtPStd.Size = new System.Drawing.Size(231, 28);
            this.txtPStd.TabIndex = 2;
            // 
            // ParameterJSONForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(1620, 1061);
            this.Controls.Add(this.cbDeviceID);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.dgvDevice);
            this.Controls.Add(this.lbMessage);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.cbbProductName);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.btnClear);
            this.Controls.Add(this.btnBrowseJSON);
            this.Controls.Add(this.txtPStd);
            this.Controls.Add(this.txtPercentage);
            this.Controls.Add(this.txtDev);
            this.Controls.Add(this.lblPercentage);
            this.Controls.Add(this.txtMeanOrig);
            this.Controls.Add(this.label15);
            this.Controls.Add(this.btnBrowseRaw);
            this.Controls.Add(this.label13);
            this.Controls.Add(this.label12);
            this.Controls.Add(this.btnBrowseResult);
            this.Controls.Add(this.btnMergeData);
            this.Controls.Add(this.btnGetResult);
            this.Controls.Add(this.txtJSONPath);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.txtLocation);
            this.Controls.Add(this.label14);
            this.Controls.Add(this.txtRawResult);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.txtFolderPath);
            this.Controls.Add(this.lbPath);
            this.Controls.Add(this.menuStrip1);
            this.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Name = "ParameterJSONForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "U-nica Mini Tool v2.14";
            ((System.ComponentModel.ISupportInitialize)(this.dgvDevice)).EndInit();
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.DataGridView dgvDevice;
        private System.Windows.Forms.Label lbMessage;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.ComboBox cbbProductName;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Button btnClear;
        private System.Windows.Forms.Button btnBrowseResult;
        private System.Windows.Forms.Button btnGetResult;
        private System.Windows.Forms.TextBox txtFolderPath;
        private System.Windows.Forms.Label lbPath;
        private System.Windows.Forms.ToolStripMenuItem configurationToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem automationTestToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem evaluationToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem collectResultToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem otherZonesResultToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem evaluationHistoryToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem regressionTestToolStripMenuItem;
        private System.Windows.Forms.FolderBrowserDialog FolderDialog;
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem jSONToolStripMenuItem;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox txtJSONPath;
        private System.Windows.Forms.Button btnBrowseJSON;
        private System.Windows.Forms.OpenFileDialog openFileDialog1;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Button btnReset;
        private System.Windows.Forms.Button btnSet;
        private System.Windows.Forms.TextBox txtComment;
        private System.Windows.Forms.TextBox txtVersion;
        private System.Windows.Forms.TextBox txtThreshold;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.TextBox txtDeviceName;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox txtThresholdBlur;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Button btnExportJSON;
        private System.Windows.Forms.Button btnMergeData;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.TextBox txtRawResult;
        private System.Windows.Forms.Button btnBrowseRaw;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.TextBox txtMeanOrig;
        private System.Windows.Forms.CheckBox cbDeviceID;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.TextBox txtLocation;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.TextBox txtDev;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.TextBox txtPercentage;
        private System.Windows.Forms.Label lblPercentage;
        private System.Windows.Forms.TextBox txtPStd;
    }
}