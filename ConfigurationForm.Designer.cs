﻿namespace ReadingResult
{
    partial class ConfigurationForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.tabConfiguration = new System.Windows.Forms.TabControl();
            this.tabDevice = new System.Windows.Forms.TabPage();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.cbDisplay = new System.Windows.Forms.CheckBox();
            this.txtDeviceCodeHidden = new System.Windows.Forms.TextBox();
            this.btnRemoveDevice = new System.Windows.Forms.Button();
            this.btnUpdateDevice = new System.Windows.Forms.Button();
            this.btnResetDevice = new System.Windows.Forms.Button();
            this.btnAddDevice = new System.Windows.Forms.Button();
            this.txtUDID = new System.Windows.Forms.TextBox();
            this.label14 = new System.Windows.Forms.Label();
            this.txtLocation = new System.Windows.Forms.TextBox();
            this.label13 = new System.Windows.Forms.Label();
            this.txtType = new System.Windows.Forms.TextBox();
            this.label12 = new System.Windows.Forms.Label();
            this.txtDeviceEmail = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.txtDeviceCode = new System.Windows.Forms.TextBox();
            this.label11 = new System.Windows.Forms.Label();
            this.txtDeviceName = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.dgvDevices = new System.Windows.Forms.DataGridView();
            this.tabProduct = new System.Windows.Forms.TabPage();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.cbbType = new System.Windows.Forms.ComboBox();
            this.label15 = new System.Windows.Forms.Label();
            this.txtPath = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.txtProductIDHidden = new System.Windows.Forms.TextBox();
            this.btnSetupFile = new System.Windows.Forms.Button();
            this.btnAddCondition = new System.Windows.Forms.Button();
            this.label5 = new System.Windows.Forms.Label();
            this.txtProductCode = new System.Windows.Forms.TextBox();
            this.btnRemoveProduct = new System.Windows.Forms.Button();
            this.btnUpdateProduct = new System.Windows.Forms.Button();
            this.btnResetProduct = new System.Windows.Forms.Button();
            this.btnAddProduct = new System.Windows.Forms.Button();
            this.txtResult = new System.Windows.Forms.TextBox();
            this.txtProductName = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.dgvProduct = new System.Windows.Forms.DataGridView();
            this.tabVersion = new System.Windows.Forms.TabPage();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.txtOS = new System.Windows.Forms.TextBox();
            this.label10 = new System.Windows.Forms.Label();
            this.txtVersionIDHidden = new System.Windows.Forms.TextBox();
            this.btnRemoveVersion = new System.Windows.Forms.Button();
            this.btnUpdateVersion = new System.Windows.Forms.Button();
            this.btnResetVersion = new System.Windows.Forms.Button();
            this.btnAddVersion = new System.Windows.Forms.Button();
            this.txtVersion = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.dgvVersion = new System.Windows.Forms.DataGridView();
            this.label4 = new System.Windows.Forms.Label();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.regressionTestToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.evaluationHistoryToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.otherZonesResultToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.collectResultToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.evaluationToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.automationTestToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.configurationToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.jSONGeneratorToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.tabConfiguration.SuspendLayout();
            this.tabDevice.SuspendLayout();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvDevices)).BeginInit();
            this.tabProduct.SuspendLayout();
            this.groupBox3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvProduct)).BeginInit();
            this.tabVersion.SuspendLayout();
            this.groupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvVersion)).BeginInit();
            this.menuStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // tabConfiguration
            // 
            this.tabConfiguration.Controls.Add(this.tabDevice);
            this.tabConfiguration.Controls.Add(this.tabProduct);
            this.tabConfiguration.Controls.Add(this.tabVersion);
            this.tabConfiguration.Font = new System.Drawing.Font("Microsoft Sans Serif", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tabConfiguration.Location = new System.Drawing.Point(25, 129);
            this.tabConfiguration.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.tabConfiguration.Name = "tabConfiguration";
            this.tabConfiguration.SelectedIndex = 0;
            this.tabConfiguration.Size = new System.Drawing.Size(1795, 854);
            this.tabConfiguration.TabIndex = 0;
            // 
            // tabDevice
            // 
            this.tabDevice.Controls.Add(this.groupBox1);
            this.tabDevice.Controls.Add(this.dgvDevices);
            this.tabDevice.Location = new System.Drawing.Point(4, 38);
            this.tabDevice.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.tabDevice.Name = "tabDevice";
            this.tabDevice.Padding = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.tabDevice.Size = new System.Drawing.Size(1787, 812);
            this.tabDevice.TabIndex = 0;
            this.tabDevice.Text = "Devices";
            this.tabDevice.UseVisualStyleBackColor = true;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.cbDisplay);
            this.groupBox1.Controls.Add(this.txtDeviceCodeHidden);
            this.groupBox1.Controls.Add(this.btnRemoveDevice);
            this.groupBox1.Controls.Add(this.btnUpdateDevice);
            this.groupBox1.Controls.Add(this.btnResetDevice);
            this.groupBox1.Controls.Add(this.btnAddDevice);
            this.groupBox1.Controls.Add(this.txtUDID);
            this.groupBox1.Controls.Add(this.label14);
            this.groupBox1.Controls.Add(this.txtLocation);
            this.groupBox1.Controls.Add(this.label13);
            this.groupBox1.Controls.Add(this.txtType);
            this.groupBox1.Controls.Add(this.label12);
            this.groupBox1.Controls.Add(this.txtDeviceEmail);
            this.groupBox1.Controls.Add(this.label8);
            this.groupBox1.Controls.Add(this.txtDeviceCode);
            this.groupBox1.Controls.Add(this.label11);
            this.groupBox1.Controls.Add(this.txtDeviceName);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Location = new System.Drawing.Point(1099, 19);
            this.groupBox1.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Padding = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.groupBox1.Size = new System.Drawing.Size(668, 761);
            this.groupBox1.TabIndex = 1;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Device Info";
            // 
            // cbDisplay
            // 
            this.cbDisplay.AutoSize = true;
            this.cbDisplay.Location = new System.Drawing.Point(211, 472);
            this.cbDisplay.Name = "cbDisplay";
            this.cbDisplay.Size = new System.Drawing.Size(114, 33);
            this.cbDisplay.TabIndex = 9;
            this.cbDisplay.Text = "Display";
            this.cbDisplay.UseVisualStyleBackColor = true;
            // 
            // txtDeviceCodeHidden
            // 
            this.txtDeviceCodeHidden.Location = new System.Drawing.Point(211, 539);
            this.txtDeviceCodeHidden.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.txtDeviceCodeHidden.Name = "txtDeviceCodeHidden";
            this.txtDeviceCodeHidden.ReadOnly = true;
            this.txtDeviceCodeHidden.Size = new System.Drawing.Size(433, 34);
            this.txtDeviceCodeHidden.TabIndex = 8;
            this.txtDeviceCodeHidden.Visible = false;
            // 
            // btnRemoveDevice
            // 
            this.btnRemoveDevice.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(129)))), ((int)(((byte)(213)))));
            this.btnRemoveDevice.ForeColor = System.Drawing.Color.White;
            this.btnRemoveDevice.Location = new System.Drawing.Point(281, 658);
            this.btnRemoveDevice.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.btnRemoveDevice.Name = "btnRemoveDevice";
            this.btnRemoveDevice.Size = new System.Drawing.Size(173, 46);
            this.btnRemoveDevice.TabIndex = 7;
            this.btnRemoveDevice.Text = "Remove";
            this.btnRemoveDevice.UseVisualStyleBackColor = false;
            this.btnRemoveDevice.Click += new System.EventHandler(this.btnRemoveDevice_Click);
            // 
            // btnUpdateDevice
            // 
            this.btnUpdateDevice.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(129)))), ((int)(((byte)(213)))));
            this.btnUpdateDevice.ForeColor = System.Drawing.Color.White;
            this.btnUpdateDevice.Location = new System.Drawing.Point(471, 658);
            this.btnUpdateDevice.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.btnUpdateDevice.Name = "btnUpdateDevice";
            this.btnUpdateDevice.Size = new System.Drawing.Size(173, 46);
            this.btnUpdateDevice.TabIndex = 6;
            this.btnUpdateDevice.Text = "Update";
            this.btnUpdateDevice.UseVisualStyleBackColor = false;
            this.btnUpdateDevice.Click += new System.EventHandler(this.btnUpdateDevice_Click);
            // 
            // btnResetDevice
            // 
            this.btnResetDevice.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(129)))), ((int)(((byte)(213)))));
            this.btnResetDevice.ForeColor = System.Drawing.Color.White;
            this.btnResetDevice.Location = new System.Drawing.Point(281, 590);
            this.btnResetDevice.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.btnResetDevice.Name = "btnResetDevice";
            this.btnResetDevice.Size = new System.Drawing.Size(173, 46);
            this.btnResetDevice.TabIndex = 5;
            this.btnResetDevice.Text = "Reset";
            this.btnResetDevice.UseVisualStyleBackColor = false;
            this.btnResetDevice.Click += new System.EventHandler(this.btnReset_Click);
            // 
            // btnAddDevice
            // 
            this.btnAddDevice.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(129)))), ((int)(((byte)(213)))));
            this.btnAddDevice.ForeColor = System.Drawing.Color.White;
            this.btnAddDevice.Location = new System.Drawing.Point(471, 590);
            this.btnAddDevice.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.btnAddDevice.Name = "btnAddDevice";
            this.btnAddDevice.Size = new System.Drawing.Size(173, 46);
            this.btnAddDevice.TabIndex = 4;
            this.btnAddDevice.Text = "Add";
            this.btnAddDevice.UseVisualStyleBackColor = false;
            this.btnAddDevice.Click += new System.EventHandler(this.btnAddDevice_Click);
            // 
            // txtUDID
            // 
            this.txtUDID.BackColor = System.Drawing.Color.White;
            this.txtUDID.Location = new System.Drawing.Point(211, 407);
            this.txtUDID.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.txtUDID.Name = "txtUDID";
            this.txtUDID.Size = new System.Drawing.Size(433, 34);
            this.txtUDID.TabIndex = 3;
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(19, 476);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(0, 29);
            this.label14.TabIndex = 1;
            // 
            // txtLocation
            // 
            this.txtLocation.BackColor = System.Drawing.Color.White;
            this.txtLocation.Location = new System.Drawing.Point(211, 342);
            this.txtLocation.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.txtLocation.Name = "txtLocation";
            this.txtLocation.Size = new System.Drawing.Size(433, 34);
            this.txtLocation.TabIndex = 3;
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(19, 409);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(76, 29);
            this.label13.TabIndex = 1;
            this.label13.Text = "UDID:";
            // 
            // txtType
            // 
            this.txtType.BackColor = System.Drawing.Color.White;
            this.txtType.Location = new System.Drawing.Point(211, 275);
            this.txtType.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.txtType.Name = "txtType";
            this.txtType.Size = new System.Drawing.Size(433, 34);
            this.txtType.TabIndex = 3;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(19, 344);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(110, 29);
            this.label12.TabIndex = 1;
            this.label12.Text = "Location:";
            // 
            // txtDeviceEmail
            // 
            this.txtDeviceEmail.BackColor = System.Drawing.Color.White;
            this.txtDeviceEmail.Location = new System.Drawing.Point(211, 207);
            this.txtDeviceEmail.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.txtDeviceEmail.Name = "txtDeviceEmail";
            this.txtDeviceEmail.Size = new System.Drawing.Size(433, 34);
            this.txtDeviceEmail.TabIndex = 3;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(19, 277);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(108, 29);
            this.label8.TabIndex = 1;
            this.label8.Text = "Platform:";
            // 
            // txtDeviceCode
            // 
            this.txtDeviceCode.BackColor = System.Drawing.Color.White;
            this.txtDeviceCode.Location = new System.Drawing.Point(211, 140);
            this.txtDeviceCode.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.txtDeviceCode.Name = "txtDeviceCode";
            this.txtDeviceCode.Size = new System.Drawing.Size(433, 34);
            this.txtDeviceCode.TabIndex = 3;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(19, 209);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(150, 29);
            this.label11.TabIndex = 1;
            this.label11.Text = "Device User:";
            // 
            // txtDeviceName
            // 
            this.txtDeviceName.BackColor = System.Drawing.Color.White;
            this.txtDeviceName.Location = new System.Drawing.Point(211, 71);
            this.txtDeviceName.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.txtDeviceName.Name = "txtDeviceName";
            this.txtDeviceName.Size = new System.Drawing.Size(433, 34);
            this.txtDeviceName.TabIndex = 2;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(19, 143);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(122, 29);
            this.label2.TabIndex = 1;
            this.label2.Text = "Device ID:";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(19, 74);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(164, 29);
            this.label1.TabIndex = 0;
            this.label1.Text = "Device Name:";
            // 
            // dgvDevices
            // 
            this.dgvDevices.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvDevices.Location = new System.Drawing.Point(21, 34);
            this.dgvDevices.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.dgvDevices.MultiSelect = false;
            this.dgvDevices.Name = "dgvDevices";
            this.dgvDevices.ReadOnly = true;
            this.dgvDevices.RowHeadersWidth = 51;
            this.dgvDevices.RowTemplate.Height = 24;
            this.dgvDevices.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvDevices.Size = new System.Drawing.Size(1051, 746);
            this.dgvDevices.TabIndex = 0;
            this.dgvDevices.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvDevices_CellClick);
            // 
            // tabProduct
            // 
            this.tabProduct.Controls.Add(this.groupBox3);
            this.tabProduct.Controls.Add(this.dgvProduct);
            this.tabProduct.Location = new System.Drawing.Point(4, 38);
            this.tabProduct.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.tabProduct.Name = "tabProduct";
            this.tabProduct.Padding = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.tabProduct.Size = new System.Drawing.Size(1787, 812);
            this.tabProduct.TabIndex = 1;
            this.tabProduct.Text = "Products";
            this.tabProduct.UseVisualStyleBackColor = true;
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.cbbType);
            this.groupBox3.Controls.Add(this.label15);
            this.groupBox3.Controls.Add(this.txtPath);
            this.groupBox3.Controls.Add(this.label3);
            this.groupBox3.Controls.Add(this.txtProductIDHidden);
            this.groupBox3.Controls.Add(this.btnSetupFile);
            this.groupBox3.Controls.Add(this.btnAddCondition);
            this.groupBox3.Controls.Add(this.label5);
            this.groupBox3.Controls.Add(this.txtProductCode);
            this.groupBox3.Controls.Add(this.btnRemoveProduct);
            this.groupBox3.Controls.Add(this.btnUpdateProduct);
            this.groupBox3.Controls.Add(this.btnResetProduct);
            this.groupBox3.Controls.Add(this.btnAddProduct);
            this.groupBox3.Controls.Add(this.txtResult);
            this.groupBox3.Controls.Add(this.txtProductName);
            this.groupBox3.Controls.Add(this.label7);
            this.groupBox3.Controls.Add(this.label6);
            this.groupBox3.Location = new System.Drawing.Point(1095, 25);
            this.groupBox3.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Padding = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.groupBox3.Size = new System.Drawing.Size(661, 742);
            this.groupBox3.TabIndex = 3;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Product Info";
            // 
            // cbbType
            // 
            this.cbbType.FormattingEnabled = true;
            this.cbbType.Items.AddRange(new object[] {
            "Manual",
            "Video Guidance"});
            this.cbbType.Location = new System.Drawing.Point(197, 370);
            this.cbbType.Name = "cbbType";
            this.cbbType.Size = new System.Drawing.Size(432, 37);
            this.cbbType.TabIndex = 14;
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(6, 378);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(74, 29);
            this.label15.TabIndex = 12;
            this.label15.Text = "Type:";
            // 
            // txtPath
            // 
            this.txtPath.BackColor = System.Drawing.Color.White;
            this.txtPath.Location = new System.Drawing.Point(196, 303);
            this.txtPath.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.txtPath.Name = "txtPath";
            this.txtPath.Size = new System.Drawing.Size(433, 34);
            this.txtPath.TabIndex = 13;
            this.txtPath.Click += new System.EventHandler(this.txtPath_Click);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(4, 306);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(114, 29);
            this.label3.TabIndex = 12;
            this.label3.Text = "File Path:";
            // 
            // txtProductIDHidden
            // 
            this.txtProductIDHidden.Location = new System.Drawing.Point(197, 427);
            this.txtProductIDHidden.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.txtProductIDHidden.Name = "txtProductIDHidden";
            this.txtProductIDHidden.ReadOnly = true;
            this.txtProductIDHidden.Size = new System.Drawing.Size(433, 34);
            this.txtProductIDHidden.TabIndex = 11;
            this.txtProductIDHidden.Visible = false;
            // 
            // btnSetupFile
            // 
            this.btnSetupFile.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(129)))), ((int)(((byte)(213)))));
            this.btnSetupFile.ForeColor = System.Drawing.Color.White;
            this.btnSetupFile.Location = new System.Drawing.Point(197, 672);
            this.btnSetupFile.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.btnSetupFile.Name = "btnSetupFile";
            this.btnSetupFile.Size = new System.Drawing.Size(432, 46);
            this.btnSetupFile.TabIndex = 10;
            this.btnSetupFile.Text = "Setup File";
            this.btnSetupFile.UseVisualStyleBackColor = false;
            this.btnSetupFile.Click += new System.EventHandler(this.btnSetupFile_Click);
            // 
            // btnAddCondition
            // 
            this.btnAddCondition.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(129)))), ((int)(((byte)(213)))));
            this.btnAddCondition.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.btnAddCondition.Location = new System.Drawing.Point(197, 604);
            this.btnAddCondition.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.btnAddCondition.Name = "btnAddCondition";
            this.btnAddCondition.Size = new System.Drawing.Size(432, 46);
            this.btnAddCondition.TabIndex = 10;
            this.btnAddCondition.Text = "Setup Zone";
            this.btnAddCondition.UseVisualStyleBackColor = false;
            this.btnAddCondition.Click += new System.EventHandler(this.btnAddCondition_Click);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(4, 57);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(124, 29);
            this.label5.TabIndex = 9;
            this.label5.Text = "Project ID:";
            // 
            // txtProductCode
            // 
            this.txtProductCode.BackColor = System.Drawing.Color.White;
            this.txtProductCode.Location = new System.Drawing.Point(196, 50);
            this.txtProductCode.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.txtProductCode.Name = "txtProductCode";
            this.txtProductCode.Size = new System.Drawing.Size(433, 34);
            this.txtProductCode.TabIndex = 8;
            // 
            // btnRemoveProduct
            // 
            this.btnRemoveProduct.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(129)))), ((int)(((byte)(213)))));
            this.btnRemoveProduct.ForeColor = System.Drawing.Color.White;
            this.btnRemoveProduct.Location = new System.Drawing.Point(197, 531);
            this.btnRemoveProduct.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.btnRemoveProduct.Name = "btnRemoveProduct";
            this.btnRemoveProduct.Size = new System.Drawing.Size(215, 46);
            this.btnRemoveProduct.TabIndex = 7;
            this.btnRemoveProduct.Text = "Remove";
            this.btnRemoveProduct.UseVisualStyleBackColor = false;
            this.btnRemoveProduct.Click += new System.EventHandler(this.btnRemoveProduct_Click);
            // 
            // btnUpdateProduct
            // 
            this.btnUpdateProduct.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(129)))), ((int)(((byte)(213)))));
            this.btnUpdateProduct.ForeColor = System.Drawing.Color.White;
            this.btnUpdateProduct.Location = new System.Drawing.Point(418, 531);
            this.btnUpdateProduct.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.btnUpdateProduct.Name = "btnUpdateProduct";
            this.btnUpdateProduct.Size = new System.Drawing.Size(211, 46);
            this.btnUpdateProduct.TabIndex = 6;
            this.btnUpdateProduct.Text = "Update";
            this.btnUpdateProduct.UseVisualStyleBackColor = false;
            this.btnUpdateProduct.Click += new System.EventHandler(this.BtnUpdateProduct_Click);
            // 
            // btnResetProduct
            // 
            this.btnResetProduct.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(129)))), ((int)(((byte)(213)))));
            this.btnResetProduct.ForeColor = System.Drawing.Color.White;
            this.btnResetProduct.Location = new System.Drawing.Point(197, 465);
            this.btnResetProduct.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.btnResetProduct.Name = "btnResetProduct";
            this.btnResetProduct.Size = new System.Drawing.Size(215, 46);
            this.btnResetProduct.TabIndex = 5;
            this.btnResetProduct.Text = "Reset";
            this.btnResetProduct.UseVisualStyleBackColor = false;
            this.btnResetProduct.Click += new System.EventHandler(this.btnResetProduct_Click);
            // 
            // btnAddProduct
            // 
            this.btnAddProduct.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(129)))), ((int)(((byte)(213)))));
            this.btnAddProduct.ForeColor = System.Drawing.Color.White;
            this.btnAddProduct.Location = new System.Drawing.Point(418, 465);
            this.btnAddProduct.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.btnAddProduct.Name = "btnAddProduct";
            this.btnAddProduct.Size = new System.Drawing.Size(211, 46);
            this.btnAddProduct.TabIndex = 4;
            this.btnAddProduct.Text = "Add";
            this.btnAddProduct.UseVisualStyleBackColor = false;
            this.btnAddProduct.Click += new System.EventHandler(this.btnAddProduct_Click);
            // 
            // txtResult
            // 
            this.txtResult.BackColor = System.Drawing.Color.White;
            this.txtResult.Location = new System.Drawing.Point(196, 188);
            this.txtResult.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.txtResult.Multiline = true;
            this.txtResult.Name = "txtResult";
            this.txtResult.Size = new System.Drawing.Size(433, 88);
            this.txtResult.TabIndex = 2;
            // 
            // txtProductName
            // 
            this.txtProductName.BackColor = System.Drawing.Color.White;
            this.txtProductName.Location = new System.Drawing.Point(196, 118);
            this.txtProductName.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.txtProductName.Name = "txtProductName";
            this.txtProductName.Size = new System.Drawing.Size(433, 34);
            this.txtProductName.TabIndex = 2;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(5, 193);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(87, 29);
            this.label7.TabIndex = 0;
            this.label7.Text = "Result:";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(4, 121);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(173, 29);
            this.label6.TabIndex = 0;
            this.label6.Text = "Product Name:";
            // 
            // dgvProduct
            // 
            this.dgvProduct.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvProduct.Location = new System.Drawing.Point(21, 38);
            this.dgvProduct.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.dgvProduct.MultiSelect = false;
            this.dgvProduct.Name = "dgvProduct";
            this.dgvProduct.ReadOnly = true;
            this.dgvProduct.RowHeadersWidth = 51;
            this.dgvProduct.RowTemplate.Height = 24;
            this.dgvProduct.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvProduct.Size = new System.Drawing.Size(1051, 729);
            this.dgvProduct.TabIndex = 2;
            this.dgvProduct.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvProduct_CellClick);
            // 
            // tabVersion
            // 
            this.tabVersion.Controls.Add(this.groupBox2);
            this.tabVersion.Controls.Add(this.dgvVersion);
            this.tabVersion.Location = new System.Drawing.Point(4, 38);
            this.tabVersion.Margin = new System.Windows.Forms.Padding(4);
            this.tabVersion.Name = "tabVersion";
            this.tabVersion.Padding = new System.Windows.Forms.Padding(4);
            this.tabVersion.Size = new System.Drawing.Size(1787, 812);
            this.tabVersion.TabIndex = 2;
            this.tabVersion.Text = "Versions";
            this.tabVersion.UseVisualStyleBackColor = true;
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.txtOS);
            this.groupBox2.Controls.Add(this.label10);
            this.groupBox2.Controls.Add(this.txtVersionIDHidden);
            this.groupBox2.Controls.Add(this.btnRemoveVersion);
            this.groupBox2.Controls.Add(this.btnUpdateVersion);
            this.groupBox2.Controls.Add(this.btnResetVersion);
            this.groupBox2.Controls.Add(this.btnAddVersion);
            this.groupBox2.Controls.Add(this.txtVersion);
            this.groupBox2.Controls.Add(this.label9);
            this.groupBox2.Location = new System.Drawing.Point(1064, 25);
            this.groupBox2.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Padding = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.groupBox2.Size = new System.Drawing.Size(709, 761);
            this.groupBox2.TabIndex = 3;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Version Info";
            // 
            // txtOS
            // 
            this.txtOS.BackColor = System.Drawing.Color.White;
            this.txtOS.Location = new System.Drawing.Point(211, 135);
            this.txtOS.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.txtOS.Name = "txtOS";
            this.txtOS.Size = new System.Drawing.Size(433, 34);
            this.txtOS.TabIndex = 10;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(19, 138);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(54, 29);
            this.label10.TabIndex = 9;
            this.label10.Text = "OS:";
            // 
            // txtVersionIDHidden
            // 
            this.txtVersionIDHidden.Location = new System.Drawing.Point(211, 196);
            this.txtVersionIDHidden.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.txtVersionIDHidden.Name = "txtVersionIDHidden";
            this.txtVersionIDHidden.ReadOnly = true;
            this.txtVersionIDHidden.Size = new System.Drawing.Size(433, 34);
            this.txtVersionIDHidden.TabIndex = 8;
            this.txtVersionIDHidden.Visible = false;
            // 
            // btnRemoveVersion
            // 
            this.btnRemoveVersion.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(129)))), ((int)(((byte)(213)))));
            this.btnRemoveVersion.ForeColor = System.Drawing.Color.White;
            this.btnRemoveVersion.Location = new System.Drawing.Point(281, 315);
            this.btnRemoveVersion.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.btnRemoveVersion.Name = "btnRemoveVersion";
            this.btnRemoveVersion.Size = new System.Drawing.Size(173, 46);
            this.btnRemoveVersion.TabIndex = 7;
            this.btnRemoveVersion.Text = "Remove";
            this.btnRemoveVersion.UseVisualStyleBackColor = false;
            this.btnRemoveVersion.Click += new System.EventHandler(this.btnRemoveVersion_Click);
            // 
            // btnUpdateVersion
            // 
            this.btnUpdateVersion.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(129)))), ((int)(((byte)(213)))));
            this.btnUpdateVersion.ForeColor = System.Drawing.Color.White;
            this.btnUpdateVersion.Location = new System.Drawing.Point(471, 315);
            this.btnUpdateVersion.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.btnUpdateVersion.Name = "btnUpdateVersion";
            this.btnUpdateVersion.Size = new System.Drawing.Size(173, 46);
            this.btnUpdateVersion.TabIndex = 6;
            this.btnUpdateVersion.Text = "Update";
            this.btnUpdateVersion.UseVisualStyleBackColor = false;
            this.btnUpdateVersion.Click += new System.EventHandler(this.btnUpdateVersion_Click);
            // 
            // btnResetVersion
            // 
            this.btnResetVersion.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(129)))), ((int)(((byte)(213)))));
            this.btnResetVersion.ForeColor = System.Drawing.Color.White;
            this.btnResetVersion.Location = new System.Drawing.Point(281, 247);
            this.btnResetVersion.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.btnResetVersion.Name = "btnResetVersion";
            this.btnResetVersion.Size = new System.Drawing.Size(173, 46);
            this.btnResetVersion.TabIndex = 5;
            this.btnResetVersion.Text = "Reset";
            this.btnResetVersion.UseVisualStyleBackColor = false;
            this.btnResetVersion.Click += new System.EventHandler(this.btnResetVersion_Click);
            // 
            // btnAddVersion
            // 
            this.btnAddVersion.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(129)))), ((int)(((byte)(213)))));
            this.btnAddVersion.ForeColor = System.Drawing.Color.White;
            this.btnAddVersion.Location = new System.Drawing.Point(471, 247);
            this.btnAddVersion.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.btnAddVersion.Name = "btnAddVersion";
            this.btnAddVersion.Size = new System.Drawing.Size(173, 46);
            this.btnAddVersion.TabIndex = 4;
            this.btnAddVersion.Text = "Add";
            this.btnAddVersion.UseVisualStyleBackColor = false;
            this.btnAddVersion.Click += new System.EventHandler(this.btnAddVersion_Click);
            // 
            // txtVersion
            // 
            this.txtVersion.BackColor = System.Drawing.Color.White;
            this.txtVersion.Location = new System.Drawing.Point(211, 71);
            this.txtVersion.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.txtVersion.Name = "txtVersion";
            this.txtVersion.Size = new System.Drawing.Size(433, 34);
            this.txtVersion.TabIndex = 2;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(19, 74);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(150, 29);
            this.label9.TabIndex = 0;
            this.label9.Text = "App Version:";
            // 
            // dgvVersion
            // 
            this.dgvVersion.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvVersion.Location = new System.Drawing.Point(23, 39);
            this.dgvVersion.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.dgvVersion.MultiSelect = false;
            this.dgvVersion.Name = "dgvVersion";
            this.dgvVersion.ReadOnly = true;
            this.dgvVersion.RowHeadersWidth = 51;
            this.dgvVersion.RowTemplate.Height = 24;
            this.dgvVersion.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvVersion.Size = new System.Drawing.Size(1012, 746);
            this.dgvVersion.TabIndex = 2;
            this.dgvVersion.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvVersion_CellContentClick);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(29, 63);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(194, 36);
            this.label4.TabIndex = 26;
            this.label4.Text = "Configuration";
            // 
            // menuStrip1
            // 
            this.menuStrip1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(12)))), ((int)(((byte)(123)))), ((int)(((byte)(177)))));
            this.menuStrip1.Font = new System.Drawing.Font("Arial", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.menuStrip1.ImageScalingSize = new System.Drawing.Size(20, 20);
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.regressionTestToolStripMenuItem,
            this.evaluationHistoryToolStripMenuItem,
            this.otherZonesResultToolStripMenuItem,
            this.collectResultToolStripMenuItem,
            this.evaluationToolStripMenuItem,
            this.automationTestToolStripMenuItem,
            this.configurationToolStripMenuItem,
            this.jSONGeneratorToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Padding = new System.Windows.Forms.Padding(5, 6, 0, 6);
            this.menuStrip1.Size = new System.Drawing.Size(1872, 43);
            this.menuStrip1.TabIndex = 27;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // regressionTestToolStripMenuItem
            // 
            this.regressionTestToolStripMenuItem.ForeColor = System.Drawing.Color.White;
            this.regressionTestToolStripMenuItem.Name = "regressionTestToolStripMenuItem";
            this.regressionTestToolStripMenuItem.Size = new System.Drawing.Size(138, 31);
            this.regressionTestToolStripMenuItem.Text = "Evaluation";
            this.regressionTestToolStripMenuItem.Click += new System.EventHandler(this.regressionTestToolStripMenuItem_Click_1);
            // 
            // evaluationHistoryToolStripMenuItem
            // 
            this.evaluationHistoryToolStripMenuItem.ForeColor = System.Drawing.Color.White;
            this.evaluationHistoryToolStripMenuItem.Name = "evaluationHistoryToolStripMenuItem";
            this.evaluationHistoryToolStripMenuItem.Size = new System.Drawing.Size(219, 31);
            this.evaluationHistoryToolStripMenuItem.Text = "Evaluation History";
            this.evaluationHistoryToolStripMenuItem.Click += new System.EventHandler(this.evaluationHistoryToolStripMenuItem_Click);
            // 
            // otherZonesResultToolStripMenuItem
            // 
            this.otherZonesResultToolStripMenuItem.ForeColor = System.Drawing.Color.White;
            this.otherZonesResultToolStripMenuItem.Name = "otherZonesResultToolStripMenuItem";
            this.otherZonesResultToolStripMenuItem.Size = new System.Drawing.Size(244, 31);
            this.otherZonesResultToolStripMenuItem.Text = "Non-STC Evaluation";
            this.otherZonesResultToolStripMenuItem.Click += new System.EventHandler(this.otherZonesResultToolStripMenuItem_Click);
            // 
            // collectResultToolStripMenuItem
            // 
            this.collectResultToolStripMenuItem.ForeColor = System.Drawing.Color.White;
            this.collectResultToolStripMenuItem.Name = "collectResultToolStripMenuItem";
            this.collectResultToolStripMenuItem.Size = new System.Drawing.Size(176, 31);
            this.collectResultToolStripMenuItem.Text = "Collect Result";
            this.collectResultToolStripMenuItem.Click += new System.EventHandler(this.collectResultToolStripMenuItem_Click);
            // 
            // evaluationToolStripMenuItem
            // 
            this.evaluationToolStripMenuItem.Font = new System.Drawing.Font("Arial", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.evaluationToolStripMenuItem.ForeColor = System.Drawing.Color.White;
            this.evaluationToolStripMenuItem.Name = "evaluationToolStripMenuItem";
            this.evaluationToolStripMenuItem.Size = new System.Drawing.Size(155, 31);
            this.evaluationToolStripMenuItem.Text = "Comparison";
            this.evaluationToolStripMenuItem.Click += new System.EventHandler(this.evaluationToolStripMenuItem_Click_1);
            // 
            // automationTestToolStripMenuItem
            // 
            this.automationTestToolStripMenuItem.ForeColor = System.Drawing.Color.White;
            this.automationTestToolStripMenuItem.Name = "automationTestToolStripMenuItem";
            this.automationTestToolStripMenuItem.Size = new System.Drawing.Size(197, 31);
            this.automationTestToolStripMenuItem.Text = "Automation Test";
            this.automationTestToolStripMenuItem.Click += new System.EventHandler(this.automationTestToolStripMenuItem_Click);
            // 
            // configurationToolStripMenuItem
            // 
            this.configurationToolStripMenuItem.Font = new System.Drawing.Font("Arial", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.configurationToolStripMenuItem.ForeColor = System.Drawing.Color.White;
            this.configurationToolStripMenuItem.Name = "configurationToolStripMenuItem";
            this.configurationToolStripMenuItem.Size = new System.Drawing.Size(170, 31);
            this.configurationToolStripMenuItem.Text = "Configuration";
            this.configurationToolStripMenuItem.Click += new System.EventHandler(this.configurationToolStripMenuItem_Click_1);
            // 
            // jSONGeneratorToolStripMenuItem
            // 
            this.jSONGeneratorToolStripMenuItem.ForeColor = System.Drawing.Color.White;
            this.jSONGeneratorToolStripMenuItem.Name = "jSONGeneratorToolStripMenuItem";
            this.jSONGeneratorToolStripMenuItem.Size = new System.Drawing.Size(205, 31);
            this.jSONGeneratorToolStripMenuItem.Text = "JSON Generator";
            this.jSONGeneratorToolStripMenuItem.Click += new System.EventHandler(this.jSONGeneratorToolStripMenuItem_Click);
            // 
            // ConfigurationForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(1872, 998);
            this.Controls.Add(this.menuStrip1);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.tabConfiguration);
            this.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.Name = "ConfigurationForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "U-nica Mini Tool v2.14";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.ConfigurationForm_FormClosed);
            this.tabConfiguration.ResumeLayout(false);
            this.tabDevice.ResumeLayout(false);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvDevices)).EndInit();
            this.tabProduct.ResumeLayout(false);
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvProduct)).EndInit();
            this.tabVersion.ResumeLayout(false);
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvVersion)).EndInit();
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TabControl tabConfiguration;
        private System.Windows.Forms.TabPage tabDevice;
        private System.Windows.Forms.TabPage tabProduct;
        private System.Windows.Forms.DataGridView dgvDevices;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.TextBox txtDeviceCode;
        private System.Windows.Forms.TextBox txtDeviceName;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button btnRemoveDevice;
        private System.Windows.Forms.Button btnUpdateDevice;
        private System.Windows.Forms.Button btnResetDevice;
        private System.Windows.Forms.Button btnAddDevice;
        private System.Windows.Forms.TextBox txtDeviceCodeHidden;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.TextBox txtProductCode;
        private System.Windows.Forms.Button btnRemoveProduct;
        private System.Windows.Forms.Button btnUpdateProduct;
        private System.Windows.Forms.Button btnResetProduct;
        private System.Windows.Forms.Button btnAddProduct;
        private System.Windows.Forms.TextBox txtProductName;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.DataGridView dgvProduct;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Button btnAddCondition;
        private System.Windows.Forms.TextBox txtProductIDHidden;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox txtResult;
        private System.Windows.Forms.TextBox txtPath;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Button btnSetupFile;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TabPage tabVersion;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.TextBox txtOS;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.TextBox txtVersionIDHidden;
        private System.Windows.Forms.Button btnRemoveVersion;
        private System.Windows.Forms.Button btnUpdateVersion;
        private System.Windows.Forms.Button btnResetVersion;
        private System.Windows.Forms.Button btnAddVersion;
        private System.Windows.Forms.TextBox txtVersion;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.DataGridView dgvVersion;
        private System.Windows.Forms.TextBox txtDeviceEmail;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem regressionTestToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem evaluationToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem configurationToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem otherZonesResultToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem evaluationHistoryToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem collectResultToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem automationTestToolStripMenuItem;
        private System.Windows.Forms.TextBox txtUDID;
        private System.Windows.Forms.TextBox txtLocation;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.TextBox txtType;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.CheckBox cbDisplay;
        private System.Windows.Forms.ComboBox cbbType;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.ToolStripMenuItem jSONGeneratorToolStripMenuItem;
    }
}