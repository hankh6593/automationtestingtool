﻿namespace ReadingResult
{
    partial class SetupConditionForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.txtProductID = new System.Windows.Forms.TextBox();
            this.txtProductName = new System.Windows.Forms.TextBox();
            this.btnResetZone5 = new System.Windows.Forms.Button();
            this.btnResetZone4 = new System.Windows.Forms.Button();
            this.btnResetZone3 = new System.Windows.Forms.Button();
            this.btnResetZone2 = new System.Windows.Forms.Button();
            this.btnUpdate = new System.Windows.Forms.Button();
            this.btnResetZone1 = new System.Windows.Forms.Button();
            this.groupBox6 = new System.Windows.Forms.GroupBox();
            this.txtThreshold5 = new System.Windows.Forms.TextBox();
            this.label21 = new System.Windows.Forms.Label();
            this.cbbMethod5 = new System.Windows.Forms.ComboBox();
            this.label16 = new System.Windows.Forms.Label();
            this.txtCode5 = new System.Windows.Forms.TextBox();
            this.label10 = new System.Windows.Forms.Label();
            this.txtZoneResult5 = new System.Windows.Forms.TextBox();
            this.label11 = new System.Windows.Forms.Label();
            this.cbActivate5 = new System.Windows.Forms.CheckBox();
            this.groupBox5 = new System.Windows.Forms.GroupBox();
            this.txtThreshold4 = new System.Windows.Forms.TextBox();
            this.label20 = new System.Windows.Forms.Label();
            this.cbbMethod4 = new System.Windows.Forms.ComboBox();
            this.label15 = new System.Windows.Forms.Label();
            this.txtCode4 = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.txtZoneResult4 = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.cbActivate4 = new System.Windows.Forms.CheckBox();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.txtThreshold3 = new System.Windows.Forms.TextBox();
            this.label19 = new System.Windows.Forms.Label();
            this.cbbMethod3 = new System.Windows.Forms.ComboBox();
            this.label14 = new System.Windows.Forms.Label();
            this.txtCode3 = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.txtZoneResult3 = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.cbActivate3 = new System.Windows.Forms.CheckBox();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.txtThreshold2 = new System.Windows.Forms.TextBox();
            this.label18 = new System.Windows.Forms.Label();
            this.cbbMethod2 = new System.Windows.Forms.ComboBox();
            this.label13 = new System.Windows.Forms.Label();
            this.txtCode2 = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.txtZoneResult2 = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.cbActivate2 = new System.Windows.Forms.CheckBox();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.txtThreshold1 = new System.Windows.Forms.TextBox();
            this.label17 = new System.Windows.Forms.Label();
            this.cbbMethod1 = new System.Windows.Forms.ComboBox();
            this.label12 = new System.Windows.Forms.Label();
            this.txtCode1 = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.txtZoneResult1 = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.cbActivate1 = new System.Windows.Forms.CheckBox();
            this.label1 = new System.Windows.Forms.Label();
            this.groupBox1.SuspendLayout();
            this.groupBox6.SuspendLayout();
            this.groupBox5.SuspendLayout();
            this.groupBox4.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.txtProductID);
            this.groupBox1.Controls.Add(this.txtProductName);
            this.groupBox1.Controls.Add(this.btnResetZone5);
            this.groupBox1.Controls.Add(this.btnResetZone4);
            this.groupBox1.Controls.Add(this.btnResetZone3);
            this.groupBox1.Controls.Add(this.btnResetZone2);
            this.groupBox1.Controls.Add(this.btnUpdate);
            this.groupBox1.Controls.Add(this.btnResetZone1);
            this.groupBox1.Controls.Add(this.groupBox6);
            this.groupBox1.Controls.Add(this.groupBox5);
            this.groupBox1.Controls.Add(this.groupBox4);
            this.groupBox1.Controls.Add(this.groupBox3);
            this.groupBox1.Controls.Add(this.groupBox2);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Font = new System.Drawing.Font("Microsoft Sans Serif", 20.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox1.Location = new System.Drawing.Point(16, 28);
            this.groupBox1.Margin = new System.Windows.Forms.Padding(4);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Padding = new System.Windows.Forms.Padding(4);
            this.groupBox1.Size = new System.Drawing.Size(1721, 708);
            this.groupBox1.TabIndex = 2;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Setup Condition";
            // 
            // txtProductID
            // 
            this.txtProductID.Location = new System.Drawing.Point(1052, 62);
            this.txtProductID.Margin = new System.Windows.Forms.Padding(4);
            this.txtProductID.Name = "txtProductID";
            this.txtProductID.Size = new System.Drawing.Size(56, 46);
            this.txtProductID.TabIndex = 24;
            this.txtProductID.Visible = false;
            // 
            // txtProductName
            // 
            this.txtProductName.BackColor = System.Drawing.Color.White;
            this.txtProductName.Enabled = false;
            this.txtProductName.Location = new System.Drawing.Point(231, 62);
            this.txtProductName.Margin = new System.Windows.Forms.Padding(4);
            this.txtProductName.Name = "txtProductName";
            this.txtProductName.Size = new System.Drawing.Size(793, 46);
            this.txtProductName.TabIndex = 23;
            // 
            // btnResetZone5
            // 
            this.btnResetZone5.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(129)))), ((int)(((byte)(213)))));
            this.btnResetZone5.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnResetZone5.ForeColor = System.Drawing.Color.White;
            this.btnResetZone5.Location = new System.Drawing.Point(1387, 545);
            this.btnResetZone5.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.btnResetZone5.Name = "btnResetZone5";
            this.btnResetZone5.Size = new System.Drawing.Size(315, 59);
            this.btnResetZone5.TabIndex = 22;
            this.btnResetZone5.Text = "Reset";
            this.btnResetZone5.UseVisualStyleBackColor = false;
            this.btnResetZone5.Click += new System.EventHandler(this.btnResetZone5_Click);
            // 
            // btnResetZone4
            // 
            this.btnResetZone4.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(129)))), ((int)(((byte)(213)))));
            this.btnResetZone4.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnResetZone4.ForeColor = System.Drawing.Color.White;
            this.btnResetZone4.Location = new System.Drawing.Point(1052, 545);
            this.btnResetZone4.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.btnResetZone4.Name = "btnResetZone4";
            this.btnResetZone4.Size = new System.Drawing.Size(315, 59);
            this.btnResetZone4.TabIndex = 21;
            this.btnResetZone4.Text = "Reset";
            this.btnResetZone4.UseVisualStyleBackColor = false;
            this.btnResetZone4.Click += new System.EventHandler(this.btnResetZone4_Click);
            // 
            // btnResetZone3
            // 
            this.btnResetZone3.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(129)))), ((int)(((byte)(213)))));
            this.btnResetZone3.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnResetZone3.ForeColor = System.Drawing.Color.White;
            this.btnResetZone3.Location = new System.Drawing.Point(711, 545);
            this.btnResetZone3.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.btnResetZone3.Name = "btnResetZone3";
            this.btnResetZone3.Size = new System.Drawing.Size(315, 59);
            this.btnResetZone3.TabIndex = 20;
            this.btnResetZone3.Text = "Reset";
            this.btnResetZone3.UseVisualStyleBackColor = false;
            this.btnResetZone3.Click += new System.EventHandler(this.btnResetZone3_Click);
            // 
            // btnResetZone2
            // 
            this.btnResetZone2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(129)))), ((int)(((byte)(213)))));
            this.btnResetZone2.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnResetZone2.ForeColor = System.Drawing.Color.White;
            this.btnResetZone2.Location = new System.Drawing.Point(369, 545);
            this.btnResetZone2.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.btnResetZone2.Name = "btnResetZone2";
            this.btnResetZone2.Size = new System.Drawing.Size(315, 59);
            this.btnResetZone2.TabIndex = 19;
            this.btnResetZone2.Text = "Reset";
            this.btnResetZone2.UseVisualStyleBackColor = false;
            this.btnResetZone2.Click += new System.EventHandler(this.btnResetZone2_Click);
            // 
            // btnUpdate
            // 
            this.btnUpdate.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(129)))), ((int)(((byte)(213)))));
            this.btnUpdate.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnUpdate.ForeColor = System.Drawing.Color.White;
            this.btnUpdate.Location = new System.Drawing.Point(28, 633);
            this.btnUpdate.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.btnUpdate.Name = "btnUpdate";
            this.btnUpdate.Size = new System.Drawing.Size(1673, 53);
            this.btnUpdate.TabIndex = 18;
            this.btnUpdate.Text = "Update";
            this.btnUpdate.UseVisualStyleBackColor = false;
            this.btnUpdate.Click += new System.EventHandler(this.btnUpdate_Click);
            // 
            // btnResetZone1
            // 
            this.btnResetZone1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(129)))), ((int)(((byte)(213)))));
            this.btnResetZone1.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnResetZone1.ForeColor = System.Drawing.Color.White;
            this.btnResetZone1.Location = new System.Drawing.Point(28, 545);
            this.btnResetZone1.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.btnResetZone1.Name = "btnResetZone1";
            this.btnResetZone1.Size = new System.Drawing.Size(315, 59);
            this.btnResetZone1.TabIndex = 17;
            this.btnResetZone1.Text = "Reset";
            this.btnResetZone1.UseVisualStyleBackColor = false;
            this.btnResetZone1.Click += new System.EventHandler(this.btnResetZone1_Click);
            // 
            // groupBox6
            // 
            this.groupBox6.Controls.Add(this.txtThreshold5);
            this.groupBox6.Controls.Add(this.label21);
            this.groupBox6.Controls.Add(this.cbbMethod5);
            this.groupBox6.Controls.Add(this.label16);
            this.groupBox6.Controls.Add(this.txtCode5);
            this.groupBox6.Controls.Add(this.label10);
            this.groupBox6.Controls.Add(this.txtZoneResult5);
            this.groupBox6.Controls.Add(this.label11);
            this.groupBox6.Controls.Add(this.cbActivate5);
            this.groupBox6.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox6.Location = new System.Drawing.Point(1387, 142);
            this.groupBox6.Margin = new System.Windows.Forms.Padding(4);
            this.groupBox6.Name = "groupBox6";
            this.groupBox6.Padding = new System.Windows.Forms.Padding(4);
            this.groupBox6.Size = new System.Drawing.Size(315, 380);
            this.groupBox6.TabIndex = 15;
            this.groupBox6.TabStop = false;
            this.groupBox6.Text = "Zone 5";
            // 
            // txtThreshold5
            // 
            this.txtThreshold5.BackColor = System.Drawing.Color.White;
            this.txtThreshold5.Location = new System.Drawing.Point(173, 325);
            this.txtThreshold5.Margin = new System.Windows.Forms.Padding(4);
            this.txtThreshold5.Name = "txtThreshold5";
            this.txtThreshold5.Size = new System.Drawing.Size(120, 34);
            this.txtThreshold5.TabIndex = 23;
            this.txtThreshold5.TextChanged += new System.EventHandler(this.TxtThreshold5_TextChanged);
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Location = new System.Drawing.Point(9, 329);
            this.label21.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(129, 29);
            this.label21.TabIndex = 22;
            this.label21.Text = "Threshold:";
            // 
            // cbbMethod5
            // 
            this.cbbMethod5.BackColor = System.Drawing.Color.White;
            this.cbbMethod5.FormattingEnabled = true;
            this.cbbMethod5.Items.AddRange(new object[] {
            "STC",
            "CD1",
            "CDS",
            "CD3",
            "CD2"});
            this.cbbMethod5.Location = new System.Drawing.Point(19, 143);
            this.cbbMethod5.Margin = new System.Windows.Forms.Padding(4);
            this.cbbMethod5.Name = "cbbMethod5";
            this.cbbMethod5.Size = new System.Drawing.Size(279, 37);
            this.cbbMethod5.TabIndex = 19;
            this.cbbMethod5.SelectedIndexChanged += new System.EventHandler(this.CbbMethod5_SelectedIndexChanged);
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Location = new System.Drawing.Point(13, 96);
            this.label16.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(100, 29);
            this.label16.TabIndex = 18;
            this.label16.Text = "Method:";
            // 
            // txtCode5
            // 
            this.txtCode5.BackColor = System.Drawing.Color.White;
            this.txtCode5.Location = new System.Drawing.Point(99, 267);
            this.txtCode5.Margin = new System.Windows.Forms.Padding(4);
            this.txtCode5.Name = "txtCode5";
            this.txtCode5.Size = new System.Drawing.Size(195, 34);
            this.txtCode5.TabIndex = 11;
            this.txtCode5.TextChanged += new System.EventHandler(this.TxtCode5_TextChanged);
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(9, 267);
            this.label10.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(64, 29);
            this.label10.TabIndex = 10;
            this.label10.Text = "Mov:";
            // 
            // txtZoneResult5
            // 
            this.txtZoneResult5.BackColor = System.Drawing.Color.White;
            this.txtZoneResult5.Location = new System.Drawing.Point(173, 207);
            this.txtZoneResult5.Margin = new System.Windows.Forms.Padding(4);
            this.txtZoneResult5.Name = "txtZoneResult5";
            this.txtZoneResult5.Size = new System.Drawing.Size(120, 34);
            this.txtZoneResult5.TabIndex = 9;
            this.txtZoneResult5.TextChanged += new System.EventHandler(this.TxtZoneResult5_TextChanged);
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(9, 210);
            this.label11.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(78, 29);
            this.label11.TabIndex = 8;
            this.label11.Text = "Code:";
            // 
            // cbActivate5
            // 
            this.cbActivate5.AutoSize = true;
            this.cbActivate5.BackColor = System.Drawing.Color.White;
            this.cbActivate5.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbActivate5.Location = new System.Drawing.Point(19, 44);
            this.cbActivate5.Margin = new System.Windows.Forms.Padding(4);
            this.cbActivate5.Name = "cbActivate5";
            this.cbActivate5.Size = new System.Drawing.Size(130, 33);
            this.cbActivate5.TabIndex = 6;
            this.cbActivate5.Text = "Activate?";
            this.cbActivate5.UseVisualStyleBackColor = false;
            this.cbActivate5.CheckedChanged += new System.EventHandler(this.CbActivate5_CheckedChanged);
            // 
            // groupBox5
            // 
            this.groupBox5.Controls.Add(this.txtThreshold4);
            this.groupBox5.Controls.Add(this.label20);
            this.groupBox5.Controls.Add(this.cbbMethod4);
            this.groupBox5.Controls.Add(this.label15);
            this.groupBox5.Controls.Add(this.txtCode4);
            this.groupBox5.Controls.Add(this.label8);
            this.groupBox5.Controls.Add(this.txtZoneResult4);
            this.groupBox5.Controls.Add(this.label9);
            this.groupBox5.Controls.Add(this.cbActivate4);
            this.groupBox5.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox5.Location = new System.Drawing.Point(1052, 142);
            this.groupBox5.Margin = new System.Windows.Forms.Padding(4);
            this.groupBox5.Name = "groupBox5";
            this.groupBox5.Padding = new System.Windows.Forms.Padding(4);
            this.groupBox5.Size = new System.Drawing.Size(315, 380);
            this.groupBox5.TabIndex = 14;
            this.groupBox5.TabStop = false;
            this.groupBox5.Text = "Zone 4";
            // 
            // txtThreshold4
            // 
            this.txtThreshold4.BackColor = System.Drawing.Color.White;
            this.txtThreshold4.Location = new System.Drawing.Point(177, 322);
            this.txtThreshold4.Margin = new System.Windows.Forms.Padding(4);
            this.txtThreshold4.Name = "txtThreshold4";
            this.txtThreshold4.Size = new System.Drawing.Size(120, 34);
            this.txtThreshold4.TabIndex = 21;
            this.txtThreshold4.TextChanged += new System.EventHandler(this.TxtThreshold4_TextChanged);
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Location = new System.Drawing.Point(13, 326);
            this.label20.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(129, 29);
            this.label20.TabIndex = 20;
            this.label20.Text = "Threshold:";
            // 
            // cbbMethod4
            // 
            this.cbbMethod4.BackColor = System.Drawing.Color.White;
            this.cbbMethod4.FormattingEnabled = true;
            this.cbbMethod4.Items.AddRange(new object[] {
            "STC",
            "CD1",
            "CDS",
            "CD3",
            "CD2"});
            this.cbbMethod4.Location = new System.Drawing.Point(19, 143);
            this.cbbMethod4.Margin = new System.Windows.Forms.Padding(4);
            this.cbbMethod4.Name = "cbbMethod4";
            this.cbbMethod4.Size = new System.Drawing.Size(279, 37);
            this.cbbMethod4.TabIndex = 17;
            this.cbbMethod4.SelectedIndexChanged += new System.EventHandler(this.CbbMethod4_SelectedIndexChanged);
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(13, 96);
            this.label15.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(100, 29);
            this.label15.TabIndex = 16;
            this.label15.Text = "Method:";
            // 
            // txtCode4
            // 
            this.txtCode4.BackColor = System.Drawing.Color.White;
            this.txtCode4.Location = new System.Drawing.Point(103, 261);
            this.txtCode4.Margin = new System.Windows.Forms.Padding(4);
            this.txtCode4.Name = "txtCode4";
            this.txtCode4.Size = new System.Drawing.Size(195, 34);
            this.txtCode4.TabIndex = 11;
            this.txtCode4.TextChanged += new System.EventHandler(this.TxtCode4_TextChanged);
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(13, 265);
            this.label8.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(64, 29);
            this.label8.TabIndex = 10;
            this.label8.Text = "Mov:";
            // 
            // txtZoneResult4
            // 
            this.txtZoneResult4.BackColor = System.Drawing.Color.White;
            this.txtZoneResult4.Location = new System.Drawing.Point(177, 203);
            this.txtZoneResult4.Margin = new System.Windows.Forms.Padding(4);
            this.txtZoneResult4.Name = "txtZoneResult4";
            this.txtZoneResult4.Size = new System.Drawing.Size(120, 34);
            this.txtZoneResult4.TabIndex = 9;
            this.txtZoneResult4.TextChanged += new System.EventHandler(this.TxtZoneResult4_TextChanged);
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(13, 207);
            this.label9.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(78, 29);
            this.label9.TabIndex = 8;
            this.label9.Text = "Code:";
            // 
            // cbActivate4
            // 
            this.cbActivate4.AutoSize = true;
            this.cbActivate4.BackColor = System.Drawing.Color.White;
            this.cbActivate4.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbActivate4.Location = new System.Drawing.Point(19, 44);
            this.cbActivate4.Margin = new System.Windows.Forms.Padding(4);
            this.cbActivate4.Name = "cbActivate4";
            this.cbActivate4.Size = new System.Drawing.Size(130, 33);
            this.cbActivate4.TabIndex = 6;
            this.cbActivate4.Text = "Activate?";
            this.cbActivate4.UseVisualStyleBackColor = false;
            this.cbActivate4.CheckedChanged += new System.EventHandler(this.CbActivate4_CheckedChanged);
            // 
            // groupBox4
            // 
            this.groupBox4.Controls.Add(this.txtThreshold3);
            this.groupBox4.Controls.Add(this.label19);
            this.groupBox4.Controls.Add(this.cbbMethod3);
            this.groupBox4.Controls.Add(this.label14);
            this.groupBox4.Controls.Add(this.txtCode3);
            this.groupBox4.Controls.Add(this.label6);
            this.groupBox4.Controls.Add(this.txtZoneResult3);
            this.groupBox4.Controls.Add(this.label7);
            this.groupBox4.Controls.Add(this.cbActivate3);
            this.groupBox4.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox4.Location = new System.Drawing.Point(711, 142);
            this.groupBox4.Margin = new System.Windows.Forms.Padding(4);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Padding = new System.Windows.Forms.Padding(4);
            this.groupBox4.Size = new System.Drawing.Size(315, 380);
            this.groupBox4.TabIndex = 13;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = "Zone 3";
            // 
            // txtThreshold3
            // 
            this.txtThreshold3.BackColor = System.Drawing.Color.White;
            this.txtThreshold3.Location = new System.Drawing.Point(173, 322);
            this.txtThreshold3.Margin = new System.Windows.Forms.Padding(4);
            this.txtThreshold3.Name = "txtThreshold3";
            this.txtThreshold3.Size = new System.Drawing.Size(120, 34);
            this.txtThreshold3.TabIndex = 19;
            this.txtThreshold3.TextChanged += new System.EventHandler(this.TxtThreshold3_TextChanged);
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Location = new System.Drawing.Point(9, 326);
            this.label19.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(129, 29);
            this.label19.TabIndex = 18;
            this.label19.Text = "Threshold:";
            // 
            // cbbMethod3
            // 
            this.cbbMethod3.BackColor = System.Drawing.Color.White;
            this.cbbMethod3.FormattingEnabled = true;
            this.cbbMethod3.Items.AddRange(new object[] {
            "STC",
            "CD1",
            "CDS",
            "CD3",
            "CD2"});
            this.cbbMethod3.Location = new System.Drawing.Point(15, 143);
            this.cbbMethod3.Margin = new System.Windows.Forms.Padding(4);
            this.cbbMethod3.Name = "cbbMethod3";
            this.cbbMethod3.Size = new System.Drawing.Size(279, 37);
            this.cbbMethod3.TabIndex = 17;
            this.cbbMethod3.SelectedIndexChanged += new System.EventHandler(this.CbbMethod3_SelectedIndexChanged);
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(9, 96);
            this.label14.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(100, 29);
            this.label14.TabIndex = 16;
            this.label14.Text = "Method:";
            // 
            // txtCode3
            // 
            this.txtCode3.BackColor = System.Drawing.Color.White;
            this.txtCode3.Location = new System.Drawing.Point(99, 265);
            this.txtCode3.Margin = new System.Windows.Forms.Padding(4);
            this.txtCode3.Name = "txtCode3";
            this.txtCode3.Size = new System.Drawing.Size(195, 34);
            this.txtCode3.TabIndex = 11;
            this.txtCode3.TextChanged += new System.EventHandler(this.TxtCode3_TextChanged);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(9, 271);
            this.label6.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(64, 29);
            this.label6.TabIndex = 10;
            this.label6.Text = "Mov:";
            // 
            // txtZoneResult3
            // 
            this.txtZoneResult3.BackColor = System.Drawing.Color.White;
            this.txtZoneResult3.Location = new System.Drawing.Point(173, 209);
            this.txtZoneResult3.Margin = new System.Windows.Forms.Padding(4);
            this.txtZoneResult3.Name = "txtZoneResult3";
            this.txtZoneResult3.Size = new System.Drawing.Size(120, 34);
            this.txtZoneResult3.TabIndex = 9;
            this.txtZoneResult3.TextChanged += new System.EventHandler(this.TxtZoneResult3_TextChanged);
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(9, 213);
            this.label7.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(78, 29);
            this.label7.TabIndex = 8;
            this.label7.Text = "Code:";
            // 
            // cbActivate3
            // 
            this.cbActivate3.AutoSize = true;
            this.cbActivate3.BackColor = System.Drawing.Color.White;
            this.cbActivate3.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbActivate3.Location = new System.Drawing.Point(19, 44);
            this.cbActivate3.Margin = new System.Windows.Forms.Padding(4);
            this.cbActivate3.Name = "cbActivate3";
            this.cbActivate3.Size = new System.Drawing.Size(130, 33);
            this.cbActivate3.TabIndex = 6;
            this.cbActivate3.Text = "Activate?";
            this.cbActivate3.UseVisualStyleBackColor = false;
            this.cbActivate3.CheckedChanged += new System.EventHandler(this.CbActivate3_CheckedChanged);
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.txtThreshold2);
            this.groupBox3.Controls.Add(this.label18);
            this.groupBox3.Controls.Add(this.cbbMethod2);
            this.groupBox3.Controls.Add(this.label13);
            this.groupBox3.Controls.Add(this.txtCode2);
            this.groupBox3.Controls.Add(this.label4);
            this.groupBox3.Controls.Add(this.txtZoneResult2);
            this.groupBox3.Controls.Add(this.label5);
            this.groupBox3.Controls.Add(this.cbActivate2);
            this.groupBox3.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox3.Location = new System.Drawing.Point(369, 142);
            this.groupBox3.Margin = new System.Windows.Forms.Padding(4);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Padding = new System.Windows.Forms.Padding(4);
            this.groupBox3.Size = new System.Drawing.Size(315, 380);
            this.groupBox3.TabIndex = 12;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Zone 2";
            // 
            // txtThreshold2
            // 
            this.txtThreshold2.BackColor = System.Drawing.Color.White;
            this.txtThreshold2.Location = new System.Drawing.Point(177, 322);
            this.txtThreshold2.Margin = new System.Windows.Forms.Padding(4);
            this.txtThreshold2.Name = "txtThreshold2";
            this.txtThreshold2.Size = new System.Drawing.Size(120, 34);
            this.txtThreshold2.TabIndex = 17;
            this.txtThreshold2.TextChanged += new System.EventHandler(this.TxtThreshold2_TextChanged);
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Location = new System.Drawing.Point(13, 326);
            this.label18.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(129, 29);
            this.label18.TabIndex = 16;
            this.label18.Text = "Threshold:";
            // 
            // cbbMethod2
            // 
            this.cbbMethod2.BackColor = System.Drawing.Color.White;
            this.cbbMethod2.FormattingEnabled = true;
            this.cbbMethod2.Items.AddRange(new object[] {
            "STC",
            "CD1",
            "CDS",
            "CD3",
            "CD2"});
            this.cbbMethod2.Location = new System.Drawing.Point(19, 143);
            this.cbbMethod2.Margin = new System.Windows.Forms.Padding(4);
            this.cbbMethod2.Name = "cbbMethod2";
            this.cbbMethod2.Size = new System.Drawing.Size(279, 37);
            this.cbbMethod2.TabIndex = 15;
            this.cbbMethod2.SelectedIndexChanged += new System.EventHandler(this.CbbMethod2_SelectedIndexChanged);
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(13, 96);
            this.label13.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(100, 29);
            this.label13.TabIndex = 14;
            this.label13.Text = "Method:";
            // 
            // txtCode2
            // 
            this.txtCode2.BackColor = System.Drawing.Color.White;
            this.txtCode2.Location = new System.Drawing.Point(103, 267);
            this.txtCode2.Margin = new System.Windows.Forms.Padding(4);
            this.txtCode2.Name = "txtCode2";
            this.txtCode2.Size = new System.Drawing.Size(195, 34);
            this.txtCode2.TabIndex = 11;
            this.txtCode2.TextChanged += new System.EventHandler(this.TxtCode2_TextChanged);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(13, 271);
            this.label4.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(64, 29);
            this.label4.TabIndex = 10;
            this.label4.Text = "Mov:";
            // 
            // txtZoneResult2
            // 
            this.txtZoneResult2.BackColor = System.Drawing.Color.White;
            this.txtZoneResult2.Location = new System.Drawing.Point(177, 207);
            this.txtZoneResult2.Margin = new System.Windows.Forms.Padding(4);
            this.txtZoneResult2.Name = "txtZoneResult2";
            this.txtZoneResult2.Size = new System.Drawing.Size(120, 34);
            this.txtZoneResult2.TabIndex = 9;
            this.txtZoneResult2.TextChanged += new System.EventHandler(this.TxtZoneResult2_TextChanged);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(13, 213);
            this.label5.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(78, 29);
            this.label5.TabIndex = 8;
            this.label5.Text = "Code:";
            // 
            // cbActivate2
            // 
            this.cbActivate2.AutoSize = true;
            this.cbActivate2.BackColor = System.Drawing.Color.White;
            this.cbActivate2.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbActivate2.Location = new System.Drawing.Point(19, 44);
            this.cbActivate2.Margin = new System.Windows.Forms.Padding(4);
            this.cbActivate2.Name = "cbActivate2";
            this.cbActivate2.Size = new System.Drawing.Size(130, 33);
            this.cbActivate2.TabIndex = 6;
            this.cbActivate2.Text = "Activate?";
            this.cbActivate2.UseVisualStyleBackColor = false;
            this.cbActivate2.CheckedChanged += new System.EventHandler(this.CbActivate2_CheckedChanged);
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.txtThreshold1);
            this.groupBox2.Controls.Add(this.label17);
            this.groupBox2.Controls.Add(this.cbbMethod1);
            this.groupBox2.Controls.Add(this.label12);
            this.groupBox2.Controls.Add(this.txtCode1);
            this.groupBox2.Controls.Add(this.label3);
            this.groupBox2.Controls.Add(this.txtZoneResult1);
            this.groupBox2.Controls.Add(this.label2);
            this.groupBox2.Controls.Add(this.cbActivate1);
            this.groupBox2.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox2.Location = new System.Drawing.Point(28, 142);
            this.groupBox2.Margin = new System.Windows.Forms.Padding(4);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Padding = new System.Windows.Forms.Padding(4);
            this.groupBox2.Size = new System.Drawing.Size(315, 380);
            this.groupBox2.TabIndex = 6;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Zone 1";
            // 
            // txtThreshold1
            // 
            this.txtThreshold1.BackColor = System.Drawing.Color.White;
            this.txtThreshold1.Location = new System.Drawing.Point(177, 325);
            this.txtThreshold1.Margin = new System.Windows.Forms.Padding(4);
            this.txtThreshold1.Name = "txtThreshold1";
            this.txtThreshold1.Size = new System.Drawing.Size(120, 34);
            this.txtThreshold1.TabIndex = 15;
            this.txtThreshold1.TextChanged += new System.EventHandler(this.TxtThreshold1_TextChanged);
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Location = new System.Drawing.Point(13, 329);
            this.label17.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(129, 29);
            this.label17.TabIndex = 14;
            this.label17.Text = "Threshold:";
            // 
            // cbbMethod1
            // 
            this.cbbMethod1.BackColor = System.Drawing.Color.White;
            this.cbbMethod1.FormattingEnabled = true;
            this.cbbMethod1.Items.AddRange(new object[] {
            "STC",
            "CD1",
            "CDS",
            "CD3",
            "CD2"});
            this.cbbMethod1.Location = new System.Drawing.Point(19, 143);
            this.cbbMethod1.Margin = new System.Windows.Forms.Padding(4);
            this.cbbMethod1.Name = "cbbMethod1";
            this.cbbMethod1.Size = new System.Drawing.Size(279, 37);
            this.cbbMethod1.TabIndex = 13;
            this.cbbMethod1.SelectedIndexChanged += new System.EventHandler(this.CbbMethod1_SelectedIndexChanged);
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(13, 96);
            this.label12.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(100, 29);
            this.label12.TabIndex = 12;
            this.label12.Text = "Method:";
            // 
            // txtCode1
            // 
            this.txtCode1.BackColor = System.Drawing.Color.White;
            this.txtCode1.Location = new System.Drawing.Point(111, 267);
            this.txtCode1.Margin = new System.Windows.Forms.Padding(4);
            this.txtCode1.Name = "txtCode1";
            this.txtCode1.Size = new System.Drawing.Size(187, 34);
            this.txtCode1.TabIndex = 11;
            this.txtCode1.TextChanged += new System.EventHandler(this.TxtCode1_TextChanged);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(13, 271);
            this.label3.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(64, 29);
            this.label3.TabIndex = 10;
            this.label3.Text = "Mov:";
            // 
            // txtZoneResult1
            // 
            this.txtZoneResult1.BackColor = System.Drawing.Color.White;
            this.txtZoneResult1.Location = new System.Drawing.Point(177, 209);
            this.txtZoneResult1.Margin = new System.Windows.Forms.Padding(4);
            this.txtZoneResult1.Name = "txtZoneResult1";
            this.txtZoneResult1.Size = new System.Drawing.Size(120, 34);
            this.txtZoneResult1.TabIndex = 9;
            this.txtZoneResult1.TextChanged += new System.EventHandler(this.TxtZoneResult1_TextChanged);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(13, 213);
            this.label2.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(78, 29);
            this.label2.TabIndex = 8;
            this.label2.Text = "Code:";
            // 
            // cbActivate1
            // 
            this.cbActivate1.AutoSize = true;
            this.cbActivate1.BackColor = System.Drawing.Color.White;
            this.cbActivate1.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbActivate1.Location = new System.Drawing.Point(19, 44);
            this.cbActivate1.Margin = new System.Windows.Forms.Padding(4);
            this.cbActivate1.Name = "cbActivate1";
            this.cbActivate1.Size = new System.Drawing.Size(130, 33);
            this.cbActivate1.TabIndex = 6;
            this.cbActivate1.Text = "Activate?";
            this.cbActivate1.UseVisualStyleBackColor = false;
            this.cbActivate1.CheckedChanged += new System.EventHandler(this.CbActivate1_CheckedChanged);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(23, 74);
            this.label1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(173, 29);
            this.label1.TabIndex = 3;
            this.label1.Text = "Product Name:";
            // 
            // SetupConditionForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(1752, 764);
            this.Controls.Add(this.groupBox1);
            this.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.Name = "SetupConditionForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "U-nica Mini Tool v2.14";
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox6.ResumeLayout(false);
            this.groupBox6.PerformLayout();
            this.groupBox5.ResumeLayout(false);
            this.groupBox5.PerformLayout();
            this.groupBox4.ResumeLayout(false);
            this.groupBox4.PerformLayout();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.CheckBox cbActivate1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox txtZoneResult1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox txtCode1;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.TextBox txtCode2;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox txtZoneResult2;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.CheckBox cbActivate2;
        private System.Windows.Forms.GroupBox groupBox6;
        private System.Windows.Forms.TextBox txtCode5;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.TextBox txtZoneResult5;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.CheckBox cbActivate5;
        private System.Windows.Forms.GroupBox groupBox5;
        private System.Windows.Forms.TextBox txtCode4;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.TextBox txtZoneResult4;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.CheckBox cbActivate4;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.TextBox txtCode3;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox txtZoneResult3;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.CheckBox cbActivate3;
        private System.Windows.Forms.ComboBox cbbMethod5;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.ComboBox cbbMethod4;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.ComboBox cbbMethod3;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.ComboBox cbbMethod2;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.ComboBox cbbMethod1;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Button btnResetZone5;
        private System.Windows.Forms.Button btnResetZone4;
        private System.Windows.Forms.Button btnResetZone3;
        private System.Windows.Forms.Button btnResetZone2;
        private System.Windows.Forms.Button btnUpdate;
        private System.Windows.Forms.Button btnResetZone1;
        private System.Windows.Forms.TextBox txtThreshold5;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.TextBox txtThreshold4;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.TextBox txtThreshold3;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.TextBox txtThreshold2;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.TextBox txtThreshold1;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.TextBox txtProductName;
        private System.Windows.Forms.TextBox txtProductID;
    }
}